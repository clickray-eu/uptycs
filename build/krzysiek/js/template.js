'use strict';

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

// file: footer.js
function releaseTheMargin(footerWrapper, bodyWrapper) {
    // console.log('releasethemargin');
    var footerWrap = $(footerWrapper),
        bodyWrap = $(bodyWrapper);
    bodyWrap.css('margin-bottom', footerWrap.height() - 3);
}

$(document).ready(function () {
    var browserWindow = $(window);
    browserWindow.resize(function () {
        return releaseTheMargin('body:not(.landing) .footer-container-wrapper', 'body:not(.landing) .body-container-wrapper');
    }).resize();

    var arrowUp = $('<div class="arrow-up-wrapper">\n                        <i class="fa fa-chevron-up" aria-hidden="true"></i>\n                    </div>');
    $('body').append(arrowUp);
    browserWindow.scroll(function () {
        return checkIfArrowIsNeeded(arrowUp);
    }).scroll();
    arrowUp.on('click', directionMountain);
});

function checkIfArrowIsNeeded(arrowEl) {
    if (window.innerWidth > 768) {
        var windowHeight = window.innerHeight;
        arrowEl.offset().top > windowHeight ? arrowEl.addClass('visible') : arrowEl.removeClass('visible');
    }
}
function directionMountain() {
    $('body, html').animate({
        scrollTop: 0
    }, 500);
}
// end file: footer.js

// file: form.js
function hideEmptyLabel() {
    $('form label').each(function (i, e) {
        if ($(this).text() == "*") {
            $(this).css({ "display": "none" });
        }
    });
}

function fakePlaceholders() {
    var $inputs = $('input[type=text], input[type=tel], input[type=email]');
    $inputs.each(function () {
        var el = $(this),
            placeholder = el.attr('placeholder'),
            id = el.attr('id');

        if ($(this).val().length > 0) {
            $(this).siblings(".fake-placeholder").addClass("has-value");
        } else {
            $(this).siblings(".fake-placeholder").removeClass("has-value");
        }

        el.attr('placeholder', '');
        el.parent().append('<label for="' + id + '" class="fake-placeholder">' + placeholder + '</label>');
    });

    $inputs.on('change', function () {
        if ($(this).val().length > 0) {
            $(this).siblings(".fake-placeholder").addClass("has-value");
        } else {
            $(this).siblings(".fake-placeholder").removeClass("has-value");
        }
    });
}

// Autogrow textarea while typing
(function ($) {
    /**
     * Auto-growing textareas; technique ripped from Facebook
     * 
     * 
     * http://github.com/jaz303/jquery-grab-bag/tree/master/javascripts/jquery.autogrow-textarea.js
     */
    $.fn.autogrow = function (options) {
        return this.filter('textarea').each(function () {
            var self = this;
            var $self = $(self);
            var minHeight = $self.height();
            var noFlickerPad = $self.hasClass('autogrow-short') ? 0 : parseInt($self.css('lineHeight')) || 0;
            var settings = $.extend({
                preGrowCallback: null,
                postGrowCallback: null
            }, options);

            var shadow = $('<div></div>').css({
                position: 'absolute',
                top: -10000,
                left: -10000,
                width: $self.width(),
                fontSize: $self.css('fontSize'),
                fontFamily: $self.css('fontFamily'),
                fontWeight: $self.css('fontWeight'),
                lineHeight: $self.css('lineHeight'),
                resize: 'none',
                'word-wrap': 'break-word'
            }).appendTo(document.body);

            var update = function update(event) {
                var times = function times(string, number) {
                    for (var i = 0, r = ''; i < number; i++) {
                        r += string;
                    }return r;
                };

                var val = self.value.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/\n$/, '<br/>&#xa0;').replace(/\n/g, '<br/>').replace(/ {2,}/g, function (space) {
                    return times('&#xa0;', space.length - 1) + ' ';
                });

                // Did enter get pressed?  Resize in this keydown event so that the flicker doesn't occur.
                if (event && event.data && event.data.event === 'keydown' && event.keyCode === 13) {
                    val += '<br />';
                }

                shadow.css('width', $self.width());
                shadow.html(val + (noFlickerPad === 0 ? '...' : '')); // Append '...' to resize pre-emptively.

                var newHeight = Math.max(shadow.height() + noFlickerPad, minHeight);
                if (settings.preGrowCallback != null) {
                    newHeight = settings.preGrowCallback($self, shadow, newHeight, minHeight);
                }

                $self.height(newHeight);

                if (settings.postGrowCallback != null) {
                    settings.postGrowCallback($self);
                }
            };

            $self.change(update).keyup(update).keydown({ event: 'keydown' }, update);
            $(window).resize(update);

            update();
        });
    };
})(jQuery);

function autoGrow() {
    $('textarea').autogrow();
}

// Select
function select($wrapper, $form) {
    if ($form.hasClass('new-select-custom')) {
        return;
    }
    $form.addClass('new-select-custom');
    $form.find("select").each(function () {
        var parent = $(this).parent(),
            options = $(this).find('option'),
            placeholder = options.first().text() ? options.first().text() : '-Select-';

        parent.addClass('dropdown_select');
        parent.append('<div class="dropdown-header"><span>' + placeholder + '</span></div>');
        parent.append('<ul class="dropdown-list" style="display: none;"></ul>');
        options.not(":disabled").each(function () {
            // Zmienilem val() na text()
            if ($(this).text() != "") {
                parent.find("ul.dropdown-list").append('<li value="' + $(this).text() + '">' + $(this).text() + '</li>');
            }
        });
    });

    $form.find('.dropdown_select.input .dropdown-header').click(function (event, element) {
        if (!$(this).hasClass('slide-down')) {
            $(this).addClass('slide-down');
            $(this).siblings('.dropdown-list').slideDown();
        } else {
            $(this).removeClass('slide-down');
            $(this).siblings('.dropdown-list').slideUp();
        }
        $(this).children('.arrow-white').toggle();
    });

    $form.find('.dropdown-list li').click(function () {
        var choose = $(this).text();
        $(this).parent().siblings('.dropdown-header').find('span').text(choose);
        $(this).parent().siblings('select').find('option').removeAttr('selected');
        $(this).parent().siblings('select').val(choose).find(' option[value="' + choose + '"] ').attr('selected', 'selected').change();
        $(this).parent().find('li').removeClass('selected');
        $(this).addClass('selected');
        $(this).parent().siblings('.dropdown-header').toggleClass('slide-down').siblings('.dropdown-list').slideUp();
        $(this).parent().siblings('.dropdown-header').children('.arrow-white').toggle();
    });
    $form.find('.dropdown_select .input').click(function (event) {
        event.stopPropagation();
    });
}

// end file: form.js

// file: global.js
$(document).ready(function () {

    iOS();
    if (/MSIE 9/i.test(navigator.userAgent) || /rv:11.0/i.test(navigator.userAgent) || /MSIE 10/i.test(navigator.userAgent) || /Edge\/12./i.test(navigator.userAgent) || window.navigator.userAgent.indexOf("Edge") > -1) {
        // console.log('ms browser');
        $('body').addClass('ms-browser');
    }
});

function waitForLoad(wrapper, element, callback) {
    if ($(wrapper).length > 0) {
        $(wrapper).each(function (i, el) {
            var waitForLoad = setInterval(function () {
                if ($(el).length == $(el).find(element).length) {
                    clearInterval(waitForLoad);
                    callback($(el), $(el).find(element));
                }
            }, 50);
        });
    }
}

waitForLoad(".widget-type-form, .form-wrapper, .widget-type-blog_comments, .hs_cos_wrapper_type_form", "form", autoGrow);
waitForLoad(".widget-type-form, .form-wrapper, .widget-type-blog_comments, .hs_cos_wrapper_type_form", "form", fakePlaceholders);
waitForLoad(".widget-type-form, .form-wrapper, .widget-type-blog_comments, .hs_cos_wrapper_type_form", "form", hideEmptyLabel);
waitForLoad(".widget-type-form, .form-wrapper, .widget-type-blog_comments, .hs_cos_wrapper_type_form", "form", select);

function iOS() {
    var iOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;
    if (iOS) $('body').addClass('ios');
}

// end file: global.js

// file: Blog/blog-301.js
$(document).ready(function () {
    if ($('.fixed-socials').length) {
        var spaceTop = $('.fixed-socials').offset().top;
        $(window).scroll(function () {
            $(window).scrollTop() > spaceTop ? $('.fixed-socials').addClass('fixed') : $('.fixed-socials').removeClass('fixed');
        }).scroll();
    }
});
// end file: Blog/blog-301.js

// file: Modules/FAQ-accordion.js
$(document).ready(function () {
    questionAccordion();
});

function questionAccordion() {
    if ($("html.hs-inline-edit").length == 0) {
        $("section.question").each(function (i, e) {
            $(e).find(".panel-group").append($(e).find(".faq-box"));
            $(e).find(".hs_cos_wrapper_type_widget_container,.hs_cos_wrapper_type_custom_widget").remove();
        });
    }
    $("section.question .panel-group").attr("id", "accordion");
    $("section.question .panel-group .panel").each(function (i, e) {
        var href = $(e).find("h4.panel-title > a").attr("href");
        $(e).find("h4.panel-title > a").attr("href", href + (i + 1));

        var id = $(e).find(".panel-collapse").attr("id");
        $(e).find(".panel-collapse").attr("id", id + (i + 1));
    });
}
// end file: Modules/FAQ-accordion.js

// file: Modules/brand-slider.js
$(document).ready(function () {
    initBrandsSlider();
});
function initBrandsSlider() {
    if ($(".brands-slider").length > 0) {
        $('.brands-slider').cutWrappers('.brands-slider__slide');
        $('.brands-slider').slick({
            autoplay: true,
            autoplaySpeed: 3000,
            slidesToShow: 4,
            slidesToScroll: 1,
            arrows: false,
            dots: false,
            responsive: [{
                breakpoint: 1150,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1
                }
            }, {
                breakpoint: 900,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            }, {
                breakpoint: 760,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }]
        });
    }
}
(function ($) {
    $.fn.cutWrappers = function (targetSelector) {
        $(this).find(targetSelector).appendTo($(this));
        $(this).find('.hs_cos_wrapper_type_custom_widget').remove();
        $(this).find('.hs_cos_wrapper_type_widget_container').remove();
        $(this).find('.widget-span').remove();
    };
})(jQuery);
// end file: Modules/brand-slider.js

// file: Modules/feature-box.js
function matchWidths() {
    var featureBox = $('.feature-box-redirect');
    featureBox.map(function (i, e) {
        if ($(e).hasClass('quarter')) {
            $(e).parent().addClass('quarter');
        } else if ($(e).hasClass('one-fifth')) {
            $(e).parent().addClass('one-fifth');
        } else if ($(e).hasClass('thirty')) {
            $(e).parent().addClass('thirty');
        } else if ($(e).hasClass('seventy')) {
            $(e).parent().addClass('seventy');
        } else if ($(e).hasClass('half')) {
            $(e).parent().addClass('half');
        } else if ($(e).hasClass('fourty')) {
            $(e).parent().addClass('fourty');
        }
    });
}
matchWidths();
// end file: Modules/feature-box.js

// file: Modules/form-and-map.js

waitForLoad('.hs_cos_wrapper', 'form', setSameHeight);

function setSameHeight() {

    var maxHeight = 0;
    var mapHeight = $('#map').innerHeight();

    $('.form-map-customize').find('.item').each(function () {
        var thisH = $(this).innerHeight();
        if (thisH > maxHeight) {
            maxHeight = thisH;
        }
    });

    if (mapHeight < maxHeight) {
        $('#map').css('height', maxHeight);
    }
}

// end file: Modules/form-and-map.js

// file: Modules/hero-banner-image-fixed.js
if ($('body.landing-2').length == 1) {

    // VARIABELS FOR START SCROLL 
    var imageTopPosition = $('.hero-banner-text-image .right img').offset().top;
    var imageLeftMargin = $('.hero-banner-text-image .right img').css('margin-left');
    // VARIABELS FOR END SCROLL 
    var offsetSection = $('.color-wrapper').offset().top + 80;
    var positionToSet = offsetSection - 90;

    // SET HEIGH FOR BANENR WHEN IMAGE IS MOVE
    var bannerH = $('.hero-banner-text-image').innerHeight();
}
// check width device

function checkDeviceWidth() {
    var widthDevice = $(window).innerWidth();

    if (widthDevice < 1024) {
        $('body.landing-2').addClass('aniamtion-off');
    } else {
        $('body.landing-2').removeClass('aniamtion-off');
    }
}

$(document).ready(function () {
    checkDeviceWidth();
});

$(window).resize(function () {
    checkDeviceWidth();
});

$(window).scroll(function () {
    if ($('body.landing-2.aniamtion-off').length == 0 && $('body.landing-2').length == 1) {
        /// START ON SCROLL ////
        var scroll = $(window).scrollTop();
        if (0 <= scroll) {
            // SET HEIGH FOR BANENR WHEN IMAGE IS MOVE
            $('.hero-banner-text-image').css('height', bannerH);
            // BE SCROLL 
            $('.hero-banner-text-image .right ').css({
                'position': 'fixed',
                'top': imageTopPosition + 'px',
                'right': '0px',
                'width': '50%'
            });
            $('.hero-banner-text-image .right img').css({
                'margin-left': imageLeftMargin,
                'margin-right': 'auto'
            });
        } else {

            $('.hero-banner-text-image .right ').css({
                'position': 'absolute',
                'width': '50%'
            });
            $('.hero-banner-text-image .right img').css({
                'margin-left': 'auto',
                'margin-right': '0'
            });
        }

        //// STOP ON BOTTOM ////

        var elementActualOffset = $('.hero-banner-text-image .right').offset().top;

        if (offsetSection < elementActualOffset) {
            $('.hero-banner-text-image .right ').css({
                'position': 'absolute',
                'top': positionToSet + 'px'
            });
        }
    }
});
// end file: Modules/hero-banner-image-fixed.js

// file: Modules/parallax.js
$(function () {
    if (navigator.userAgent.match(/(iPod|iPhone|iPad|Android)/)) {
        $('#ios-notice').removeClass('hidden');
        $('.parallax-container').height($(window).height() * 0.5 | 0);
    } else {
        $(window).resize(function () {
            var parallaxHeight = Math.max($(window).height() * 0.7, 200) | 0;
            $('.parallax-container').height(parallaxHeight);
        }).trigger('resize');
    }
});

/*!
 * parallax.js v1.4.2 (https://pixelcog.github.io/parallax.js/)
 * @copyright 2016 PixelCog, Inc.
 * @license MIT (https://github.com/pixelcog/parallax.js/blob/master/LICENSE)
 */
!function (t, i, e, s) {
    function o(i, e) {
        var h = this;"object" == (typeof e === 'undefined' ? 'undefined' : _typeof(e)) && (delete e.refresh, delete e.render, t.extend(this, e)), this.$element = t(i), !this.imageSrc && this.$element.is("img") && (this.imageSrc = this.$element.attr("src"));var r = (this.position + "").toLowerCase().match(/\S+/g) || [];if (r.length < 1 && r.push("center"), 1 == r.length && r.push(r[0]), ("top" == r[0] || "bottom" == r[0] || "left" == r[1] || "right" == r[1]) && (r = [r[1], r[0]]), this.positionX != s && (r[0] = this.positionX.toLowerCase()), this.positionY != s && (r[1] = this.positionY.toLowerCase()), h.positionX = r[0], h.positionY = r[1], "left" != this.positionX && "right" != this.positionX && (this.positionX = isNaN(parseInt(this.positionX)) ? "center" : parseInt(this.positionX)), "top" != this.positionY && "bottom" != this.positionY && (this.positionY = isNaN(parseInt(this.positionY)) ? "center" : parseInt(this.positionY)), this.position = this.positionX + (isNaN(this.positionX) ? "" : "px") + " " + this.positionY + (isNaN(this.positionY) ? "" : "px"), navigator.userAgent.match(/(iPod|iPhone|iPad)/)) return this.imageSrc && this.iosFix && !this.$element.is("img") && this.$element.css({ backgroundImage: "url(" + this.imageSrc + ")", backgroundSize: "cover", backgroundPosition: this.position }), this;if (navigator.userAgent.match(/(Android)/)) return this.imageSrc && this.androidFix && !this.$element.is("img") && this.$element.css({ backgroundImage: "url(" + this.imageSrc + ")", backgroundSize: "cover", backgroundPosition: this.position }), this;this.$mirror = t("<div />").prependTo("body");var a = this.$element.find(">.parallax-slider"),
            n = !1;0 == a.length ? this.$slider = t("<img />").prependTo(this.$mirror) : (this.$slider = a.prependTo(this.$mirror), n = !0), this.$mirror.addClass("parallax-mirror").css({ visibility: "hidden", zIndex: this.zIndex, position: "fixed", top: 0, left: 0, overflow: "hidden" }), this.$slider.addClass("parallax-slider").one("load", function () {
            h.naturalHeight && h.naturalWidth || (h.naturalHeight = this.naturalHeight || this.height || 1, h.naturalWidth = this.naturalWidth || this.width || 1), h.aspectRatio = h.naturalWidth / h.naturalHeight, o.isSetup || o.setup(), o.sliders.push(h), o.isFresh = !1, o.requestRender();
        }), n || (this.$slider[0].src = this.imageSrc), (this.naturalHeight && this.naturalWidth || this.$slider[0].complete || a.length > 0) && this.$slider.trigger("load");
    }function h(s) {
        return this.each(function () {
            var h = t(this),
                r = "object" == (typeof s === 'undefined' ? 'undefined' : _typeof(s)) && s;this == i || this == e || h.is("body") ? o.configure(r) : h.data("px.parallax") ? "object" == (typeof s === 'undefined' ? 'undefined' : _typeof(s)) && t.extend(h.data("px.parallax"), r) : (r = t.extend({}, h.data(), r), h.data("px.parallax", new o(this, r))), "string" == typeof s && ("destroy" == s ? o.destroy(this) : o[s]());
        });
    }!function () {
        for (var t = 0, e = ["ms", "moz", "webkit", "o"], s = 0; s < e.length && !i.requestAnimationFrame; ++s) {
            i.requestAnimationFrame = i[e[s] + "RequestAnimationFrame"], i.cancelAnimationFrame = i[e[s] + "CancelAnimationFrame"] || i[e[s] + "CancelRequestAnimationFrame"];
        }i.requestAnimationFrame || (i.requestAnimationFrame = function (e) {
            var s = new Date().getTime(),
                o = Math.max(0, 16 - (s - t)),
                h = i.setTimeout(function () {
                e(s + o);
            }, o);return t = s + o, h;
        }), i.cancelAnimationFrame || (i.cancelAnimationFrame = function (t) {
            clearTimeout(t);
        });
    }(), t.extend(o.prototype, { speed: .2, bleed: 0, zIndex: -100, iosFix: !0, androidFix: !0, position: "center", overScrollFix: !1, refresh: function refresh() {
            this.boxWidth = this.$element.outerWidth(), this.boxHeight = this.$element.outerHeight() + 2 * this.bleed, this.boxOffsetTop = this.$element.offset().top - this.bleed, this.boxOffsetLeft = this.$element.offset().left, this.boxOffsetBottom = this.boxOffsetTop + this.boxHeight;var t = o.winHeight,
                i = o.docHeight,
                e = Math.min(this.boxOffsetTop, i - t),
                s = Math.max(this.boxOffsetTop + this.boxHeight - t, 0),
                h = this.boxHeight + (e - s) * (1 - this.speed) | 0,
                r = (this.boxOffsetTop - e) * (1 - this.speed) | 0;if (h * this.aspectRatio >= this.boxWidth) {
                this.imageWidth = h * this.aspectRatio | 0, this.imageHeight = h, this.offsetBaseTop = r;var a = this.imageWidth - this.boxWidth;this.offsetLeft = "left" == this.positionX ? 0 : "right" == this.positionX ? -a : isNaN(this.positionX) ? -a / 2 | 0 : Math.max(this.positionX, -a);
            } else {
                this.imageWidth = this.boxWidth, this.imageHeight = this.boxWidth / this.aspectRatio | 0, this.offsetLeft = 0;var a = this.imageHeight - h;this.offsetBaseTop = "top" == this.positionY ? r : "bottom" == this.positionY ? r - a : isNaN(this.positionY) ? r - a / 2 | 0 : r + Math.max(this.positionY, -a);
            }
        }, render: function render() {
            var t = o.scrollTop,
                i = o.scrollLeft,
                e = this.overScrollFix ? o.overScroll : 0,
                s = t + o.winHeight;this.boxOffsetBottom > t && this.boxOffsetTop <= s ? (this.visibility = "visible", this.mirrorTop = this.boxOffsetTop - t, this.mirrorLeft = this.boxOffsetLeft - i, this.offsetTop = this.offsetBaseTop - this.mirrorTop * (1 - this.speed)) : this.visibility = "hidden", this.$mirror.css({ transform: "translate3d(0px, 0px, 0px)", visibility: this.visibility, top: this.mirrorTop - e, left: this.mirrorLeft, height: this.boxHeight, width: this.boxWidth }), this.$slider.css({ transform: "translate3d(0px, 0px, 0px)", position: "absolute", top: this.offsetTop, left: this.offsetLeft, height: this.imageHeight, width: this.imageWidth, maxWidth: "none" });
        } }), t.extend(o, { scrollTop: 0, scrollLeft: 0, winHeight: 0, winWidth: 0, docHeight: 1 << 30, docWidth: 1 << 30, sliders: [], isReady: !1, isFresh: !1, isBusy: !1, setup: function setup() {
            if (!this.isReady) {
                var s = t(e),
                    h = t(i),
                    r = function r() {
                    o.winHeight = h.height(), o.winWidth = h.width(), o.docHeight = s.height(), o.docWidth = s.width();
                },
                    a = function a() {
                    var t = h.scrollTop(),
                        i = o.docHeight - o.winHeight,
                        e = o.docWidth - o.winWidth;o.scrollTop = Math.max(0, Math.min(i, t)), o.scrollLeft = Math.max(0, Math.min(e, h.scrollLeft())), o.overScroll = Math.max(t - i, Math.min(t, 0));
                };h.on("resize.px.parallax load.px.parallax", function () {
                    r(), o.isFresh = !1, o.requestRender();
                }).on("scroll.px.parallax load.px.parallax", function () {
                    a(), o.requestRender();
                }), r(), a(), this.isReady = !0;
            }
        }, configure: function configure(i) {
            "object" == (typeof i === 'undefined' ? 'undefined' : _typeof(i)) && (delete i.refresh, delete i.render, t.extend(this.prototype, i));
        }, refresh: function refresh() {
            t.each(this.sliders, function () {
                this.refresh();
            }), this.isFresh = !0;
        }, render: function render() {
            this.isFresh || this.refresh(), t.each(this.sliders, function () {
                this.render();
            });
        }, requestRender: function requestRender() {
            var t = this;this.isBusy || (this.isBusy = !0, i.requestAnimationFrame(function () {
                t.render(), t.isBusy = !1;
            }));
        }, destroy: function destroy(e) {
            var s,
                h = t(e).data("px.parallax");for (h.$mirror.remove(), s = 0; s < this.sliders.length; s += 1) {
                this.sliders[s] == h && this.sliders.splice(s, 1);
            }t(e).data("px.parallax", !1), 0 === this.sliders.length && (t(i).off("scroll.px.parallax resize.px.parallax load.px.parallax"), this.isReady = !1, o.isSetup = !1);
        } });var r = t.fn.parallax;t.fn.parallax = h, t.fn.parallax.Constructor = o, t.fn.parallax.noConflict = function () {
        return t.fn.parallax = r, this;
    }, t(e).on("ready.px.parallax.data-api", function () {
        t('[data-parallax="scroll"]').parallax();
    });
}(jQuery, window, document);
// end file: Modules/parallax.js

// file: Modules/video.js
// file: Storylead/stl-product-slider.js
$(document).ready(function () {
    $('.stl-product-slider .hs_cos_wrapper_type_widget_container').slick({
        infinite: true,
        autoplay: true,
        dots: false,
        arrows: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        responsive: [{
            breakpoint: 991,
            settings: {
                arrows: false,
                slidesToShow: 2,
                slidesToScroll: 1
            }
        }, {
            breakpoint: 767,
            settings: {
                arrows: false,
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }]
    });
});
// end file: Storylead/stl-product-slider.js

// file: Storylead/stl-product-video-slider.js
$(document).ready(function () {
    $('.stl-product-video-slider .hs_cos_wrapper_type_widget_container').slick({
        infinite: true,
        autoplay: true,
        autoplaySpeed: 5000,
        dots: false,
        arrows: false,
        slidesToShow: 4,
        variableWidth: true,
        slidesToScroll: 1,
        responsive: [{
            breakpoint: 1200,
            settings: {
                slidesToShow: 1,
                centerMode: true
            }
        }]
    });

    videoAutoplayFix();
});

$(".stl-product-video-slider a.popup.video-url, .stl-product-video-slide a.popup.video-url").click(function () {
    var videoSrc = $(this).data('video');
    var $this = $(this);

    $.magnificPopup.open({
        items: {
            type: 'inline',
            src: '<video controls preload="auto" width="100%" height="100%" src="' + videoSrc + '"></video>'
        },
        callbacks: {
            open: function open() {
                $('html').css('margin-right', 0);
                $this.parent().parent().addClass("hover");
                $(this.content)[0].play();
            },
            close: function close() {
                $this.parent().parent().removeClass("hover");
                $(this.content)[0].load();
            }
        }
    });
});

$(".stl-product-video-slider a.popup.video-embed, .stl-product-video-slide a.popup.video-embed").click(function () {
    var videoSrc = $(this).data('video');
    var $this = $(this);

    $.magnificPopup.open({
        items: {
            type: 'inline',
            src: '<div class="iframe-container"><iframe src="' + videoSrc + '?autoplay=1" frameborder="0" allowfullscreen></iframe></div>'
        },
        callbacks: {
            open: function open() {
                $('html').css('margin-right', 0);
                $this.parent().parent().addClass("hover");
            },
            close: function close() {
                $this.parent().parent().removeClass("hover");
            }
        }
    });
});

$(".stl-product-video-slider a.popup.image, .stl-product-video-slide a.popup.image").click(function () {
    var imageSrc = $(this).data('image');
    var $this = $(this);

    $.magnificPopup.open({
        items: {
            type: 'inline',
            src: '<div><img width="100%" src="' + imageSrc + '"></div>'
        },
        callbacks: {
            open: function open() {
                $('html').css('margin-right', 0);
                $this.parent().parent().addClass("hover");
            },
            close: function close() {
                $this.parent().parent().removeClass("hover");
            }
        }
    });
});

function videoAutoplayFix() {
    $('.stl-product-video-slider video, .stl-product-video-slide video').each(function () {
        if ($(this).attr('autoplay')) {
            $(this)[0].load();
            $(this)[0].play();
        }
    });

    $('.stl-product-video-slider .vimeo, .stl-product-video-slide vimeo').each(function () {
        var video = $(this)[0];

        //Create a new Vimeo.Player object
        var player = new Vimeo.Player(video);

        //When the player is ready, set the volume to 0
        player.ready().then(function () {
            player.setVolume(0);
        });
    });
}
// end file: Storylead/stl-product-video-slider.js
// end file: Modules/video.js
//# sourceMappingURL=template.js.map
