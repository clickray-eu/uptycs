"use strict";

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

// file: _es6_polyfill.min.js
// jshint ignore: start
!function e(t, n, r) {
    function s(o, u) {
        if (!n[o]) {
            if (!t[o]) {
                var c = "function" == typeof require && require;if (!u && c) return c(o, !0);if (i) return i(o, !0);var a = new Error("Cannot find module '" + o + "'");throw a.code = "MODULE_NOT_FOUND", a;
            }var f = n[o] = { exports: {} };t[o][0].call(f.exports, function (n) {
                var r = t[o][1][n];return s(r || n);
            }, f, f.exports, e, t, n, r);
        }return n[o].exports;
    }for (var i = "function" == typeof require && require, o = 0; o < r.length; o++) {
        s(r[o]);
    }return s;
}({ 1: [function (t, n, r) {
        (function (n) {
            "use strict";
            function define(t, n, e) {
                t[n] || Object[r](t, n, { writable: !0, configurable: !0, value: e });
            }if (t(327), t(328), t(2), n._babelPolyfill) throw new Error("only one instance of babel-polyfill is allowed");n._babelPolyfill = !0;var r = "defineProperty";define(String.prototype, "padLeft", "".padStart), define(String.prototype, "padRight", "".padEnd), "pop,reverse,shift,keys,values,entries,indexOf,every,some,forEach,map,filter,find,findIndex,includes,join,slice,concat,push,splice,unshift,sort,lastIndexOf,reduce,reduceRight,copyWithin,fill".split(",").forEach(function (t) {
                [][t] && define(Array, t, Function.call.bind([][t]));
            });
        }).call(this, "undefined" != typeof global ? global : "undefined" != typeof self ? self : "undefined" != typeof window ? window : {});
    }, { 2: 2, 327: 327, 328: 328 }], 2: [function (t, n, r) {
        t(130), n.exports = t(23).RegExp.escape;
    }, { 130: 130, 23: 23 }], 3: [function (t, n, r) {
        n.exports = function (t) {
            if ("function" != typeof t) throw TypeError(t + " is not a function!");return t;
        };
    }, {}], 4: [function (t, n, r) {
        var e = t(18);n.exports = function (t, n) {
            if ("number" != typeof t && "Number" != e(t)) throw TypeError(n);return +t;
        };
    }, { 18: 18 }], 5: [function (t, n, r) {
        var e = t(128)("unscopables"),
            i = Array.prototype;void 0 == i[e] && t(42)(i, e, {}), n.exports = function (t) {
            i[e][t] = !0;
        };
    }, { 128: 128, 42: 42 }], 6: [function (t, n, r) {
        n.exports = function (t, n, r, e) {
            if (!(t instanceof n) || void 0 !== e && e in t) throw TypeError(r + ": incorrect invocation!");return t;
        };
    }, {}], 7: [function (t, n, r) {
        var e = t(51);n.exports = function (t) {
            if (!e(t)) throw TypeError(t + " is not an object!");return t;
        };
    }, { 51: 51 }], 8: [function (t, n, r) {
        "use strict";
        var e = t(119),
            i = t(114),
            o = t(118);n.exports = [].copyWithin || function copyWithin(t, n) {
            var r = e(this),
                u = o(r.length),
                c = i(t, u),
                a = i(n, u),
                f = arguments.length > 2 ? arguments[2] : void 0,
                s = Math.min((void 0 === f ? u : i(f, u)) - a, u - c),
                l = 1;for (a < c && c < a + s && (l = -1, a += s - 1, c += s - 1); s-- > 0;) {
                a in r ? r[c] = r[a] : delete r[c], c += l, a += l;
            }return r;
        };
    }, { 114: 114, 118: 118, 119: 119 }], 9: [function (t, n, r) {
        "use strict";
        var e = t(119),
            i = t(114),
            o = t(118);n.exports = function fill(t) {
            for (var n = e(this), r = o(n.length), u = arguments.length, c = i(u > 1 ? arguments[1] : void 0, r), a = u > 2 ? arguments[2] : void 0, f = void 0 === a ? r : i(a, r); f > c;) {
                n[c++] = t;
            }return n;
        };
    }, { 114: 114, 118: 118, 119: 119 }], 10: [function (t, n, r) {
        var e = t(39);n.exports = function (t, n) {
            var r = [];return e(t, !1, r.push, r, n), r;
        };
    }, { 39: 39 }], 11: [function (t, n, r) {
        var e = t(117),
            i = t(118),
            o = t(114);n.exports = function (t) {
            return function (n, r, u) {
                var c,
                    a = e(n),
                    f = i(a.length),
                    s = o(u, f);if (t && r != r) {
                    for (; f > s;) {
                        if ((c = a[s++]) != c) return !0;
                    }
                } else for (; f > s; s++) {
                    if ((t || s in a) && a[s] === r) return t || s || 0;
                }return !t && -1;
            };
        };
    }, { 114: 114, 117: 117, 118: 118 }], 12: [function (t, n, r) {
        var e = t(25),
            i = t(47),
            o = t(119),
            u = t(118),
            c = t(15);n.exports = function (t, n) {
            var r = 1 == t,
                a = 2 == t,
                f = 3 == t,
                s = 4 == t,
                l = 6 == t,
                h = 5 == t || l,
                v = n || c;return function (n, c, p) {
                for (var d, y, g = o(n), m = i(g), b = e(c, p, 3), x = u(m.length), S = 0, w = r ? v(n, x) : a ? v(n, 0) : void 0; x > S; S++) {
                    if ((h || S in m) && (d = m[S], y = b(d, S, g), t)) if (r) w[S] = y;else if (y) switch (t) {case 3:
                            return !0;case 5:
                            return d;case 6:
                            return S;case 2:
                            w.push(d);} else if (s) return !1;
                }return l ? -1 : f || s ? s : w;
            };
        };
    }, { 118: 118, 119: 119, 15: 15, 25: 25, 47: 47 }], 13: [function (t, n, r) {
        var e = t(3),
            i = t(119),
            o = t(47),
            u = t(118);n.exports = function (t, n, r, c, a) {
            e(n);var f = i(t),
                s = o(f),
                l = u(f.length),
                h = a ? l - 1 : 0,
                v = a ? -1 : 1;if (r < 2) for (;;) {
                if (h in s) {
                    c = s[h], h += v;break;
                }if (h += v, a ? h < 0 : l <= h) throw TypeError("Reduce of empty array with no initial value");
            }for (; a ? h >= 0 : l > h; h += v) {
                h in s && (c = n(c, s[h], h, f));
            }return c;
        };
    }, { 118: 118, 119: 119, 3: 3, 47: 47 }], 14: [function (t, n, r) {
        var e = t(51),
            i = t(49),
            o = t(128)("species");n.exports = function (t) {
            var n;return i(t) && (n = t.constructor, "function" != typeof n || n !== Array && !i(n.prototype) || (n = void 0), e(n) && null === (n = n[o]) && (n = void 0)), void 0 === n ? Array : n;
        };
    }, { 128: 128, 49: 49, 51: 51 }], 15: [function (t, n, r) {
        var e = t(14);n.exports = function (t, n) {
            return new (e(t))(n);
        };
    }, { 14: 14 }], 16: [function (t, n, r) {
        "use strict";
        var e = t(3),
            i = t(51),
            o = t(46),
            u = [].slice,
            c = {},
            a = function a(t, n, r) {
            if (!(n in c)) {
                for (var e = [], i = 0; i < n; i++) {
                    e[i] = "a[" + i + "]";
                }c[n] = Function("F,a", "return new F(" + e.join(",") + ")");
            }return c[n](t, r);
        };n.exports = Function.bind || function bind(t) {
            var n = e(this),
                r = u.call(arguments, 1),
                c = function c() {
                var e = r.concat(u.call(arguments));return this instanceof c ? a(n, e.length, e) : o(n, e, t);
            };return i(n.prototype) && (c.prototype = n.prototype), c;
        };
    }, { 3: 3, 46: 46, 51: 51 }], 17: [function (t, n, r) {
        var e = t(18),
            i = t(128)("toStringTag"),
            o = "Arguments" == e(function () {
            return arguments;
        }()),
            u = function u(t, n) {
            try {
                return t[n];
            } catch (t) {}
        };n.exports = function (t) {
            var n, r, c;return void 0 === t ? "Undefined" : null === t ? "Null" : "string" == typeof (r = u(n = Object(t), i)) ? r : o ? e(n) : "Object" == (c = e(n)) && "function" == typeof n.callee ? "Arguments" : c;
        };
    }, { 128: 128, 18: 18 }], 18: [function (t, n, r) {
        var e = {}.toString;n.exports = function (t) {
            return e.call(t).slice(8, -1);
        };
    }, {}], 19: [function (t, n, r) {
        "use strict";
        var e = t(72).f,
            i = t(71),
            o = t(93),
            u = t(25),
            c = t(6),
            a = t(39),
            f = t(55),
            s = t(57),
            l = t(100),
            h = t(29),
            v = t(66).fastKey,
            p = t(125),
            d = h ? "_s" : "size",
            y = function y(t, n) {
            var r,
                e = v(n);if ("F" !== e) return t._i[e];for (r = t._f; r; r = r.n) {
                if (r.k == n) return r;
            }
        };n.exports = { getConstructor: function getConstructor(t, n, r, f) {
                var s = t(function (t, e) {
                    c(t, s, n, "_i"), t._t = n, t._i = i(null), t._f = void 0, t._l = void 0, t[d] = 0, void 0 != e && a(e, r, t[f], t);
                });return o(s.prototype, { clear: function clear() {
                        for (var t = p(this, n), r = t._i, e = t._f; e; e = e.n) {
                            e.r = !0, e.p && (e.p = e.p.n = void 0), delete r[e.i];
                        }t._f = t._l = void 0, t[d] = 0;
                    }, delete: function _delete(t) {
                        var r = p(this, n),
                            e = y(r, t);if (e) {
                            var i = e.n,
                                o = e.p;delete r._i[e.i], e.r = !0, o && (o.n = i), i && (i.p = o), r._f == e && (r._f = i), r._l == e && (r._l = o), r[d]--;
                        }return !!e;
                    }, forEach: function forEach(t) {
                        p(this, n);for (var r, e = u(t, arguments.length > 1 ? arguments[1] : void 0, 3); r = r ? r.n : this._f;) {
                            for (e(r.v, r.k, this); r && r.r;) {
                                r = r.p;
                            }
                        }
                    }, has: function has(t) {
                        return !!y(p(this, n), t);
                    } }), h && e(s.prototype, "size", { get: function get() {
                        return p(this, n)[d];
                    } }), s;
            }, def: function def(t, n, r) {
                var e,
                    i,
                    o = y(t, n);return o ? o.v = r : (t._l = o = { i: i = v(n, !0), k: n, v: r, p: e = t._l, n: void 0, r: !1 }, t._f || (t._f = o), e && (e.n = o), t[d]++, "F" !== i && (t._i[i] = o)), t;
            }, getEntry: y, setStrong: function setStrong(t, n, r) {
                f(t, n, function (t, r) {
                    this._t = p(t, n), this._k = r, this._l = void 0;
                }, function () {
                    for (var t = this, n = t._k, r = t._l; r && r.r;) {
                        r = r.p;
                    }return t._t && (t._l = r = r ? r.n : t._t._f) ? "keys" == n ? s(0, r.k) : "values" == n ? s(0, r.v) : s(0, [r.k, r.v]) : (t._t = void 0, s(1));
                }, r ? "entries" : "values", !r, !0), l(n);
            } };
    }, { 100: 100, 125: 125, 25: 25, 29: 29, 39: 39, 55: 55, 57: 57, 6: 6, 66: 66, 71: 71, 72: 72, 93: 93 }], 20: [function (t, n, r) {
        var e = t(17),
            i = t(10);n.exports = function (t) {
            return function toJSON() {
                if (e(this) != t) throw TypeError(t + "#toJSON isn't generic");return i(this);
            };
        };
    }, { 10: 10, 17: 17 }], 21: [function (t, n, r) {
        "use strict";
        var e = t(93),
            i = t(66).getWeak,
            o = t(7),
            u = t(51),
            c = t(6),
            a = t(39),
            f = t(12),
            s = t(41),
            l = t(125),
            h = f(5),
            v = f(6),
            p = 0,
            d = function d(t) {
            return t._l || (t._l = new y());
        },
            y = function y() {
            this.a = [];
        },
            g = function g(t, n) {
            return h(t.a, function (t) {
                return t[0] === n;
            });
        };y.prototype = { get: function get(t) {
                var n = g(this, t);if (n) return n[1];
            }, has: function has(t) {
                return !!g(this, t);
            }, set: function set(t, n) {
                var r = g(this, t);r ? r[1] = n : this.a.push([t, n]);
            }, delete: function _delete(t) {
                var n = v(this.a, function (n) {
                    return n[0] === t;
                });return ~n && this.a.splice(n, 1), !!~n;
            } }, n.exports = { getConstructor: function getConstructor(t, n, r, o) {
                var f = t(function (t, e) {
                    c(t, f, n, "_i"), t._t = n, t._i = p++, t._l = void 0, void 0 != e && a(e, r, t[o], t);
                });return e(f.prototype, { delete: function _delete(t) {
                        if (!u(t)) return !1;var r = i(t);return !0 === r ? d(l(this, n)).delete(t) : r && s(r, this._i) && delete r[this._i];
                    }, has: function has(t) {
                        if (!u(t)) return !1;var r = i(t);return !0 === r ? d(l(this, n)).has(t) : r && s(r, this._i);
                    } }), f;
            }, def: function def(t, n, r) {
                var e = i(o(n), !0);return !0 === e ? d(t).set(n, r) : e[t._i] = r, t;
            }, ufstore: d };
    }, { 12: 12, 125: 125, 39: 39, 41: 41, 51: 51, 6: 6, 66: 66, 7: 7, 93: 93 }], 22: [function (t, n, r) {
        "use strict";
        var e = t(40),
            i = t(33),
            o = t(94),
            u = t(93),
            c = t(66),
            a = t(39),
            f = t(6),
            s = t(51),
            l = t(35),
            h = t(56),
            v = t(101),
            p = t(45);n.exports = function (t, n, r, d, y, g) {
            var m = e[t],
                b = m,
                x = y ? "set" : "add",
                S = b && b.prototype,
                w = {},
                _ = function _(t) {
                var n = S[t];o(S, t, "delete" == t ? function (t) {
                    return !(g && !s(t)) && n.call(this, 0 === t ? 0 : t);
                } : "has" == t ? function has(t) {
                    return !(g && !s(t)) && n.call(this, 0 === t ? 0 : t);
                } : "get" == t ? function get(t) {
                    return g && !s(t) ? void 0 : n.call(this, 0 === t ? 0 : t);
                } : "add" == t ? function add(t) {
                    return n.call(this, 0 === t ? 0 : t), this;
                } : function set(t, r) {
                    return n.call(this, 0 === t ? 0 : t, r), this;
                });
            };if ("function" == typeof b && (g || S.forEach && !l(function () {
                new b().entries().next();
            }))) {
                var E = new b(),
                    O = E[x](g ? {} : -0, 1) != E,
                    P = l(function () {
                    E.has(1);
                }),
                    M = h(function (t) {
                    new b(t);
                }),
                    F = !g && l(function () {
                    for (var t = new b(), n = 5; n--;) {
                        t[x](n, n);
                    }return !t.has(-0);
                });M || (b = n(function (n, r) {
                    f(n, b, t);var e = p(new m(), n, b);return void 0 != r && a(r, y, e[x], e), e;
                }), b.prototype = S, S.constructor = b), (P || F) && (_("delete"), _("has"), y && _("get")), (F || O) && _(x), g && S.clear && delete S.clear;
            } else b = d.getConstructor(n, t, y, x), u(b.prototype, r), c.NEED = !0;return v(b, t), w[t] = b, i(i.G + i.W + i.F * (b != m), w), g || d.setStrong(b, t, y), b;
        };
    }, { 101: 101, 33: 33, 35: 35, 39: 39, 40: 40, 45: 45, 51: 51, 56: 56, 6: 6, 66: 66, 93: 93, 94: 94 }], 23: [function (t, n, r) {
        var e = n.exports = { version: "2.5.0" };"number" == typeof __e && (__e = e);
    }, {}], 24: [function (t, n, r) {
        "use strict";
        var e = t(72),
            i = t(92);n.exports = function (t, n, r) {
            n in t ? e.f(t, n, i(0, r)) : t[n] = r;
        };
    }, { 72: 72, 92: 92 }], 25: [function (t, n, r) {
        var e = t(3);n.exports = function (t, n, r) {
            if (e(t), void 0 === n) return t;switch (r) {case 1:
                    return function (r) {
                        return t.call(n, r);
                    };case 2:
                    return function (r, e) {
                        return t.call(n, r, e);
                    };case 3:
                    return function (r, e, i) {
                        return t.call(n, r, e, i);
                    };}return function () {
                return t.apply(n, arguments);
            };
        };
    }, { 3: 3 }], 26: [function (t, n, r) {
        "use strict";
        var e = t(35),
            i = Date.prototype.getTime,
            o = Date.prototype.toISOString,
            u = function u(t) {
            return t > 9 ? t : "0" + t;
        };n.exports = e(function () {
            return "0385-07-25T07:06:39.999Z" != o.call(new Date(-5e13 - 1));
        }) || !e(function () {
            o.call(new Date(NaN));
        }) ? function toISOString() {
            if (!isFinite(i.call(this))) throw RangeError("Invalid time value");var t = this,
                n = t.getUTCFullYear(),
                r = t.getUTCMilliseconds(),
                e = n < 0 ? "-" : n > 9999 ? "+" : "";return e + ("00000" + Math.abs(n)).slice(e ? -6 : -4) + "-" + u(t.getUTCMonth() + 1) + "-" + u(t.getUTCDate()) + "T" + u(t.getUTCHours()) + ":" + u(t.getUTCMinutes()) + ":" + u(t.getUTCSeconds()) + "." + (r > 99 ? r : "0" + u(r)) + "Z";
        } : o;
    }, { 35: 35 }], 27: [function (t, n, r) {
        "use strict";
        var e = t(7),
            i = t(120);n.exports = function (t) {
            if ("string" !== t && "number" !== t && "default" !== t) throw TypeError("Incorrect hint");return i(e(this), "number" != t);
        };
    }, { 120: 120, 7: 7 }], 28: [function (t, n, r) {
        n.exports = function (t) {
            if (void 0 == t) throw TypeError("Can't call method on  " + t);return t;
        };
    }, {}], 29: [function (t, n, r) {
        n.exports = !t(35)(function () {
            return 7 != Object.defineProperty({}, "a", { get: function get() {
                    return 7;
                } }).a;
        });
    }, { 35: 35 }], 30: [function (t, n, r) {
        var e = t(51),
            i = t(40).document,
            o = e(i) && e(i.createElement);n.exports = function (t) {
            return o ? i.createElement(t) : {};
        };
    }, { 40: 40, 51: 51 }], 31: [function (t, n, r) {
        n.exports = "constructor,hasOwnProperty,isPrototypeOf,propertyIsEnumerable,toLocaleString,toString,valueOf".split(",");
    }, {}], 32: [function (t, n, r) {
        var e = t(81),
            i = t(78),
            o = t(82);n.exports = function (t) {
            var n = e(t),
                r = i.f;if (r) for (var u, c = r(t), a = o.f, f = 0; c.length > f;) {
                a.call(t, u = c[f++]) && n.push(u);
            }return n;
        };
    }, { 78: 78, 81: 81, 82: 82 }], 33: [function (t, n, r) {
        var e = t(40),
            i = t(23),
            o = t(42),
            u = t(94),
            c = t(25),
            a = function a(t, n, r) {
            var f,
                s,
                l,
                h,
                v = t & a.F,
                p = t & a.G,
                d = t & a.S,
                y = t & a.P,
                g = t & a.B,
                m = p ? e : d ? e[n] || (e[n] = {}) : (e[n] || {}).prototype,
                b = p ? i : i[n] || (i[n] = {}),
                x = b.prototype || (b.prototype = {});p && (r = n);for (f in r) {
                s = !v && m && void 0 !== m[f], l = (s ? m : r)[f], h = g && s ? c(l, e) : y && "function" == typeof l ? c(Function.call, l) : l, m && u(m, f, l, t & a.U), b[f] != l && o(b, f, h), y && x[f] != l && (x[f] = l);
            }
        };e.core = i, a.F = 1, a.G = 2, a.S = 4, a.P = 8, a.B = 16, a.W = 32, a.U = 64, a.R = 128, n.exports = a;
    }, { 23: 23, 25: 25, 40: 40, 42: 42, 94: 94 }], 34: [function (t, n, r) {
        var e = t(128)("match");n.exports = function (t) {
            var n = /./;try {
                "/./"[t](n);
            } catch (r) {
                try {
                    return n[e] = !1, !"/./"[t](n);
                } catch (t) {}
            }return !0;
        };
    }, { 128: 128 }], 35: [function (t, n, r) {
        n.exports = function (t) {
            try {
                return !!t();
            } catch (t) {
                return !0;
            }
        };
    }, {}], 36: [function (t, n, r) {
        "use strict";
        var e = t(42),
            i = t(94),
            o = t(35),
            u = t(28),
            c = t(128);n.exports = function (t, n, r) {
            var a = c(t),
                f = r(u, a, ""[t]),
                s = f[0],
                l = f[1];o(function () {
                var n = {};return n[a] = function () {
                    return 7;
                }, 7 != ""[t](n);
            }) && (i(String.prototype, t, s), e(RegExp.prototype, a, 2 == n ? function (t, n) {
                return l.call(t, this, n);
            } : function (t) {
                return l.call(t, this);
            }));
        };
    }, { 128: 128, 28: 28, 35: 35, 42: 42, 94: 94 }], 37: [function (t, n, r) {
        "use strict";
        var e = t(7);n.exports = function () {
            var t = e(this),
                n = "";return t.global && (n += "g"), t.ignoreCase && (n += "i"), t.multiline && (n += "m"), t.unicode && (n += "u"), t.sticky && (n += "y"), n;
        };
    }, { 7: 7 }], 38: [function (t, n, r) {
        "use strict";
        function flattenIntoArray(t, n, r, a, f, s, l, h) {
            for (var v, p, d = f, y = 0, g = !!l && u(l, h, 3); y < a;) {
                if (y in r) {
                    if (v = g ? g(r[y], y, n) : r[y], p = !1, i(v) && (p = v[c], p = void 0 !== p ? !!p : e(v)), p && s > 0) d = flattenIntoArray(t, n, v, o(v.length), d, s - 1) - 1;else {
                        if (d >= 9007199254740991) throw TypeError();t[d] = v;
                    }d++;
                }y++;
            }return d;
        }var e = t(49),
            i = t(51),
            o = t(118),
            u = t(25),
            c = t(128)("isConcatSpreadable");n.exports = flattenIntoArray;
    }, { 118: 118, 128: 128, 25: 25, 49: 49, 51: 51 }], 39: [function (t, n, r) {
        var e = t(25),
            i = t(53),
            o = t(48),
            u = t(7),
            c = t(118),
            a = t(129),
            f = {},
            s = {},
            r = n.exports = function (t, n, r, l, h) {
            var v,
                p,
                d,
                y,
                g = h ? function () {
                return t;
            } : a(t),
                m = e(r, l, n ? 2 : 1),
                b = 0;if ("function" != typeof g) throw TypeError(t + " is not iterable!");if (o(g)) {
                for (v = c(t.length); v > b; b++) {
                    if ((y = n ? m(u(p = t[b])[0], p[1]) : m(t[b])) === f || y === s) return y;
                }
            } else for (d = g.call(t); !(p = d.next()).done;) {
                if ((y = i(d, m, p.value, n)) === f || y === s) return y;
            }
        };r.BREAK = f, r.RETURN = s;
    }, { 118: 118, 129: 129, 25: 25, 48: 48, 53: 53, 7: 7 }], 40: [function (t, n, r) {
        var e = n.exports = "undefined" != typeof window && window.Math == Math ? window : "undefined" != typeof self && self.Math == Math ? self : Function("return this")();"number" == typeof __g && (__g = e);
    }, {}], 41: [function (t, n, r) {
        var e = {}.hasOwnProperty;n.exports = function (t, n) {
            return e.call(t, n);
        };
    }, {}], 42: [function (t, n, r) {
        var e = t(72),
            i = t(92);n.exports = t(29) ? function (t, n, r) {
            return e.f(t, n, i(1, r));
        } : function (t, n, r) {
            return t[n] = r, t;
        };
    }, { 29: 29, 72: 72, 92: 92 }], 43: [function (t, n, r) {
        var e = t(40).document;n.exports = e && e.documentElement;
    }, { 40: 40 }], 44: [function (t, n, r) {
        n.exports = !t(29) && !t(35)(function () {
            return 7 != Object.defineProperty(t(30)("div"), "a", { get: function get() {
                    return 7;
                } }).a;
        });
    }, { 29: 29, 30: 30, 35: 35 }], 45: [function (t, n, r) {
        var e = t(51),
            i = t(99).set;n.exports = function (t, n, r) {
            var o,
                u = n.constructor;return u !== r && "function" == typeof u && (o = u.prototype) !== r.prototype && e(o) && i && i(t, o), t;
        };
    }, { 51: 51, 99: 99 }], 46: [function (t, n, r) {
        n.exports = function (t, n, r) {
            var e = void 0 === r;switch (n.length) {case 0:
                    return e ? t() : t.call(r);case 1:
                    return e ? t(n[0]) : t.call(r, n[0]);case 2:
                    return e ? t(n[0], n[1]) : t.call(r, n[0], n[1]);case 3:
                    return e ? t(n[0], n[1], n[2]) : t.call(r, n[0], n[1], n[2]);case 4:
                    return e ? t(n[0], n[1], n[2], n[3]) : t.call(r, n[0], n[1], n[2], n[3]);}return t.apply(r, n);
        };
    }, {}], 47: [function (t, n, r) {
        var e = t(18);n.exports = Object("z").propertyIsEnumerable(0) ? Object : function (t) {
            return "String" == e(t) ? t.split("") : Object(t);
        };
    }, { 18: 18 }], 48: [function (t, n, r) {
        var e = t(58),
            i = t(128)("iterator"),
            o = Array.prototype;n.exports = function (t) {
            return void 0 !== t && (e.Array === t || o[i] === t);
        };
    }, { 128: 128, 58: 58 }], 49: [function (t, n, r) {
        var e = t(18);n.exports = Array.isArray || function isArray(t) {
            return "Array" == e(t);
        };
    }, { 18: 18 }], 50: [function (t, n, r) {
        var e = t(51),
            i = Math.floor;n.exports = function isInteger(t) {
            return !e(t) && isFinite(t) && i(t) === t;
        };
    }, { 51: 51 }], 51: [function (t, n, r) {
        n.exports = function (t) {
            return "object" == (typeof t === "undefined" ? "undefined" : _typeof(t)) ? null !== t : "function" == typeof t;
        };
    }, {}], 52: [function (t, n, r) {
        var e = t(51),
            i = t(18),
            o = t(128)("match");n.exports = function (t) {
            var n;return e(t) && (void 0 !== (n = t[o]) ? !!n : "RegExp" == i(t));
        };
    }, { 128: 128, 18: 18, 51: 51 }], 53: [function (t, n, r) {
        var e = t(7);n.exports = function (t, n, r, i) {
            try {
                return i ? n(e(r)[0], r[1]) : n(r);
            } catch (n) {
                var o = t.return;throw void 0 !== o && e(o.call(t)), n;
            }
        };
    }, { 7: 7 }], 54: [function (t, n, r) {
        "use strict";
        var e = t(71),
            i = t(92),
            o = t(101),
            u = {};t(42)(u, t(128)("iterator"), function () {
            return this;
        }), n.exports = function (t, n, r) {
            t.prototype = e(u, { next: i(1, r) }), o(t, n + " Iterator");
        };
    }, { 101: 101, 128: 128, 42: 42, 71: 71, 92: 92 }], 55: [function (t, n, r) {
        "use strict";
        var e = t(60),
            i = t(33),
            o = t(94),
            u = t(42),
            c = t(41),
            a = t(58),
            f = t(54),
            s = t(101),
            l = t(79),
            h = t(128)("iterator"),
            v = !([].keys && "next" in [].keys()),
            p = function p() {
            return this;
        };n.exports = function (t, n, r, d, y, g, m) {
            f(r, n, d);var b,
                x,
                S,
                w = function w(t) {
                if (!v && t in P) return P[t];switch (t) {case "keys":
                        return function keys() {
                            return new r(this, t);
                        };case "values":
                        return function values() {
                            return new r(this, t);
                        };}return function entries() {
                    return new r(this, t);
                };
            },
                _ = n + " Iterator",
                E = "values" == y,
                O = !1,
                P = t.prototype,
                M = P[h] || P["@@iterator"] || y && P[y],
                F = M || w(y),
                I = y ? E ? w("entries") : F : void 0,
                A = "Array" == n ? P.entries || M : M;if (A && (S = l(A.call(new t()))) !== Object.prototype && S.next && (s(S, _, !0), e || c(S, h) || u(S, h, p)), E && M && "values" !== M.name && (O = !0, F = function values() {
                return M.call(this);
            }), e && !m || !v && !O && P[h] || u(P, h, F), a[n] = F, a[_] = p, y) if (b = { values: E ? F : w("values"), keys: g ? F : w("keys"), entries: I }, m) for (x in b) {
                x in P || o(P, x, b[x]);
            } else i(i.P + i.F * (v || O), n, b);return b;
        };
    }, { 101: 101, 128: 128, 33: 33, 41: 41, 42: 42, 54: 54, 58: 58, 60: 60, 79: 79, 94: 94 }], 56: [function (t, n, r) {
        var e = t(128)("iterator"),
            i = !1;try {
            var o = [7][e]();o.return = function () {
                i = !0;
            }, Array.from(o, function () {
                throw 2;
            });
        } catch (t) {}n.exports = function (t, n) {
            if (!n && !i) return !1;var r = !1;try {
                var o = [7],
                    u = o[e]();u.next = function () {
                    return { done: r = !0 };
                }, o[e] = function () {
                    return u;
                }, t(o);
            } catch (t) {}return r;
        };
    }, { 128: 128 }], 57: [function (t, n, r) {
        n.exports = function (t, n) {
            return { value: n, done: !!t };
        };
    }, {}], 58: [function (t, n, r) {
        n.exports = {};
    }, {}], 59: [function (t, n, r) {
        var e = t(81),
            i = t(117);n.exports = function (t, n) {
            for (var r, o = i(t), u = e(o), c = u.length, a = 0; c > a;) {
                if (o[r = u[a++]] === n) return r;
            }
        };
    }, { 117: 117, 81: 81 }], 60: [function (t, n, r) {
        n.exports = !1;
    }, {}], 61: [function (t, n, r) {
        var e = Math.expm1;n.exports = !e || e(10) > 22025.465794806718 || e(10) < 22025.465794806718 || -2e-17 != e(-2e-17) ? function expm1(t) {
            return 0 == (t = +t) ? t : t > -1e-6 && t < 1e-6 ? t + t * t / 2 : Math.exp(t) - 1;
        } : e;
    }, {}], 62: [function (t, n, r) {
        var e = t(65),
            i = Math.pow,
            o = i(2, -52),
            u = i(2, -23),
            c = i(2, 127) * (2 - u),
            a = i(2, -126),
            f = function f(t) {
            return t + 1 / o - 1 / o;
        };n.exports = Math.fround || function fround(t) {
            var n,
                r,
                i = Math.abs(t),
                s = e(t);return i < a ? s * f(i / a / u) * a * u : (n = (1 + u / o) * i, r = n - (n - i), r > c || r != r ? s * (1 / 0) : s * r);
        };
    }, { 65: 65 }], 63: [function (t, n, r) {
        n.exports = Math.log1p || function log1p(t) {
            return (t = +t) > -1e-8 && t < 1e-8 ? t - t * t / 2 : Math.log(1 + t);
        };
    }, {}], 64: [function (t, n, r) {
        n.exports = Math.scale || function scale(t, n, r, e, i) {
            return 0 === arguments.length || t != t || n != n || r != r || e != e || i != i ? NaN : t === 1 / 0 || t === -1 / 0 ? t : (t - n) * (i - e) / (r - n) + e;
        };
    }, {}], 65: [function (t, n, r) {
        n.exports = Math.sign || function sign(t) {
            return 0 == (t = +t) || t != t ? t : t < 0 ? -1 : 1;
        };
    }, {}], 66: [function (t, n, r) {
        var e = t(124)("meta"),
            i = t(51),
            o = t(41),
            u = t(72).f,
            c = 0,
            a = Object.isExtensible || function () {
            return !0;
        },
            f = !t(35)(function () {
            return a(Object.preventExtensions({}));
        }),
            s = function s(t) {
            u(t, e, { value: { i: "O" + ++c, w: {} } });
        },
            l = function l(t, n) {
            if (!i(t)) return "symbol" == (typeof t === "undefined" ? "undefined" : _typeof(t)) ? t : ("string" == typeof t ? "S" : "P") + t;if (!o(t, e)) {
                if (!a(t)) return "F";if (!n) return "E";s(t);
            }return t[e].i;
        },
            h = function h(t, n) {
            if (!o(t, e)) {
                if (!a(t)) return !0;if (!n) return !1;s(t);
            }return t[e].w;
        },
            v = function v(t) {
            return f && p.NEED && a(t) && !o(t, e) && s(t), t;
        },
            p = n.exports = { KEY: e, NEED: !1, fastKey: l, getWeak: h, onFreeze: v };
    }, { 124: 124, 35: 35, 41: 41, 51: 51, 72: 72 }], 67: [function (t, n, r) {
        var e = t(160),
            i = t(33),
            o = t(103)("metadata"),
            u = o.store || (o.store = new (t(266))()),
            c = function c(t, n, r) {
            var i = u.get(t);if (!i) {
                if (!r) return;u.set(t, i = new e());
            }var o = i.get(n);if (!o) {
                if (!r) return;i.set(n, o = new e());
            }return o;
        },
            a = function a(t, n, r) {
            var e = c(n, r, !1);return void 0 !== e && e.has(t);
        },
            f = function f(t, n, r) {
            var e = c(n, r, !1);return void 0 === e ? void 0 : e.get(t);
        },
            s = function s(t, n, r, e) {
            c(r, e, !0).set(t, n);
        },
            l = function l(t, n) {
            var r = c(t, n, !1),
                e = [];return r && r.forEach(function (t, n) {
                e.push(n);
            }), e;
        },
            h = function h(t) {
            return void 0 === t || "symbol" == (typeof t === "undefined" ? "undefined" : _typeof(t)) ? t : String(t);
        },
            v = function v(t) {
            i(i.S, "Reflect", t);
        };n.exports = { store: u, map: c, has: a, get: f, set: s, keys: l, key: h, exp: v };
    }, { 103: 103, 160: 160, 266: 266, 33: 33 }], 68: [function (t, n, r) {
        var e = t(40),
            i = t(113).set,
            o = e.MutationObserver || e.WebKitMutationObserver,
            u = e.process,
            c = e.Promise,
            a = "process" == t(18)(u);n.exports = function () {
            var t,
                n,
                r,
                f = function f() {
                var e, i;for (a && (e = u.domain) && e.exit(); t;) {
                    i = t.fn, t = t.next;try {
                        i();
                    } catch (e) {
                        throw t ? r() : n = void 0, e;
                    }
                }n = void 0, e && e.enter();
            };if (a) r = function r() {
                u.nextTick(f);
            };else if (o) {
                var s = !0,
                    l = document.createTextNode("");new o(f).observe(l, { characterData: !0 }), r = function r() {
                    l.data = s = !s;
                };
            } else if (c && c.resolve) {
                var h = c.resolve();r = function r() {
                    h.then(f);
                };
            } else r = function r() {
                i.call(e, f);
            };return function (e) {
                var i = { fn: e, next: void 0 };n && (n.next = i), t || (t = i, r()), n = i;
            };
        };
    }, { 113: 113, 18: 18, 40: 40 }], 69: [function (t, n, r) {
        "use strict";
        function PromiseCapability(t) {
            var n, r;this.promise = new t(function (t, e) {
                if (void 0 !== n || void 0 !== r) throw TypeError("Bad Promise constructor");n = t, r = e;
            }), this.resolve = e(n), this.reject = e(r);
        }var e = t(3);n.exports.f = function (t) {
            return new PromiseCapability(t);
        };
    }, { 3: 3 }], 70: [function (t, n, r) {
        "use strict";
        var e = t(81),
            i = t(78),
            o = t(82),
            u = t(119),
            c = t(47),
            a = Object.assign;n.exports = !a || t(35)(function () {
            var t = {},
                n = {},
                r = Symbol(),
                e = "abcdefghijklmnopqrst";return t[r] = 7, e.split("").forEach(function (t) {
                n[t] = t;
            }), 7 != a({}, t)[r] || Object.keys(a({}, n)).join("") != e;
        }) ? function assign(t, n) {
            for (var r = u(t), a = arguments.length, f = 1, s = i.f, l = o.f; a > f;) {
                for (var h, v = c(arguments[f++]), p = s ? e(v).concat(s(v)) : e(v), d = p.length, y = 0; d > y;) {
                    l.call(v, h = p[y++]) && (r[h] = v[h]);
                }
            }return r;
        } : a;
    }, { 119: 119, 35: 35, 47: 47, 78: 78, 81: 81, 82: 82 }], 71: [function (t, n, r) {
        var e = t(7),
            i = t(73),
            o = t(31),
            u = t(102)("IE_PROTO"),
            c = function c() {},
            _a = function a() {
            var n,
                r = t(30)("iframe"),
                e = o.length;for (r.style.display = "none", t(43).appendChild(r), r.src = "javascript:", n = r.contentWindow.document, n.open(), n.write("<script>document.F=Object<\/script>"), n.close(), _a = n.F; e--;) {
                delete _a.prototype[o[e]];
            }return _a();
        };n.exports = Object.create || function create(t, n) {
            var r;return null !== t ? (c.prototype = e(t), r = new c(), c.prototype = null, r[u] = t) : r = _a(), void 0 === n ? r : i(r, n);
        };
    }, { 102: 102, 30: 30, 31: 31, 43: 43, 7: 7, 73: 73 }], 72: [function (t, n, r) {
        var e = t(7),
            i = t(44),
            o = t(120),
            u = Object.defineProperty;r.f = t(29) ? Object.defineProperty : function defineProperty(t, n, r) {
            if (e(t), n = o(n, !0), e(r), i) try {
                return u(t, n, r);
            } catch (t) {}if ("get" in r || "set" in r) throw TypeError("Accessors not supported!");return "value" in r && (t[n] = r.value), t;
        };
    }, { 120: 120, 29: 29, 44: 44, 7: 7 }], 73: [function (t, n, r) {
        var e = t(72),
            i = t(7),
            o = t(81);n.exports = t(29) ? Object.defineProperties : function defineProperties(t, n) {
            i(t);for (var r, u = o(n), c = u.length, a = 0; c > a;) {
                e.f(t, r = u[a++], n[r]);
            }return t;
        };
    }, { 29: 29, 7: 7, 72: 72, 81: 81 }], 74: [function (t, n, r) {
        "use strict";
        n.exports = t(60) || !t(35)(function () {
            var n = Math.random();__defineSetter__.call(null, n, function () {}), delete t(40)[n];
        });
    }, { 35: 35, 40: 40, 60: 60 }], 75: [function (t, n, r) {
        var e = t(82),
            i = t(92),
            o = t(117),
            u = t(120),
            c = t(41),
            a = t(44),
            f = Object.getOwnPropertyDescriptor;r.f = t(29) ? f : function getOwnPropertyDescriptor(t, n) {
            if (t = o(t), n = u(n, !0), a) try {
                return f(t, n);
            } catch (t) {}if (c(t, n)) return i(!e.f.call(t, n), t[n]);
        };
    }, { 117: 117, 120: 120, 29: 29, 41: 41, 44: 44, 82: 82, 92: 92 }], 76: [function (t, n, r) {
        var e = t(117),
            i = t(77).f,
            o = {}.toString,
            u = "object" == (typeof window === "undefined" ? "undefined" : _typeof(window)) && window && Object.getOwnPropertyNames ? Object.getOwnPropertyNames(window) : [],
            c = function c(t) {
            try {
                return i(t);
            } catch (t) {
                return u.slice();
            }
        };n.exports.f = function getOwnPropertyNames(t) {
            return u && "[object Window]" == o.call(t) ? c(t) : i(e(t));
        };
    }, { 117: 117, 77: 77 }], 77: [function (t, n, r) {
        var e = t(80),
            i = t(31).concat("length", "prototype");r.f = Object.getOwnPropertyNames || function getOwnPropertyNames(t) {
            return e(t, i);
        };
    }, { 31: 31, 80: 80 }], 78: [function (t, n, r) {
        r.f = Object.getOwnPropertySymbols;
    }, {}], 79: [function (t, n, r) {
        var e = t(41),
            i = t(119),
            o = t(102)("IE_PROTO"),
            u = Object.prototype;n.exports = Object.getPrototypeOf || function (t) {
            return t = i(t), e(t, o) ? t[o] : "function" == typeof t.constructor && t instanceof t.constructor ? t.constructor.prototype : t instanceof Object ? u : null;
        };
    }, { 102: 102, 119: 119, 41: 41 }], 80: [function (t, n, r) {
        var e = t(41),
            i = t(117),
            o = t(11)(!1),
            u = t(102)("IE_PROTO");n.exports = function (t, n) {
            var r,
                c = i(t),
                a = 0,
                f = [];for (r in c) {
                r != u && e(c, r) && f.push(r);
            }for (; n.length > a;) {
                e(c, r = n[a++]) && (~o(f, r) || f.push(r));
            }return f;
        };
    }, { 102: 102, 11: 11, 117: 117, 41: 41 }], 81: [function (t, n, r) {
        var e = t(80),
            i = t(31);n.exports = Object.keys || function keys(t) {
            return e(t, i);
        };
    }, { 31: 31, 80: 80 }], 82: [function (t, n, r) {
        r.f = {}.propertyIsEnumerable;
    }, {}], 83: [function (t, n, r) {
        var e = t(33),
            i = t(23),
            o = t(35);n.exports = function (t, n) {
            var r = (i.Object || {})[t] || Object[t],
                u = {};u[t] = n(r), e(e.S + e.F * o(function () {
                r(1);
            }), "Object", u);
        };
    }, { 23: 23, 33: 33, 35: 35 }], 84: [function (t, n, r) {
        var e = t(81),
            i = t(117),
            o = t(82).f;n.exports = function (t) {
            return function (n) {
                for (var r, u = i(n), c = e(u), a = c.length, f = 0, s = []; a > f;) {
                    o.call(u, r = c[f++]) && s.push(t ? [r, u[r]] : u[r]);
                }return s;
            };
        };
    }, { 117: 117, 81: 81, 82: 82 }], 85: [function (t, n, r) {
        var e = t(77),
            i = t(78),
            o = t(7),
            u = t(40).Reflect;n.exports = u && u.ownKeys || function ownKeys(t) {
            var n = e.f(o(t)),
                r = i.f;return r ? n.concat(r(t)) : n;
        };
    }, { 40: 40, 7: 7, 77: 77, 78: 78 }], 86: [function (t, n, r) {
        var e = t(40).parseFloat,
            i = t(111).trim;n.exports = 1 / e(t(112) + "-0") != -1 / 0 ? function parseFloat(t) {
            var n = i(String(t), 3),
                r = e(n);return 0 === r && "-" == n.charAt(0) ? -0 : r;
        } : e;
    }, { 111: 111, 112: 112, 40: 40 }], 87: [function (t, n, r) {
        var e = t(40).parseInt,
            i = t(111).trim,
            o = t(112),
            u = /^[-+]?0[xX]/;n.exports = 8 !== e(o + "08") || 22 !== e(o + "0x16") ? function parseInt(t, n) {
            var r = i(String(t), 3);return e(r, n >>> 0 || (u.test(r) ? 16 : 10));
        } : e;
    }, { 111: 111, 112: 112, 40: 40 }], 88: [function (t, n, r) {
        "use strict";
        var e = t(89),
            i = t(46),
            o = t(3);n.exports = function () {
            for (var t = o(this), n = arguments.length, r = Array(n), u = 0, c = e._, a = !1; n > u;) {
                (r[u] = arguments[u++]) === c && (a = !0);
            }return function () {
                var e,
                    o = this,
                    u = arguments.length,
                    f = 0,
                    s = 0;if (!a && !u) return i(t, r, o);if (e = r.slice(), a) for (; n > f; f++) {
                    e[f] === c && (e[f] = arguments[s++]);
                }for (; u > s;) {
                    e.push(arguments[s++]);
                }return i(t, e, o);
            };
        };
    }, { 3: 3, 46: 46, 89: 89 }], 89: [function (t, n, r) {
        n.exports = t(40);
    }, { 40: 40 }], 90: [function (t, n, r) {
        n.exports = function (t) {
            try {
                return { e: !1, v: t() };
            } catch (t) {
                return { e: !0, v: t };
            }
        };
    }, {}], 91: [function (t, n, r) {
        var e = t(69);n.exports = function (t, n) {
            var r = e.f(t);return (0, r.resolve)(n), r.promise;
        };
    }, { 69: 69 }], 92: [function (t, n, r) {
        n.exports = function (t, n) {
            return { enumerable: !(1 & t), configurable: !(2 & t), writable: !(4 & t), value: n };
        };
    }, {}], 93: [function (t, n, r) {
        var e = t(94);n.exports = function (t, n, r) {
            for (var i in n) {
                e(t, i, n[i], r);
            }return t;
        };
    }, { 94: 94 }], 94: [function (t, n, r) {
        var e = t(40),
            i = t(42),
            o = t(41),
            u = t(124)("src"),
            c = Function.toString,
            a = ("" + c).split("toString");t(23).inspectSource = function (t) {
            return c.call(t);
        }, (n.exports = function (t, n, r, c) {
            var f = "function" == typeof r;f && (o(r, "name") || i(r, "name", n)), t[n] !== r && (f && (o(r, u) || i(r, u, t[n] ? "" + t[n] : a.join(String(n)))), t === e ? t[n] = r : c ? t[n] ? t[n] = r : i(t, n, r) : (delete t[n], i(t, n, r)));
        })(Function.prototype, "toString", function toString() {
            return "function" == typeof this && this[u] || c.call(this);
        });
    }, { 124: 124, 23: 23, 40: 40, 41: 41, 42: 42 }], 95: [function (t, n, r) {
        n.exports = function (t, n) {
            var r = n === Object(n) ? function (t) {
                return n[t];
            } : n;return function (n) {
                return String(n).replace(t, r);
            };
        };
    }, {}], 96: [function (t, n, r) {
        n.exports = Object.is || function is(t, n) {
            return t === n ? 0 !== t || 1 / t == 1 / n : t != t && n != n;
        };
    }, {}], 97: [function (t, n, r) {
        "use strict";
        var e = t(33),
            i = t(3),
            o = t(25),
            u = t(39);n.exports = function (t) {
            e(e.S, t, { from: function from(t) {
                    var n,
                        r,
                        e,
                        c,
                        a = arguments[1];return i(this), n = void 0 !== a, n && i(a), void 0 == t ? new this() : (r = [], n ? (e = 0, c = o(a, arguments[2], 2), u(t, !1, function (t) {
                        r.push(c(t, e++));
                    })) : u(t, !1, r.push, r), new this(r));
                } });
        };
    }, { 25: 25, 3: 3, 33: 33, 39: 39 }], 98: [function (t, n, r) {
        "use strict";
        var e = t(33);n.exports = function (t) {
            e(e.S, t, { of: function of() {
                    for (var t = arguments.length, n = Array(t); t--;) {
                        n[t] = arguments[t];
                    }return new this(n);
                } });
        };
    }, { 33: 33 }], 99: [function (t, n, r) {
        var e = t(51),
            i = t(7),
            o = function o(t, n) {
            if (i(t), !e(n) && null !== n) throw TypeError(n + ": can't set as prototype!");
        };n.exports = { set: Object.setPrototypeOf || ("__proto__" in {} ? function (n, r, e) {
                try {
                    e = t(25)(Function.call, t(75).f(Object.prototype, "__proto__").set, 2), e(n, []), r = !(n instanceof Array);
                } catch (t) {
                    r = !0;
                }return function setPrototypeOf(t, n) {
                    return o(t, n), r ? t.__proto__ = n : e(t, n), t;
                };
            }({}, !1) : void 0), check: o };
    }, { 25: 25, 51: 51, 7: 7, 75: 75 }], 100: [function (t, n, r) {
        "use strict";
        var e = t(40),
            i = t(72),
            o = t(29),
            u = t(128)("species");n.exports = function (t) {
            var n = e[t];o && n && !n[u] && i.f(n, u, { configurable: !0, get: function get() {
                    return this;
                } });
        };
    }, { 128: 128, 29: 29, 40: 40, 72: 72 }], 101: [function (t, n, r) {
        var e = t(72).f,
            i = t(41),
            o = t(128)("toStringTag");n.exports = function (t, n, r) {
            t && !i(t = r ? t : t.prototype, o) && e(t, o, { configurable: !0, value: n });
        };
    }, { 128: 128, 41: 41, 72: 72 }], 102: [function (t, n, r) {
        var e = t(103)("keys"),
            i = t(124);n.exports = function (t) {
            return e[t] || (e[t] = i(t));
        };
    }, { 103: 103, 124: 124 }], 103: [function (t, n, r) {
        var e = t(40),
            i = e["__core-js_shared__"] || (e["__core-js_shared__"] = {});n.exports = function (t) {
            return i[t] || (i[t] = {});
        };
    }, { 40: 40 }], 104: [function (t, n, r) {
        var e = t(7),
            i = t(3),
            o = t(128)("species");n.exports = function (t, n) {
            var r,
                u = e(t).constructor;return void 0 === u || void 0 == (r = e(u)[o]) ? n : i(r);
        };
    }, { 128: 128, 3: 3, 7: 7 }], 105: [function (t, n, r) {
        "use strict";
        var e = t(35);n.exports = function (t, n) {
            return !!t && e(function () {
                n ? t.call(null, function () {}, 1) : t.call(null);
            });
        };
    }, { 35: 35 }], 106: [function (t, n, r) {
        var e = t(116),
            i = t(28);n.exports = function (t) {
            return function (n, r) {
                var o,
                    u,
                    c = String(i(n)),
                    a = e(r),
                    f = c.length;return a < 0 || a >= f ? t ? "" : void 0 : (o = c.charCodeAt(a), o < 55296 || o > 56319 || a + 1 === f || (u = c.charCodeAt(a + 1)) < 56320 || u > 57343 ? t ? c.charAt(a) : o : t ? c.slice(a, a + 2) : u - 56320 + (o - 55296 << 10) + 65536);
            };
        };
    }, { 116: 116, 28: 28 }], 107: [function (t, n, r) {
        var e = t(52),
            i = t(28);n.exports = function (t, n, r) {
            if (e(n)) throw TypeError("String#" + r + " doesn't accept regex!");return String(i(t));
        };
    }, { 28: 28, 52: 52 }], 108: [function (t, n, r) {
        var e = t(33),
            i = t(35),
            o = t(28),
            u = /"/g,
            c = function c(t, n, r, e) {
            var i = String(o(t)),
                c = "<" + n;return "" !== r && (c += " " + r + '="' + String(e).replace(u, "&quot;") + '"'), c + ">" + i + "</" + n + ">";
        };n.exports = function (t, n) {
            var r = {};r[t] = n(c), e(e.P + e.F * i(function () {
                var n = ""[t]('"');return n !== n.toLowerCase() || n.split('"').length > 3;
            }), "String", r);
        };
    }, { 28: 28, 33: 33, 35: 35 }], 109: [function (t, n, r) {
        var e = t(118),
            i = t(110),
            o = t(28);n.exports = function (t, n, r, u) {
            var c = String(o(t)),
                a = c.length,
                f = void 0 === r ? " " : String(r),
                s = e(n);if (s <= a || "" == f) return c;var l = s - a,
                h = i.call(f, Math.ceil(l / f.length));return h.length > l && (h = h.slice(0, l)), u ? h + c : c + h;
        };
    }, { 110: 110, 118: 118, 28: 28 }], 110: [function (t, n, r) {
        "use strict";
        var e = t(116),
            i = t(28);n.exports = function repeat(t) {
            var n = String(i(this)),
                r = "",
                o = e(t);if (o < 0 || o == 1 / 0) throw RangeError("Count can't be negative");for (; o > 0; (o >>>= 1) && (n += n)) {
                1 & o && (r += n);
            }return r;
        };
    }, { 116: 116, 28: 28 }], 111: [function (t, n, r) {
        var e = t(33),
            i = t(28),
            o = t(35),
            u = t(112),
            c = "[" + u + "]",
            a = "​",
            f = RegExp("^" + c + c + "*"),
            s = RegExp(c + c + "*$"),
            l = function l(t, n, r) {
            var i = {},
                c = o(function () {
                return !!u[t]() || a[t]() != a;
            }),
                f = i[t] = c ? n(h) : u[t];r && (i[r] = f), e(e.P + e.F * c, "String", i);
        },
            h = l.trim = function (t, n) {
            return t = String(i(t)), 1 & n && (t = t.replace(f, "")), 2 & n && (t = t.replace(s, "")), t;
        };n.exports = l;
    }, { 112: 112, 28: 28, 33: 33, 35: 35 }], 112: [function (t, n, r) {
        n.exports = "\t\n\x0B\f\r \xA0\u1680\u180E\u2000\u2001\u2002\u2003\u2004\u2005\u2006\u2007\u2008\u2009\u200A\u202F\u205F\u3000\u2028\u2029\uFEFF";
    }, {}], 113: [function (t, n, r) {
        var e,
            i,
            o,
            u = t(25),
            c = t(46),
            a = t(43),
            f = t(30),
            s = t(40),
            l = s.process,
            h = s.setImmediate,
            v = s.clearImmediate,
            p = s.MessageChannel,
            d = s.Dispatch,
            y = 0,
            g = {},
            m = function m() {
            var t = +this;if (g.hasOwnProperty(t)) {
                var n = g[t];delete g[t], n();
            }
        },
            b = function b(t) {
            m.call(t.data);
        };h && v || (h = function setImmediate(t) {
            for (var n = [], r = 1; arguments.length > r;) {
                n.push(arguments[r++]);
            }return g[++y] = function () {
                c("function" == typeof t ? t : Function(t), n);
            }, e(y), y;
        }, v = function clearImmediate(t) {
            delete g[t];
        }, "process" == t(18)(l) ? e = function e(t) {
            l.nextTick(u(m, t, 1));
        } : d && d.now ? e = function e(t) {
            d.now(u(m, t, 1));
        } : p ? (i = new p(), o = i.port2, i.port1.onmessage = b, e = u(o.postMessage, o, 1)) : s.addEventListener && "function" == typeof postMessage && !s.importScripts ? (e = function e(t) {
            s.postMessage(t + "", "*");
        }, s.addEventListener("message", b, !1)) : e = "onreadystatechange" in f("script") ? function (t) {
            a.appendChild(f("script")).onreadystatechange = function () {
                a.removeChild(this), m.call(t);
            };
        } : function (t) {
            setTimeout(u(m, t, 1), 0);
        }), n.exports = { set: h, clear: v };
    }, { 18: 18, 25: 25, 30: 30, 40: 40, 43: 43, 46: 46 }], 114: [function (t, n, r) {
        var e = t(116),
            i = Math.max,
            o = Math.min;n.exports = function (t, n) {
            return t = e(t), t < 0 ? i(t + n, 0) : o(t, n);
        };
    }, { 116: 116 }], 115: [function (t, n, r) {
        var e = t(116),
            i = t(118);n.exports = function (t) {
            if (void 0 === t) return 0;var n = e(t),
                r = i(n);if (n !== r) throw RangeError("Wrong length!");return r;
        };
    }, { 116: 116, 118: 118 }], 116: [function (t, n, r) {
        var e = Math.ceil,
            i = Math.floor;n.exports = function (t) {
            return isNaN(t = +t) ? 0 : (t > 0 ? i : e)(t);
        };
    }, {}], 117: [function (t, n, r) {
        var e = t(47),
            i = t(28);n.exports = function (t) {
            return e(i(t));
        };
    }, { 28: 28, 47: 47 }], 118: [function (t, n, r) {
        var e = t(116),
            i = Math.min;n.exports = function (t) {
            return t > 0 ? i(e(t), 9007199254740991) : 0;
        };
    }, { 116: 116 }],
    119: [function (t, n, r) {
        var e = t(28);n.exports = function (t) {
            return Object(e(t));
        };
    }, { 28: 28 }], 120: [function (t, n, r) {
        var e = t(51);n.exports = function (t, n) {
            if (!e(t)) return t;var r, i;if (n && "function" == typeof (r = t.toString) && !e(i = r.call(t))) return i;if ("function" == typeof (r = t.valueOf) && !e(i = r.call(t))) return i;if (!n && "function" == typeof (r = t.toString) && !e(i = r.call(t))) return i;throw TypeError("Can't convert object to primitive value");
        };
    }, { 51: 51 }], 121: [function (t, n, r) {
        "use strict";
        if (t(29)) {
            var e = t(60),
                i = t(40),
                o = t(35),
                u = t(33),
                c = t(123),
                a = t(122),
                f = t(25),
                s = t(6),
                l = t(92),
                h = t(42),
                v = t(93),
                p = t(116),
                d = t(118),
                y = t(115),
                g = t(114),
                m = t(120),
                b = t(41),
                x = t(17),
                S = t(51),
                w = t(119),
                _ = t(48),
                E = t(71),
                O = t(79),
                P = t(77).f,
                M = t(129),
                F = t(124),
                I = t(128),
                A = t(12),
                k = t(11),
                N = t(104),
                j = t(141),
                T = t(58),
                R = t(56),
                L = t(100),
                G = t(9),
                D = t(8),
                C = t(72),
                W = t(75),
                U = C.f,
                B = W.f,
                V = i.RangeError,
                z = i.TypeError,
                q = i.Uint8Array,
                K = Array.prototype,
                Y = a.ArrayBuffer,
                J = a.DataView,
                H = A(0),
                X = A(2),
                $ = A(3),
                Z = A(4),
                Q = A(5),
                tt = A(6),
                nt = k(!0),
                rt = k(!1),
                et = j.values,
                it = j.keys,
                ot = j.entries,
                ut = K.lastIndexOf,
                ct = K.reduce,
                at = K.reduceRight,
                ft = K.join,
                st = K.sort,
                lt = K.slice,
                ht = K.toString,
                vt = K.toLocaleString,
                pt = I("iterator"),
                dt = I("toStringTag"),
                yt = F("typed_constructor"),
                gt = F("def_constructor"),
                mt = c.CONSTR,
                bt = c.TYPED,
                xt = c.VIEW,
                St = A(1, function (t, n) {
                return Pt(N(t, t[gt]), n);
            }),
                wt = o(function () {
                return 1 === new q(new Uint16Array([1]).buffer)[0];
            }),
                _t = !!q && !!q.prototype.set && o(function () {
                new q(1).set({});
            }),
                Et = function Et(t, n) {
                var r = p(t);if (r < 0 || r % n) throw V("Wrong offset!");return r;
            },
                Ot = function Ot(t) {
                if (S(t) && bt in t) return t;throw z(t + " is not a typed array!");
            },
                Pt = function Pt(t, n) {
                if (!(S(t) && yt in t)) throw z("It is not a typed array constructor!");return new t(n);
            },
                Mt = function Mt(t, n) {
                return Ft(N(t, t[gt]), n);
            },
                Ft = function Ft(t, n) {
                for (var r = 0, e = n.length, i = Pt(t, e); e > r;) {
                    i[r] = n[r++];
                }return i;
            },
                It = function It(t, n, r) {
                U(t, n, { get: function get() {
                        return this._d[r];
                    } });
            },
                At = function from(t) {
                var n,
                    r,
                    e,
                    i,
                    o,
                    u,
                    c = w(t),
                    a = arguments.length,
                    s = a > 1 ? arguments[1] : void 0,
                    l = void 0 !== s,
                    h = M(c);if (void 0 != h && !_(h)) {
                    for (u = h.call(c), e = [], n = 0; !(o = u.next()).done; n++) {
                        e.push(o.value);
                    }c = e;
                }for (l && a > 2 && (s = f(s, arguments[2], 2)), n = 0, r = d(c.length), i = Pt(this, r); r > n; n++) {
                    i[n] = l ? s(c[n], n) : c[n];
                }return i;
            },
                kt = function of() {
                for (var t = 0, n = arguments.length, r = Pt(this, n); n > t;) {
                    r[t] = arguments[t++];
                }return r;
            },
                Nt = !!q && o(function () {
                vt.call(new q(1));
            }),
                jt = function toLocaleString() {
                return vt.apply(Nt ? lt.call(Ot(this)) : Ot(this), arguments);
            },
                Tt = { copyWithin: function copyWithin(t, n) {
                    return D.call(Ot(this), t, n, arguments.length > 2 ? arguments[2] : void 0);
                }, every: function every(t) {
                    return Z(Ot(this), t, arguments.length > 1 ? arguments[1] : void 0);
                }, fill: function fill(t) {
                    return G.apply(Ot(this), arguments);
                }, filter: function filter(t) {
                    return Mt(this, X(Ot(this), t, arguments.length > 1 ? arguments[1] : void 0));
                }, find: function find(t) {
                    return Q(Ot(this), t, arguments.length > 1 ? arguments[1] : void 0);
                }, findIndex: function findIndex(t) {
                    return tt(Ot(this), t, arguments.length > 1 ? arguments[1] : void 0);
                }, forEach: function forEach(t) {
                    H(Ot(this), t, arguments.length > 1 ? arguments[1] : void 0);
                }, indexOf: function indexOf(t) {
                    return rt(Ot(this), t, arguments.length > 1 ? arguments[1] : void 0);
                }, includes: function includes(t) {
                    return nt(Ot(this), t, arguments.length > 1 ? arguments[1] : void 0);
                }, join: function join(t) {
                    return ft.apply(Ot(this), arguments);
                }, lastIndexOf: function lastIndexOf(t) {
                    return ut.apply(Ot(this), arguments);
                }, map: function map(t) {
                    return St(Ot(this), t, arguments.length > 1 ? arguments[1] : void 0);
                }, reduce: function reduce(t) {
                    return ct.apply(Ot(this), arguments);
                }, reduceRight: function reduceRight(t) {
                    return at.apply(Ot(this), arguments);
                }, reverse: function reverse() {
                    for (var t, n = this, r = Ot(n).length, e = Math.floor(r / 2), i = 0; i < e;) {
                        t = n[i], n[i++] = n[--r], n[r] = t;
                    }return n;
                }, some: function some(t) {
                    return $(Ot(this), t, arguments.length > 1 ? arguments[1] : void 0);
                }, sort: function sort(t) {
                    return st.call(Ot(this), t);
                }, subarray: function subarray(t, n) {
                    var r = Ot(this),
                        e = r.length,
                        i = g(t, e);return new (N(r, r[gt]))(r.buffer, r.byteOffset + i * r.BYTES_PER_ELEMENT, d((void 0 === n ? e : g(n, e)) - i));
                } },
                Rt = function slice(t, n) {
                return Mt(this, lt.call(Ot(this), t, n));
            },
                Lt = function set(t) {
                Ot(this);var n = Et(arguments[1], 1),
                    r = this.length,
                    e = w(t),
                    i = d(e.length),
                    o = 0;if (i + n > r) throw V("Wrong length!");for (; o < i;) {
                    this[n + o] = e[o++];
                }
            },
                Gt = { entries: function entries() {
                    return ot.call(Ot(this));
                }, keys: function keys() {
                    return it.call(Ot(this));
                }, values: function values() {
                    return et.call(Ot(this));
                } },
                Dt = function Dt(t, n) {
                return S(t) && t[bt] && "symbol" != (typeof n === "undefined" ? "undefined" : _typeof(n)) && n in t && String(+n) == String(n);
            },
                Ct = function getOwnPropertyDescriptor(t, n) {
                return Dt(t, n = m(n, !0)) ? l(2, t[n]) : B(t, n);
            },
                Wt = function defineProperty(t, n, r) {
                return !(Dt(t, n = m(n, !0)) && S(r) && b(r, "value")) || b(r, "get") || b(r, "set") || r.configurable || b(r, "writable") && !r.writable || b(r, "enumerable") && !r.enumerable ? U(t, n, r) : (t[n] = r.value, t);
            };mt || (W.f = Ct, C.f = Wt), u(u.S + u.F * !mt, "Object", { getOwnPropertyDescriptor: Ct, defineProperty: Wt }), o(function () {
                ht.call({});
            }) && (ht = vt = function toString() {
                return ft.call(this);
            });var Ut = v({}, Tt);v(Ut, Gt), h(Ut, pt, Gt.values), v(Ut, { slice: Rt, set: Lt, constructor: function constructor() {}, toString: ht, toLocaleString: jt }), It(Ut, "buffer", "b"), It(Ut, "byteOffset", "o"), It(Ut, "byteLength", "l"), It(Ut, "length", "e"), U(Ut, dt, { get: function get() {
                    return this[bt];
                } }), n.exports = function (t, n, r, a) {
                a = !!a;var f = t + (a ? "Clamped" : "") + "Array",
                    l = "get" + t,
                    v = "set" + t,
                    p = i[f],
                    g = p || {},
                    m = p && O(p),
                    b = !p || !c.ABV,
                    w = {},
                    _ = p && p.prototype,
                    M = function M(t, r) {
                    var e = t._d;return e.v[l](r * n + e.o, wt);
                },
                    F = function F(t, r, e) {
                    var i = t._d;a && (e = (e = Math.round(e)) < 0 ? 0 : e > 255 ? 255 : 255 & e), i.v[v](r * n + i.o, e, wt);
                },
                    I = function I(t, n) {
                    U(t, n, { get: function get() {
                            return M(this, n);
                        }, set: function set(t) {
                            return F(this, n, t);
                        }, enumerable: !0 });
                };b ? (p = r(function (t, r, e, i) {
                    s(t, p, f, "_d");var o,
                        u,
                        c,
                        a,
                        l = 0,
                        v = 0;if (S(r)) {
                        if (!(r instanceof Y || "ArrayBuffer" == (a = x(r)) || "SharedArrayBuffer" == a)) return bt in r ? Ft(p, r) : At.call(p, r);o = r, v = Et(e, n);var g = r.byteLength;if (void 0 === i) {
                            if (g % n) throw V("Wrong length!");if ((u = g - v) < 0) throw V("Wrong length!");
                        } else if ((u = d(i) * n) + v > g) throw V("Wrong length!");c = u / n;
                    } else c = y(r), u = c * n, o = new Y(u);for (h(t, "_d", { b: o, o: v, l: u, e: c, v: new J(o) }); l < c;) {
                        I(t, l++);
                    }
                }), _ = p.prototype = E(Ut), h(_, "constructor", p)) : o(function () {
                    p(1);
                }) && o(function () {
                    new p(-1);
                }) && R(function (t) {
                    new p(), new p(null), new p(1.5), new p(t);
                }, !0) || (p = r(function (t, r, e, i) {
                    s(t, p, f);var o;return S(r) ? r instanceof Y || "ArrayBuffer" == (o = x(r)) || "SharedArrayBuffer" == o ? void 0 !== i ? new g(r, Et(e, n), i) : void 0 !== e ? new g(r, Et(e, n)) : new g(r) : bt in r ? Ft(p, r) : At.call(p, r) : new g(y(r));
                }), H(m !== Function.prototype ? P(g).concat(P(m)) : P(g), function (t) {
                    t in p || h(p, t, g[t]);
                }), p.prototype = _, e || (_.constructor = p));var A = _[pt],
                    k = !!A && ("values" == A.name || void 0 == A.name),
                    N = Gt.values;h(p, yt, !0), h(_, bt, f), h(_, xt, !0), h(_, gt, p), (a ? new p(1)[dt] == f : dt in _) || U(_, dt, { get: function get() {
                        return f;
                    } }), w[f] = p, u(u.G + u.W + u.F * (p != g), w), u(u.S, f, { BYTES_PER_ELEMENT: n }), u(u.S + u.F * o(function () {
                    g.of.call(p, 1);
                }), f, { from: At, of: kt }), "BYTES_PER_ELEMENT" in _ || h(_, "BYTES_PER_ELEMENT", n), u(u.P, f, Tt), L(f), u(u.P + u.F * _t, f, { set: Lt }), u(u.P + u.F * !k, f, Gt), e || _.toString == ht || (_.toString = ht), u(u.P + u.F * o(function () {
                    new p(1).slice();
                }), f, { slice: Rt }), u(u.P + u.F * (o(function () {
                    return [1, 2].toLocaleString() != new p([1, 2]).toLocaleString();
                }) || !o(function () {
                    _.toLocaleString.call([1, 2]);
                })), f, { toLocaleString: jt }), T[f] = k ? A : N, e || k || h(_, pt, N);
            };
        } else n.exports = function () {};
    }, { 100: 100, 104: 104, 11: 11, 114: 114, 115: 115, 116: 116, 118: 118, 119: 119, 12: 12, 120: 120, 122: 122, 123: 123, 124: 124, 128: 128, 129: 129, 141: 141, 17: 17, 25: 25, 29: 29, 33: 33, 35: 35, 40: 40, 41: 41, 42: 42, 48: 48, 51: 51, 56: 56, 58: 58, 6: 6, 60: 60, 71: 71, 72: 72, 75: 75, 77: 77, 79: 79, 8: 8, 9: 9, 92: 92, 93: 93 }], 122: [function (t, n, r) {
        "use strict";
        function packIEEE754(t, n, r) {
            var e,
                i,
                o,
                u = Array(r),
                c = 8 * r - n - 1,
                a = (1 << c) - 1,
                f = a >> 1,
                s = 23 === n ? M(2, -24) - M(2, -77) : 0,
                l = 0,
                h = t < 0 || 0 === t && 1 / t < 0 ? 1 : 0;for (t = P(t), t != t || t === E ? (i = t != t ? 1 : 0, e = a) : (e = F(I(t) / A), t * (o = M(2, -e)) < 1 && (e--, o *= 2), t += e + f >= 1 ? s / o : s * M(2, 1 - f), t * o >= 2 && (e++, o /= 2), e + f >= a ? (i = 0, e = a) : e + f >= 1 ? (i = (t * o - 1) * M(2, n), e += f) : (i = t * M(2, f - 1) * M(2, n), e = 0)); n >= 8; u[l++] = 255 & i, i /= 256, n -= 8) {}for (e = e << n | i, c += n; c > 0; u[l++] = 255 & e, e /= 256, c -= 8) {}return u[--l] |= 128 * h, u;
        }function unpackIEEE754(t, n, r) {
            var e,
                i = 8 * r - n - 1,
                o = (1 << i) - 1,
                u = o >> 1,
                c = i - 7,
                a = r - 1,
                f = t[a--],
                s = 127 & f;for (f >>= 7; c > 0; s = 256 * s + t[a], a--, c -= 8) {}for (e = s & (1 << -c) - 1, s >>= -c, c += n; c > 0; e = 256 * e + t[a], a--, c -= 8) {}if (0 === s) s = 1 - u;else {
                if (s === o) return e ? NaN : f ? -E : E;e += M(2, n), s -= u;
            }return (f ? -1 : 1) * e * M(2, s - n);
        }function unpackI32(t) {
            return t[3] << 24 | t[2] << 16 | t[1] << 8 | t[0];
        }function packI8(t) {
            return [255 & t];
        }function packI16(t) {
            return [255 & t, t >> 8 & 255];
        }function packI32(t) {
            return [255 & t, t >> 8 & 255, t >> 16 & 255, t >> 24 & 255];
        }function packF64(t) {
            return packIEEE754(t, 52, 8);
        }function packF32(t) {
            return packIEEE754(t, 23, 4);
        }function addGetter(t, n, r) {
            d(t[m], n, { get: function get() {
                    return this[r];
                } });
        }function get(t, n, r, e) {
            var i = +r,
                o = v(i);if (o + n > t[N]) throw _(b);var u = t[k]._b,
                c = o + t[j],
                a = u.slice(c, c + n);return e ? a : a.reverse();
        }function set(t, n, r, e, i, o) {
            var u = +r,
                c = v(u);if (c + n > t[N]) throw _(b);for (var a = t[k]._b, f = c + t[j], s = e(+i), l = 0; l < n; l++) {
                a[f + l] = s[o ? l : n - l - 1];
            }
        }var e = t(40),
            i = t(29),
            o = t(60),
            u = t(123),
            c = t(42),
            a = t(93),
            f = t(35),
            s = t(6),
            l = t(116),
            h = t(118),
            v = t(115),
            p = t(77).f,
            d = t(72).f,
            y = t(9),
            g = t(101),
            m = "prototype",
            b = "Wrong index!",
            x = e.ArrayBuffer,
            S = e.DataView,
            w = e.Math,
            _ = e.RangeError,
            E = e.Infinity,
            O = x,
            P = w.abs,
            M = w.pow,
            F = w.floor,
            I = w.log,
            A = w.LN2,
            k = i ? "_b" : "buffer",
            N = i ? "_l" : "byteLength",
            j = i ? "_o" : "byteOffset";if (u.ABV) {
            if (!f(function () {
                x(1);
            }) || !f(function () {
                new x(-1);
            }) || f(function () {
                return new x(), new x(1.5), new x(NaN), "ArrayBuffer" != x.name;
            })) {
                x = function ArrayBuffer(t) {
                    return s(this, x), new O(v(t));
                };for (var T, R = x[m] = O[m], L = p(O), G = 0; L.length > G;) {
                    (T = L[G++]) in x || c(x, T, O[T]);
                }o || (R.constructor = x);
            }var D = new S(new x(2)),
                C = S[m].setInt8;D.setInt8(0, 2147483648), D.setInt8(1, 2147483649), !D.getInt8(0) && D.getInt8(1) || a(S[m], { setInt8: function setInt8(t, n) {
                    C.call(this, t, n << 24 >> 24);
                }, setUint8: function setUint8(t, n) {
                    C.call(this, t, n << 24 >> 24);
                } }, !0);
        } else x = function ArrayBuffer(t) {
            s(this, x, "ArrayBuffer");var n = v(t);this._b = y.call(Array(n), 0), this[N] = n;
        }, S = function DataView(t, n, r) {
            s(this, S, "DataView"), s(t, x, "DataView");var e = t[N],
                i = l(n);if (i < 0 || i > e) throw _("Wrong offset!");if (r = void 0 === r ? e - i : h(r), i + r > e) throw _("Wrong length!");this[k] = t, this[j] = i, this[N] = r;
        }, i && (addGetter(x, "byteLength", "_l"), addGetter(S, "buffer", "_b"), addGetter(S, "byteLength", "_l"), addGetter(S, "byteOffset", "_o")), a(S[m], { getInt8: function getInt8(t) {
                return get(this, 1, t)[0] << 24 >> 24;
            }, getUint8: function getUint8(t) {
                return get(this, 1, t)[0];
            }, getInt16: function getInt16(t) {
                var n = get(this, 2, t, arguments[1]);return (n[1] << 8 | n[0]) << 16 >> 16;
            }, getUint16: function getUint16(t) {
                var n = get(this, 2, t, arguments[1]);return n[1] << 8 | n[0];
            }, getInt32: function getInt32(t) {
                return unpackI32(get(this, 4, t, arguments[1]));
            }, getUint32: function getUint32(t) {
                return unpackI32(get(this, 4, t, arguments[1])) >>> 0;
            }, getFloat32: function getFloat32(t) {
                return unpackIEEE754(get(this, 4, t, arguments[1]), 23, 4);
            }, getFloat64: function getFloat64(t) {
                return unpackIEEE754(get(this, 8, t, arguments[1]), 52, 8);
            }, setInt8: function setInt8(t, n) {
                set(this, 1, t, packI8, n);
            }, setUint8: function setUint8(t, n) {
                set(this, 1, t, packI8, n);
            }, setInt16: function setInt16(t, n) {
                set(this, 2, t, packI16, n, arguments[2]);
            }, setUint16: function setUint16(t, n) {
                set(this, 2, t, packI16, n, arguments[2]);
            }, setInt32: function setInt32(t, n) {
                set(this, 4, t, packI32, n, arguments[2]);
            }, setUint32: function setUint32(t, n) {
                set(this, 4, t, packI32, n, arguments[2]);
            }, setFloat32: function setFloat32(t, n) {
                set(this, 4, t, packF32, n, arguments[2]);
            }, setFloat64: function setFloat64(t, n) {
                set(this, 8, t, packF64, n, arguments[2]);
            } });g(x, "ArrayBuffer"), g(S, "DataView"), c(S[m], u.VIEW, !0), r.ArrayBuffer = x, r.DataView = S;
    }, { 101: 101, 115: 115, 116: 116, 118: 118, 123: 123, 29: 29, 35: 35, 40: 40, 42: 42, 6: 6, 60: 60, 72: 72, 77: 77, 9: 9, 93: 93 }], 123: [function (t, n, r) {
        for (var e, i = t(40), o = t(42), u = t(124), c = u("typed_array"), a = u("view"), f = !(!i.ArrayBuffer || !i.DataView), s = f, l = 0, h = "Int8Array,Uint8Array,Uint8ClampedArray,Int16Array,Uint16Array,Int32Array,Uint32Array,Float32Array,Float64Array".split(","); l < 9;) {
            (e = i[h[l++]]) ? (o(e.prototype, c, !0), o(e.prototype, a, !0)) : s = !1;
        }n.exports = { ABV: f, CONSTR: s, TYPED: c, VIEW: a };
    }, { 124: 124, 40: 40, 42: 42 }], 124: [function (t, n, r) {
        var e = 0,
            i = Math.random();n.exports = function (t) {
            return "Symbol(".concat(void 0 === t ? "" : t, ")_", (++e + i).toString(36));
        };
    }, {}], 125: [function (t, n, r) {
        var e = t(51);n.exports = function (t, n) {
            if (!e(t) || t._t !== n) throw TypeError("Incompatible receiver, " + n + " required!");return t;
        };
    }, { 51: 51 }], 126: [function (t, n, r) {
        var e = t(40),
            i = t(23),
            o = t(60),
            u = t(127),
            c = t(72).f;n.exports = function (t) {
            var n = i.Symbol || (i.Symbol = o ? {} : e.Symbol || {});"_" == t.charAt(0) || t in n || c(n, t, { value: u.f(t) });
        };
    }, { 127: 127, 23: 23, 40: 40, 60: 60, 72: 72 }], 127: [function (t, n, r) {
        r.f = t(128);
    }, { 128: 128 }], 128: [function (t, n, r) {
        var e = t(103)("wks"),
            i = t(124),
            o = t(40).Symbol,
            u = "function" == typeof o;(n.exports = function (t) {
            return e[t] || (e[t] = u && o[t] || (u ? o : i)("Symbol." + t));
        }).store = e;
    }, { 103: 103, 124: 124, 40: 40 }], 129: [function (t, n, r) {
        var e = t(17),
            i = t(128)("iterator"),
            o = t(58);n.exports = t(23).getIteratorMethod = function (t) {
            if (void 0 != t) return t[i] || t["@@iterator"] || o[e(t)];
        };
    }, { 128: 128, 17: 17, 23: 23, 58: 58 }], 130: [function (t, n, r) {
        var e = t(33),
            i = t(95)(/[\\^$*+?.()|[\]{}]/g, "\\$&");e(e.S, "RegExp", { escape: function escape(t) {
                return i(t);
            } });
    }, { 33: 33, 95: 95 }], 131: [function (t, n, r) {
        var e = t(33);e(e.P, "Array", { copyWithin: t(8) }), t(5)("copyWithin");
    }, { 33: 33, 5: 5, 8: 8 }], 132: [function (t, n, r) {
        "use strict";
        var e = t(33),
            i = t(12)(4);e(e.P + e.F * !t(105)([].every, !0), "Array", { every: function every(t) {
                return i(this, t, arguments[1]);
            } });
    }, { 105: 105, 12: 12, 33: 33 }], 133: [function (t, n, r) {
        var e = t(33);e(e.P, "Array", { fill: t(9) }), t(5)("fill");
    }, { 33: 33, 5: 5, 9: 9 }], 134: [function (t, n, r) {
        "use strict";
        var e = t(33),
            i = t(12)(2);e(e.P + e.F * !t(105)([].filter, !0), "Array", { filter: function filter(t) {
                return i(this, t, arguments[1]);
            } });
    }, { 105: 105, 12: 12, 33: 33 }], 135: [function (t, n, r) {
        "use strict";
        var e = t(33),
            i = t(12)(6),
            o = "findIndex",
            u = !0;o in [] && Array(1)[o](function () {
            u = !1;
        }), e(e.P + e.F * u, "Array", { findIndex: function findIndex(t) {
                return i(this, t, arguments.length > 1 ? arguments[1] : void 0);
            } }), t(5)(o);
    }, { 12: 12, 33: 33, 5: 5 }], 136: [function (t, n, r) {
        "use strict";
        var e = t(33),
            i = t(12)(5),
            o = !0;"find" in [] && Array(1).find(function () {
            o = !1;
        }), e(e.P + e.F * o, "Array", { find: function find(t) {
                return i(this, t, arguments.length > 1 ? arguments[1] : void 0);
            } }), t(5)("find");
    }, { 12: 12, 33: 33, 5: 5 }], 137: [function (t, n, r) {
        "use strict";
        var e = t(33),
            i = t(12)(0),
            o = t(105)([].forEach, !0);e(e.P + e.F * !o, "Array", { forEach: function forEach(t) {
                return i(this, t, arguments[1]);
            } });
    }, { 105: 105, 12: 12, 33: 33 }], 138: [function (t, n, r) {
        "use strict";
        var e = t(25),
            i = t(33),
            o = t(119),
            u = t(53),
            c = t(48),
            a = t(118),
            f = t(24),
            s = t(129);i(i.S + i.F * !t(56)(function (t) {
            Array.from(t);
        }), "Array", { from: function from(t) {
                var n,
                    r,
                    i,
                    l,
                    h = o(t),
                    v = "function" == typeof this ? this : Array,
                    p = arguments.length,
                    d = p > 1 ? arguments[1] : void 0,
                    y = void 0 !== d,
                    g = 0,
                    m = s(h);if (y && (d = e(d, p > 2 ? arguments[2] : void 0, 2)), void 0 == m || v == Array && c(m)) for (n = a(h.length), r = new v(n); n > g; g++) {
                    f(r, g, y ? d(h[g], g) : h[g]);
                } else for (l = m.call(h), r = new v(); !(i = l.next()).done; g++) {
                    f(r, g, y ? u(l, d, [i.value, g], !0) : i.value);
                }return r.length = g, r;
            } });
    }, { 118: 118, 119: 119, 129: 129, 24: 24, 25: 25, 33: 33, 48: 48, 53: 53, 56: 56 }], 139: [function (t, n, r) {
        "use strict";
        var e = t(33),
            i = t(11)(!1),
            o = [].indexOf,
            u = !!o && 1 / [1].indexOf(1, -0) < 0;e(e.P + e.F * (u || !t(105)(o)), "Array", { indexOf: function indexOf(t) {
                return u ? o.apply(this, arguments) || 0 : i(this, t, arguments[1]);
            } });
    }, { 105: 105, 11: 11, 33: 33 }], 140: [function (t, n, r) {
        var e = t(33);e(e.S, "Array", { isArray: t(49) });
    }, { 33: 33, 49: 49 }], 141: [function (t, n, r) {
        "use strict";
        var e = t(5),
            i = t(57),
            o = t(58),
            u = t(117);n.exports = t(55)(Array, "Array", function (t, n) {
            this._t = u(t), this._i = 0, this._k = n;
        }, function () {
            var t = this._t,
                n = this._k,
                r = this._i++;return !t || r >= t.length ? (this._t = void 0, i(1)) : "keys" == n ? i(0, r) : "values" == n ? i(0, t[r]) : i(0, [r, t[r]]);
        }, "values"), o.Arguments = o.Array, e("keys"), e("values"), e("entries");
    }, { 117: 117, 5: 5, 55: 55, 57: 57, 58: 58 }], 142: [function (t, n, r) {
        "use strict";
        var e = t(33),
            i = t(117),
            o = [].join;e(e.P + e.F * (t(47) != Object || !t(105)(o)), "Array", { join: function join(t) {
                return o.call(i(this), void 0 === t ? "," : t);
            } });
    }, { 105: 105, 117: 117, 33: 33, 47: 47 }], 143: [function (t, n, r) {
        "use strict";
        var e = t(33),
            i = t(117),
            o = t(116),
            u = t(118),
            c = [].lastIndexOf,
            a = !!c && 1 / [1].lastIndexOf(1, -0) < 0;e(e.P + e.F * (a || !t(105)(c)), "Array", { lastIndexOf: function lastIndexOf(t) {
                if (a) return c.apply(this, arguments) || 0;var n = i(this),
                    r = u(n.length),
                    e = r - 1;for (arguments.length > 1 && (e = Math.min(e, o(arguments[1]))), e < 0 && (e = r + e); e >= 0; e--) {
                    if (e in n && n[e] === t) return e || 0;
                }return -1;
            } });
    }, { 105: 105, 116: 116, 117: 117, 118: 118, 33: 33 }], 144: [function (t, n, r) {
        "use strict";
        var e = t(33),
            i = t(12)(1);e(e.P + e.F * !t(105)([].map, !0), "Array", { map: function map(t) {
                return i(this, t, arguments[1]);
            } });
    }, { 105: 105, 12: 12, 33: 33 }], 145: [function (t, n, r) {
        "use strict";
        var e = t(33),
            i = t(24);e(e.S + e.F * t(35)(function () {
            function F() {}return !(Array.of.call(F) instanceof F);
        }), "Array", { of: function of() {
                for (var t = 0, n = arguments.length, r = new ("function" == typeof this ? this : Array)(n); n > t;) {
                    i(r, t, arguments[t++]);
                }return r.length = n, r;
            } });
    }, { 24: 24, 33: 33, 35: 35 }], 146: [function (t, n, r) {
        "use strict";
        var e = t(33),
            i = t(13);e(e.P + e.F * !t(105)([].reduceRight, !0), "Array", { reduceRight: function reduceRight(t) {
                return i(this, t, arguments.length, arguments[1], !0);
            } });
    }, { 105: 105, 13: 13, 33: 33 }], 147: [function (t, n, r) {
        "use strict";
        var e = t(33),
            i = t(13);e(e.P + e.F * !t(105)([].reduce, !0), "Array", { reduce: function reduce(t) {
                return i(this, t, arguments.length, arguments[1], !1);
            } });
    }, { 105: 105, 13: 13, 33: 33 }], 148: [function (t, n, r) {
        "use strict";
        var e = t(33),
            i = t(43),
            o = t(18),
            u = t(114),
            c = t(118),
            a = [].slice;e(e.P + e.F * t(35)(function () {
            i && a.call(i);
        }), "Array", { slice: function slice(t, n) {
                var r = c(this.length),
                    e = o(this);if (n = void 0 === n ? r : n, "Array" == e) return a.call(this, t, n);for (var i = u(t, r), f = u(n, r), s = c(f - i), l = Array(s), h = 0; h < s; h++) {
                    l[h] = "String" == e ? this.charAt(i + h) : this[i + h];
                }return l;
            } });
    }, { 114: 114, 118: 118, 18: 18, 33: 33, 35: 35, 43: 43 }], 149: [function (t, n, r) {
        "use strict";
        var e = t(33),
            i = t(12)(3);e(e.P + e.F * !t(105)([].some, !0), "Array", { some: function some(t) {
                return i(this, t, arguments[1]);
            } });
    }, { 105: 105, 12: 12, 33: 33 }], 150: [function (t, n, r) {
        "use strict";
        var e = t(33),
            i = t(3),
            o = t(119),
            u = t(35),
            c = [].sort,
            a = [1, 2, 3];e(e.P + e.F * (u(function () {
            a.sort(void 0);
        }) || !u(function () {
            a.sort(null);
        }) || !t(105)(c)), "Array", { sort: function sort(t) {
                return void 0 === t ? c.call(o(this)) : c.call(o(this), i(t));
            } });
    }, { 105: 105, 119: 119, 3: 3, 33: 33, 35: 35 }], 151: [function (t, n, r) {
        t(100)("Array");
    }, { 100: 100 }], 152: [function (t, n, r) {
        var e = t(33);e(e.S, "Date", { now: function now() {
                return new Date().getTime();
            } });
    }, { 33: 33 }], 153: [function (t, n, r) {
        var e = t(33),
            i = t(26);e(e.P + e.F * (Date.prototype.toISOString !== i), "Date", { toISOString: i });
    }, { 26: 26, 33: 33 }], 154: [function (t, n, r) {
        "use strict";
        var e = t(33),
            i = t(119),
            o = t(120);e(e.P + e.F * t(35)(function () {
            return null !== new Date(NaN).toJSON() || 1 !== Date.prototype.toJSON.call({ toISOString: function toISOString() {
                    return 1;
                } });
        }), "Date", { toJSON: function toJSON(t) {
                var n = i(this),
                    r = o(n);return "number" != typeof r || isFinite(r) ? n.toISOString() : null;
            } });
    }, { 119: 119, 120: 120, 33: 33, 35: 35 }], 155: [function (t, n, r) {
        var e = t(128)("toPrimitive"),
            i = Date.prototype;e in i || t(42)(i, e, t(27));
    }, { 128: 128, 27: 27, 42: 42 }], 156: [function (t, n, r) {
        var e = Date.prototype,
            i = e.toString,
            o = e.getTime;new Date(NaN) + "" != "Invalid Date" && t(94)(e, "toString", function toString() {
            var t = o.call(this);return t === t ? i.call(this) : "Invalid Date";
        });
    }, { 94: 94 }], 157: [function (t, n, r) {
        var e = t(33);e(e.P, "Function", { bind: t(16) });
    }, { 16: 16, 33: 33 }], 158: [function (t, n, r) {
        "use strict";
        var e = t(51),
            i = t(79),
            o = t(128)("hasInstance"),
            u = Function.prototype;o in u || t(72).f(u, o, { value: function value(t) {
                if ("function" != typeof this || !e(t)) return !1;if (!e(this.prototype)) return t instanceof this;for (; t = i(t);) {
                    if (this.prototype === t) return !0;
                }return !1;
            } });
    }, { 128: 128, 51: 51, 72: 72, 79: 79 }], 159: [function (t, n, r) {
        var e = t(72).f,
            i = Function.prototype,
            o = /^\s*function ([^ (]*)/;"name" in i || t(29) && e(i, "name", { configurable: !0, get: function get() {
                try {
                    return ("" + this).match(o)[1];
                } catch (t) {
                    return "";
                }
            } });
    }, { 29: 29, 72: 72 }], 160: [function (t, n, r) {
        "use strict";
        var e = t(19),
            i = t(125);n.exports = t(22)("Map", function (t) {
            return function Map() {
                return t(this, arguments.length > 0 ? arguments[0] : void 0);
            };
        }, { get: function get(t) {
                var n = e.getEntry(i(this, "Map"), t);return n && n.v;
            }, set: function set(t, n) {
                return e.def(i(this, "Map"), 0 === t ? 0 : t, n);
            } }, e, !0);
    }, { 125: 125, 19: 19, 22: 22 }], 161: [function (t, n, r) {
        var e = t(33),
            i = t(63),
            o = Math.sqrt,
            u = Math.acosh;e(e.S + e.F * !(u && 710 == Math.floor(u(Number.MAX_VALUE)) && u(1 / 0) == 1 / 0), "Math", { acosh: function acosh(t) {
                return (t = +t) < 1 ? NaN : t > 94906265.62425156 ? Math.log(t) + Math.LN2 : i(t - 1 + o(t - 1) * o(t + 1));
            } });
    }, { 33: 33, 63: 63 }], 162: [function (t, n, r) {
        function asinh(t) {
            return isFinite(t = +t) && 0 != t ? t < 0 ? -asinh(-t) : Math.log(t + Math.sqrt(t * t + 1)) : t;
        }var e = t(33),
            i = Math.asinh;e(e.S + e.F * !(i && 1 / i(0) > 0), "Math", { asinh: asinh });
    }, { 33: 33 }], 163: [function (t, n, r) {
        var e = t(33),
            i = Math.atanh;e(e.S + e.F * !(i && 1 / i(-0) < 0), "Math", { atanh: function atanh(t) {
                return 0 == (t = +t) ? t : Math.log((1 + t) / (1 - t)) / 2;
            } });
    }, { 33: 33 }], 164: [function (t, n, r) {
        var e = t(33),
            i = t(65);e(e.S, "Math", { cbrt: function cbrt(t) {
                return i(t = +t) * Math.pow(Math.abs(t), 1 / 3);
            } });
    }, { 33: 33, 65: 65 }], 165: [function (t, n, r) {
        var e = t(33);e(e.S, "Math", { clz32: function clz32(t) {
                return (t >>>= 0) ? 31 - Math.floor(Math.log(t + .5) * Math.LOG2E) : 32;
            } });
    }, { 33: 33 }], 166: [function (t, n, r) {
        var e = t(33),
            i = Math.exp;e(e.S, "Math", { cosh: function cosh(t) {
                return (i(t = +t) + i(-t)) / 2;
            } });
    }, { 33: 33 }], 167: [function (t, n, r) {
        var e = t(33),
            i = t(61);e(e.S + e.F * (i != Math.expm1), "Math", { expm1: i });
    }, { 33: 33, 61: 61 }], 168: [function (t, n, r) {
        var e = t(33);e(e.S, "Math", { fround: t(62) });
    }, { 33: 33, 62: 62 }], 169: [function (t, n, r) {
        var e = t(33),
            i = Math.abs;e(e.S, "Math", { hypot: function hypot(t, n) {
                for (var r, e, o = 0, u = 0, c = arguments.length, a = 0; u < c;) {
                    r = i(arguments[u++]), a < r ? (e = a / r, o = o * e * e + 1, a = r) : r > 0 ? (e = r / a, o += e * e) : o += r;
                }return a === 1 / 0 ? 1 / 0 : a * Math.sqrt(o);
            } });
    }, { 33: 33 }], 170: [function (t, n, r) {
        var e = t(33),
            i = Math.imul;e(e.S + e.F * t(35)(function () {
            return -5 != i(4294967295, 5) || 2 != i.length;
        }), "Math", { imul: function imul(t, n) {
                var r = +t,
                    e = +n,
                    i = 65535 & r,
                    o = 65535 & e;return 0 | i * o + ((65535 & r >>> 16) * o + i * (65535 & e >>> 16) << 16 >>> 0);
            } });
    }, { 33: 33, 35: 35 }], 171: [function (t, n, r) {
        var e = t(33);e(e.S, "Math", { log10: function log10(t) {
                return Math.log(t) * Math.LOG10E;
            } });
    }, { 33: 33 }], 172: [function (t, n, r) {
        var e = t(33);e(e.S, "Math", { log1p: t(63) });
    }, { 33: 33, 63: 63 }], 173: [function (t, n, r) {
        var e = t(33);e(e.S, "Math", { log2: function log2(t) {
                return Math.log(t) / Math.LN2;
            } });
    }, { 33: 33 }], 174: [function (t, n, r) {
        var e = t(33);e(e.S, "Math", { sign: t(65) });
    }, { 33: 33, 65: 65 }], 175: [function (t, n, r) {
        var e = t(33),
            i = t(61),
            o = Math.exp;e(e.S + e.F * t(35)(function () {
            return -2e-17 != !Math.sinh(-2e-17);
        }), "Math", { sinh: function sinh(t) {
                return Math.abs(t = +t) < 1 ? (i(t) - i(-t)) / 2 : (o(t - 1) - o(-t - 1)) * (Math.E / 2);
            } });
    }, { 33: 33, 35: 35, 61: 61 }], 176: [function (t, n, r) {
        var e = t(33),
            i = t(61),
            o = Math.exp;e(e.S, "Math", { tanh: function tanh(t) {
                var n = i(t = +t),
                    r = i(-t);return n == 1 / 0 ? 1 : r == 1 / 0 ? -1 : (n - r) / (o(t) + o(-t));
            } });
    }, { 33: 33, 61: 61 }], 177: [function (t, n, r) {
        var e = t(33);e(e.S, "Math", { trunc: function trunc(t) {
                return (t > 0 ? Math.floor : Math.ceil)(t);
            } });
    }, { 33: 33 }], 178: [function (t, n, r) {
        "use strict";
        var e = t(40),
            i = t(41),
            o = t(18),
            u = t(45),
            c = t(120),
            a = t(35),
            f = t(77).f,
            s = t(75).f,
            l = t(72).f,
            h = t(111).trim,
            v = e.Number,
            p = v,
            d = v.prototype,
            y = "Number" == o(t(71)(d)),
            g = "trim" in String.prototype,
            m = function m(t) {
            var n = c(t, !1);if ("string" == typeof n && n.length > 2) {
                n = g ? n.trim() : h(n, 3);var r,
                    e,
                    i,
                    o = n.charCodeAt(0);if (43 === o || 45 === o) {
                    if (88 === (r = n.charCodeAt(2)) || 120 === r) return NaN;
                } else if (48 === o) {
                    switch (n.charCodeAt(1)) {case 66:case 98:
                            e = 2, i = 49;break;case 79:case 111:
                            e = 8, i = 55;break;default:
                            return +n;}for (var u, a = n.slice(2), f = 0, s = a.length; f < s; f++) {
                        if ((u = a.charCodeAt(f)) < 48 || u > i) return NaN;
                    }return parseInt(a, e);
                }
            }return +n;
        };if (!v(" 0o1") || !v("0b1") || v("+0x1")) {
            v = function Number(t) {
                var n = arguments.length < 1 ? 0 : t,
                    r = this;return r instanceof v && (y ? a(function () {
                    d.valueOf.call(r);
                }) : "Number" != o(r)) ? u(new p(m(n)), r, v) : m(n);
            };for (var b, x = t(29) ? f(p) : "MAX_VALUE,MIN_VALUE,NaN,NEGATIVE_INFINITY,POSITIVE_INFINITY,EPSILON,isFinite,isInteger,isNaN,isSafeInteger,MAX_SAFE_INTEGER,MIN_SAFE_INTEGER,parseFloat,parseInt,isInteger".split(","), S = 0; x.length > S; S++) {
                i(p, b = x[S]) && !i(v, b) && l(v, b, s(p, b));
            }v.prototype = d, d.constructor = v, t(94)(e, "Number", v);
        }
    }, { 111: 111, 120: 120, 18: 18, 29: 29, 35: 35, 40: 40, 41: 41, 45: 45, 71: 71, 72: 72, 75: 75, 77: 77, 94: 94 }], 179: [function (t, n, r) {
        var e = t(33);e(e.S, "Number", { EPSILON: Math.pow(2, -52) });
    }, { 33: 33 }], 180: [function (t, n, r) {
        var e = t(33),
            i = t(40).isFinite;e(e.S, "Number", { isFinite: function isFinite(t) {
                return "number" == typeof t && i(t);
            } });
    }, { 33: 33, 40: 40 }], 181: [function (t, n, r) {
        var e = t(33);e(e.S, "Number", { isInteger: t(50) });
    }, { 33: 33, 50: 50 }], 182: [function (t, n, r) {
        var e = t(33);e(e.S, "Number", { isNaN: function isNaN(t) {
                return t != t;
            } });
    }, { 33: 33 }], 183: [function (t, n, r) {
        var e = t(33),
            i = t(50),
            o = Math.abs;e(e.S, "Number", { isSafeInteger: function isSafeInteger(t) {
                return i(t) && o(t) <= 9007199254740991;
            } });
    }, { 33: 33, 50: 50 }], 184: [function (t, n, r) {
        var e = t(33);e(e.S, "Number", { MAX_SAFE_INTEGER: 9007199254740991 });
    }, { 33: 33 }], 185: [function (t, n, r) {
        var e = t(33);e(e.S, "Number", { MIN_SAFE_INTEGER: -9007199254740991 });
    }, { 33: 33 }], 186: [function (t, n, r) {
        var e = t(33),
            i = t(86);e(e.S + e.F * (Number.parseFloat != i), "Number", { parseFloat: i });
    }, { 33: 33, 86: 86 }], 187: [function (t, n, r) {
        var e = t(33),
            i = t(87);e(e.S + e.F * (Number.parseInt != i), "Number", { parseInt: i });
    }, { 33: 33, 87: 87 }], 188: [function (t, n, r) {
        "use strict";
        var e = t(33),
            i = t(116),
            o = t(4),
            u = t(110),
            c = 1..toFixed,
            a = Math.floor,
            f = [0, 0, 0, 0, 0, 0],
            s = "Number.toFixed: incorrect invocation!",
            l = function l(t, n) {
            for (var r = -1, e = n; ++r < 6;) {
                e += t * f[r], f[r] = e % 1e7, e = a(e / 1e7);
            }
        },
            h = function h(t) {
            for (var n = 6, r = 0; --n >= 0;) {
                r += f[n], f[n] = a(r / t), r = r % t * 1e7;
            }
        },
            v = function v() {
            for (var t = 6, n = ""; --t >= 0;) {
                if ("" !== n || 0 === t || 0 !== f[t]) {
                    var r = String(f[t]);n = "" === n ? r : n + u.call("0", 7 - r.length) + r;
                }
            }return n;
        },
            p = function p(t, n, r) {
            return 0 === n ? r : n % 2 == 1 ? p(t, n - 1, r * t) : p(t * t, n / 2, r);
        },
            d = function d(t) {
            for (var n = 0, r = t; r >= 4096;) {
                n += 12, r /= 4096;
            }for (; r >= 2;) {
                n += 1, r /= 2;
            }return n;
        };e(e.P + e.F * (!!c && ("0.000" !== 8e-5.toFixed(3) || "1" !== .9.toFixed(0) || "1.25" !== 1.255.toFixed(2) || "1000000000000000128" !== 0xde0b6b3a7640080.toFixed(0)) || !t(35)(function () {
            c.call({});
        })), "Number", { toFixed: function toFixed(t) {
                var n,
                    r,
                    e,
                    c,
                    a = o(this, s),
                    f = i(t),
                    y = "",
                    g = "0";if (f < 0 || f > 20) throw RangeError(s);if (a != a) return "NaN";if (a <= -1e21 || a >= 1e21) return String(a);if (a < 0 && (y = "-", a = -a), a > 1e-21) if (n = d(a * p(2, 69, 1)) - 69, r = n < 0 ? a * p(2, -n, 1) : a / p(2, n, 1), r *= 4503599627370496, (n = 52 - n) > 0) {
                    for (l(0, r), e = f; e >= 7;) {
                        l(1e7, 0), e -= 7;
                    }for (l(p(10, e, 1), 0), e = n - 1; e >= 23;) {
                        h(1 << 23), e -= 23;
                    }h(1 << e), l(1, 1), h(2), g = v();
                } else l(0, r), l(1 << -n, 0), g = v() + u.call("0", f);return f > 0 ? (c = g.length, g = y + (c <= f ? "0." + u.call("0", f - c) + g : g.slice(0, c - f) + "." + g.slice(c - f))) : g = y + g, g;
            } });
    }, { 110: 110, 116: 116, 33: 33, 35: 35, 4: 4 }], 189: [function (t, n, r) {
        "use strict";
        var e = t(33),
            i = t(35),
            o = t(4),
            u = 1..toPrecision;e(e.P + e.F * (i(function () {
            return "1" !== u.call(1, void 0);
        }) || !i(function () {
            u.call({});
        })), "Number", { toPrecision: function toPrecision(t) {
                var n = o(this, "Number#toPrecision: incorrect invocation!");return void 0 === t ? u.call(n) : u.call(n, t);
            } });
    }, { 33: 33, 35: 35, 4: 4 }], 190: [function (t, n, r) {
        var e = t(33);e(e.S + e.F, "Object", { assign: t(70) });
    }, { 33: 33, 70: 70 }], 191: [function (t, n, r) {
        var e = t(33);e(e.S, "Object", { create: t(71) });
    }, { 33: 33, 71: 71 }], 192: [function (t, n, r) {
        var e = t(33);e(e.S + e.F * !t(29), "Object", { defineProperties: t(73) });
    }, { 29: 29, 33: 33, 73: 73 }], 193: [function (t, n, r) {
        var e = t(33);e(e.S + e.F * !t(29), "Object", { defineProperty: t(72).f });
    }, { 29: 29, 33: 33, 72: 72 }], 194: [function (t, n, r) {
        var e = t(51),
            i = t(66).onFreeze;t(83)("freeze", function (t) {
            return function freeze(n) {
                return t && e(n) ? t(i(n)) : n;
            };
        });
    }, { 51: 51, 66: 66, 83: 83 }], 195: [function (t, n, r) {
        var e = t(117),
            i = t(75).f;t(83)("getOwnPropertyDescriptor", function () {
            return function getOwnPropertyDescriptor(t, n) {
                return i(e(t), n);
            };
        });
    }, { 117: 117, 75: 75, 83: 83 }], 196: [function (t, n, r) {
        t(83)("getOwnPropertyNames", function () {
            return t(76).f;
        });
    }, { 76: 76, 83: 83 }], 197: [function (t, n, r) {
        var e = t(119),
            i = t(79);t(83)("getPrototypeOf", function () {
            return function getPrototypeOf(t) {
                return i(e(t));
            };
        });
    }, { 119: 119, 79: 79, 83: 83 }], 198: [function (t, n, r) {
        var e = t(51);t(83)("isExtensible", function (t) {
            return function isExtensible(n) {
                return !!e(n) && (!t || t(n));
            };
        });
    }, { 51: 51, 83: 83 }], 199: [function (t, n, r) {
        var e = t(51);t(83)("isFrozen", function (t) {
            return function isFrozen(n) {
                return !e(n) || !!t && t(n);
            };
        });
    }, { 51: 51, 83: 83 }], 200: [function (t, n, r) {
        var e = t(51);t(83)("isSealed", function (t) {
            return function isSealed(n) {
                return !e(n) || !!t && t(n);
            };
        });
    }, { 51: 51, 83: 83 }], 201: [function (t, n, r) {
        var e = t(33);e(e.S, "Object", { is: t(96) });
    }, { 33: 33, 96: 96 }], 202: [function (t, n, r) {
        var e = t(119),
            i = t(81);t(83)("keys", function () {
            return function keys(t) {
                return i(e(t));
            };
        });
    }, { 119: 119, 81: 81, 83: 83 }], 203: [function (t, n, r) {
        var e = t(51),
            i = t(66).onFreeze;t(83)("preventExtensions", function (t) {
            return function preventExtensions(n) {
                return t && e(n) ? t(i(n)) : n;
            };
        });
    }, { 51: 51, 66: 66, 83: 83 }], 204: [function (t, n, r) {
        var e = t(51),
            i = t(66).onFreeze;t(83)("seal", function (t) {
            return function seal(n) {
                return t && e(n) ? t(i(n)) : n;
            };
        });
    }, { 51: 51, 66: 66, 83: 83 }], 205: [function (t, n, r) {
        var e = t(33);e(e.S, "Object", { setPrototypeOf: t(99).set });
    }, { 33: 33, 99: 99 }], 206: [function (t, n, r) {
        "use strict";
        var e = t(17),
            i = {};i[t(128)("toStringTag")] = "z", i + "" != "[object z]" && t(94)(Object.prototype, "toString", function toString() {
            return "[object " + e(this) + "]";
        }, !0);
    }, { 128: 128, 17: 17, 94: 94 }], 207: [function (t, n, r) {
        var e = t(33),
            i = t(86);e(e.G + e.F * (parseFloat != i), { parseFloat: i });
    }, { 33: 33, 86: 86 }], 208: [function (t, n, r) {
        var e = t(33),
            i = t(87);e(e.G + e.F * (parseInt != i), { parseInt: i });
    }, { 33: 33, 87: 87 }], 209: [function (t, n, r) {
        "use strict";
        var e,
            i,
            o,
            u,
            c = t(60),
            a = t(40),
            f = t(25),
            s = t(17),
            l = t(33),
            h = t(51),
            v = t(3),
            p = t(6),
            d = t(39),
            y = t(104),
            g = t(113).set,
            m = t(68)(),
            b = t(69),
            x = t(90),
            S = t(91),
            w = a.TypeError,
            _ = a.process,
            E = a.Promise,
            O = "process" == s(_),
            P = function P() {},
            M = i = b.f,
            F = !!function () {
            try {
                var n = E.resolve(1),
                    r = (n.constructor = {})[t(128)("species")] = function (t) {
                    t(P, P);
                };return (O || "function" == typeof PromiseRejectionEvent) && n.then(P) instanceof r;
            } catch (t) {}
        }(),
            I = c ? function (t, n) {
            return t === n || t === E && n === u;
        } : function (t, n) {
            return t === n;
        },
            A = function A(t) {
            var n;return !(!h(t) || "function" != typeof (n = t.then)) && n;
        },
            k = function k(t, n) {
            if (!t._n) {
                t._n = !0;var r = t._c;m(function () {
                    for (var e = t._v, i = 1 == t._s, o = 0; r.length > o;) {
                        !function (n) {
                            var r,
                                o,
                                u = i ? n.ok : n.fail,
                                c = n.resolve,
                                a = n.reject,
                                f = n.domain;try {
                                u ? (i || (2 == t._h && T(t), t._h = 1), !0 === u ? r = e : (f && f.enter(), r = u(e), f && f.exit()), r === n.promise ? a(w("Promise-chain cycle")) : (o = A(r)) ? o.call(r, c, a) : c(r)) : a(e);
                            } catch (t) {
                                a(t);
                            }
                        }(r[o++]);
                    }t._c = [], t._n = !1, n && !t._h && N(t);
                });
            }
        },
            N = function N(t) {
            g.call(a, function () {
                var n,
                    r,
                    e,
                    i = t._v,
                    o = j(t);if (o && (n = x(function () {
                    O ? _.emit("unhandledRejection", i, t) : (r = a.onunhandledrejection) ? r({ promise: t, reason: i }) : (e = a.console) && e.error && e.error("Unhandled promise rejection", i);
                }), t._h = O || j(t) ? 2 : 1), t._a = void 0, o && n.e) throw n.v;
            });
        },
            j = function j(t) {
            if (1 == t._h) return !1;for (var n, r = t._a || t._c, e = 0; r.length > e;) {
                if (n = r[e++], n.fail || !j(n.promise)) return !1;
            }return !0;
        },
            T = function T(t) {
            g.call(a, function () {
                var n;O ? _.emit("rejectionHandled", t) : (n = a.onrejectionhandled) && n({ promise: t, reason: t._v });
            });
        },
            R = function R(t) {
            var n = this;n._d || (n._d = !0, n = n._w || n, n._v = t, n._s = 2, n._a || (n._a = n._c.slice()), k(n, !0));
        },
            L = function L(t) {
            var n,
                r = this;if (!r._d) {
                r._d = !0, r = r._w || r;try {
                    if (r === t) throw w("Promise can't be resolved itself");(n = A(t)) ? m(function () {
                        var e = { _w: r, _d: !1 };try {
                            n.call(t, f(L, e, 1), f(R, e, 1));
                        } catch (t) {
                            R.call(e, t);
                        }
                    }) : (r._v = t, r._s = 1, k(r, !1));
                } catch (t) {
                    R.call({ _w: r, _d: !1 }, t);
                }
            }
        };F || (E = function Promise(t) {
            p(this, E, "Promise", "_h"), v(t), e.call(this);try {
                t(f(L, this, 1), f(R, this, 1));
            } catch (t) {
                R.call(this, t);
            }
        }, e = function Promise(t) {
            this._c = [], this._a = void 0, this._s = 0, this._d = !1, this._v = void 0, this._h = 0, this._n = !1;
        }, e.prototype = t(93)(E.prototype, { then: function then(t, n) {
                var r = M(y(this, E));return r.ok = "function" != typeof t || t, r.fail = "function" == typeof n && n, r.domain = O ? _.domain : void 0, this._c.push(r), this._a && this._a.push(r), this._s && k(this, !1), r.promise;
            }, catch: function _catch(t) {
                return this.then(void 0, t);
            } }), o = function o() {
            var t = new e();this.promise = t, this.resolve = f(L, t, 1), this.reject = f(R, t, 1);
        }, b.f = M = function M(t) {
            return I(E, t) ? new o(t) : i(t);
        }), l(l.G + l.W + l.F * !F, { Promise: E }), t(101)(E, "Promise"), t(100)("Promise"), u = t(23).Promise, l(l.S + l.F * !F, "Promise", { reject: function reject(t) {
                var n = M(this);return (0, n.reject)(t), n.promise;
            } }), l(l.S + l.F * (c || !F), "Promise", { resolve: function resolve(t) {
                return t instanceof E && I(t.constructor, this) ? t : S(this, t);
            } }), l(l.S + l.F * !(F && t(56)(function (t) {
            E.all(t).catch(P);
        })), "Promise", { all: function all(t) {
                var n = this,
                    r = M(n),
                    e = r.resolve,
                    i = r.reject,
                    o = x(function () {
                    var r = [],
                        o = 0,
                        u = 1;d(t, !1, function (t) {
                        var c = o++,
                            a = !1;r.push(void 0), u++, n.resolve(t).then(function (t) {
                            a || (a = !0, r[c] = t, --u || e(r));
                        }, i);
                    }), --u || e(r);
                });return o.e && i(o.v), r.promise;
            }, race: function race(t) {
                var n = this,
                    r = M(n),
                    e = r.reject,
                    i = x(function () {
                    d(t, !1, function (t) {
                        n.resolve(t).then(r.resolve, e);
                    });
                });return i.e && e(i.v), r.promise;
            } });
    }, { 100: 100, 101: 101, 104: 104, 113: 113, 128: 128, 17: 17, 23: 23, 25: 25,
        3: 3, 33: 33, 39: 39, 40: 40, 51: 51, 56: 56, 6: 6, 60: 60, 68: 68, 69: 69, 90: 90, 91: 91, 93: 93 }], 210: [function (t, n, r) {
        var e = t(33),
            i = t(3),
            o = t(7),
            u = (t(40).Reflect || {}).apply,
            c = Function.apply;e(e.S + e.F * !t(35)(function () {
            u(function () {});
        }), "Reflect", { apply: function apply(t, n, r) {
                var e = i(t),
                    a = o(r);return u ? u(e, n, a) : c.call(e, n, a);
            } });
    }, { 3: 3, 33: 33, 35: 35, 40: 40, 7: 7 }], 211: [function (t, n, r) {
        var e = t(33),
            i = t(71),
            o = t(3),
            u = t(7),
            c = t(51),
            a = t(35),
            f = t(16),
            s = (t(40).Reflect || {}).construct,
            l = a(function () {
            function F() {}return !(s(function () {}, [], F) instanceof F);
        }),
            h = !a(function () {
            s(function () {});
        });e(e.S + e.F * (l || h), "Reflect", { construct: function construct(t, n) {
                o(t), u(n);var r = arguments.length < 3 ? t : o(arguments[2]);if (h && !l) return s(t, n, r);if (t == r) {
                    switch (n.length) {case 0:
                            return new t();case 1:
                            return new t(n[0]);case 2:
                            return new t(n[0], n[1]);case 3:
                            return new t(n[0], n[1], n[2]);case 4:
                            return new t(n[0], n[1], n[2], n[3]);}var e = [null];return e.push.apply(e, n), new (f.apply(t, e))();
                }var a = r.prototype,
                    v = i(c(a) ? a : Object.prototype),
                    p = Function.apply.call(t, v, n);return c(p) ? p : v;
            } });
    }, { 16: 16, 3: 3, 33: 33, 35: 35, 40: 40, 51: 51, 7: 7, 71: 71 }], 212: [function (t, n, r) {
        var e = t(72),
            i = t(33),
            o = t(7),
            u = t(120);i(i.S + i.F * t(35)(function () {
            Reflect.defineProperty(e.f({}, 1, { value: 1 }), 1, { value: 2 });
        }), "Reflect", { defineProperty: function defineProperty(t, n, r) {
                o(t), n = u(n, !0), o(r);try {
                    return e.f(t, n, r), !0;
                } catch (t) {
                    return !1;
                }
            } });
    }, { 120: 120, 33: 33, 35: 35, 7: 7, 72: 72 }], 213: [function (t, n, r) {
        var e = t(33),
            i = t(75).f,
            o = t(7);e(e.S, "Reflect", { deleteProperty: function deleteProperty(t, n) {
                var r = i(o(t), n);return !(r && !r.configurable) && delete t[n];
            } });
    }, { 33: 33, 7: 7, 75: 75 }], 214: [function (t, n, r) {
        "use strict";
        var e = t(33),
            i = t(7),
            o = function o(t) {
            this._t = i(t), this._i = 0;var n,
                r = this._k = [];for (n in t) {
                r.push(n);
            }
        };t(54)(o, "Object", function () {
            var t,
                n = this,
                r = n._k;do {
                if (n._i >= r.length) return { value: void 0, done: !0 };
            } while (!((t = r[n._i++]) in n._t));return { value: t, done: !1 };
        }), e(e.S, "Reflect", { enumerate: function enumerate(t) {
                return new o(t);
            } });
    }, { 33: 33, 54: 54, 7: 7 }], 215: [function (t, n, r) {
        var e = t(75),
            i = t(33),
            o = t(7);i(i.S, "Reflect", { getOwnPropertyDescriptor: function getOwnPropertyDescriptor(t, n) {
                return e.f(o(t), n);
            } });
    }, { 33: 33, 7: 7, 75: 75 }], 216: [function (t, n, r) {
        var e = t(33),
            i = t(79),
            o = t(7);e(e.S, "Reflect", { getPrototypeOf: function getPrototypeOf(t) {
                return i(o(t));
            } });
    }, { 33: 33, 7: 7, 79: 79 }], 217: [function (t, n, r) {
        function get(t, n) {
            var r,
                u,
                f = arguments.length < 3 ? t : arguments[2];return a(t) === f ? t[n] : (r = e.f(t, n)) ? o(r, "value") ? r.value : void 0 !== r.get ? r.get.call(f) : void 0 : c(u = i(t)) ? get(u, n, f) : void 0;
        }var e = t(75),
            i = t(79),
            o = t(41),
            u = t(33),
            c = t(51),
            a = t(7);u(u.S, "Reflect", { get: get });
    }, { 33: 33, 41: 41, 51: 51, 7: 7, 75: 75, 79: 79 }], 218: [function (t, n, r) {
        var e = t(33);e(e.S, "Reflect", { has: function has(t, n) {
                return n in t;
            } });
    }, { 33: 33 }], 219: [function (t, n, r) {
        var e = t(33),
            i = t(7),
            o = Object.isExtensible;e(e.S, "Reflect", { isExtensible: function isExtensible(t) {
                return i(t), !o || o(t);
            } });
    }, { 33: 33, 7: 7 }], 220: [function (t, n, r) {
        var e = t(33);e(e.S, "Reflect", { ownKeys: t(85) });
    }, { 33: 33, 85: 85 }], 221: [function (t, n, r) {
        var e = t(33),
            i = t(7),
            o = Object.preventExtensions;e(e.S, "Reflect", { preventExtensions: function preventExtensions(t) {
                i(t);try {
                    return o && o(t), !0;
                } catch (t) {
                    return !1;
                }
            } });
    }, { 33: 33, 7: 7 }], 222: [function (t, n, r) {
        var e = t(33),
            i = t(99);i && e(e.S, "Reflect", { setPrototypeOf: function setPrototypeOf(t, n) {
                i.check(t, n);try {
                    return i.set(t, n), !0;
                } catch (t) {
                    return !1;
                }
            } });
    }, { 33: 33, 99: 99 }], 223: [function (t, n, r) {
        function set(t, n, r) {
            var c,
                l,
                h = arguments.length < 4 ? t : arguments[3],
                v = i.f(f(t), n);if (!v) {
                if (s(l = o(t))) return set(l, n, r, h);v = a(0);
            }return u(v, "value") ? !(!1 === v.writable || !s(h)) && (c = i.f(h, n) || a(0), c.value = r, e.f(h, n, c), !0) : void 0 !== v.set && (v.set.call(h, r), !0);
        }var e = t(72),
            i = t(75),
            o = t(79),
            u = t(41),
            c = t(33),
            a = t(92),
            f = t(7),
            s = t(51);c(c.S, "Reflect", { set: set });
    }, { 33: 33, 41: 41, 51: 51, 7: 7, 72: 72, 75: 75, 79: 79, 92: 92 }], 224: [function (t, n, r) {
        var e = t(40),
            i = t(45),
            o = t(72).f,
            u = t(77).f,
            c = t(52),
            a = t(37),
            f = e.RegExp,
            s = f,
            l = f.prototype,
            h = /a/g,
            v = /a/g,
            p = new f(h) !== h;if (t(29) && (!p || t(35)(function () {
            return v[t(128)("match")] = !1, f(h) != h || f(v) == v || "/a/i" != f(h, "i");
        }))) {
            f = function RegExp(t, n) {
                var r = this instanceof f,
                    e = c(t),
                    o = void 0 === n;return !r && e && t.constructor === f && o ? t : i(p ? new s(e && !o ? t.source : t, n) : s((e = t instanceof f) ? t.source : t, e && o ? a.call(t) : n), r ? this : l, f);
            };for (var d = u(s), y = 0; d.length > y;) {
                !function (t) {
                    t in f || o(f, t, { configurable: !0, get: function get() {
                            return s[t];
                        }, set: function set(n) {
                            s[t] = n;
                        } });
                }(d[y++]);
            }l.constructor = f, f.prototype = l, t(94)(e, "RegExp", f);
        }t(100)("RegExp");
    }, { 100: 100, 128: 128, 29: 29, 35: 35, 37: 37, 40: 40, 45: 45, 52: 52, 72: 72, 77: 77, 94: 94 }], 225: [function (t, n, r) {
        t(29) && "g" != /./g.flags && t(72).f(RegExp.prototype, "flags", { configurable: !0, get: t(37) });
    }, { 29: 29, 37: 37, 72: 72 }], 226: [function (t, n, r) {
        t(36)("match", 1, function (t, n, r) {
            return [function match(r) {
                "use strict";
                var e = t(this),
                    i = void 0 == r ? void 0 : r[n];return void 0 !== i ? i.call(r, e) : new RegExp(r)[n](String(e));
            }, r];
        });
    }, { 36: 36 }], 227: [function (t, n, r) {
        t(36)("replace", 2, function (t, n, r) {
            return [function replace(e, i) {
                "use strict";
                var o = t(this),
                    u = void 0 == e ? void 0 : e[n];return void 0 !== u ? u.call(e, o, i) : r.call(String(o), e, i);
            }, r];
        });
    }, { 36: 36 }], 228: [function (t, n, r) {
        t(36)("search", 1, function (t, n, r) {
            return [function search(r) {
                "use strict";
                var e = t(this),
                    i = void 0 == r ? void 0 : r[n];return void 0 !== i ? i.call(r, e) : new RegExp(r)[n](String(e));
            }, r];
        });
    }, { 36: 36 }], 229: [function (t, n, r) {
        t(36)("split", 2, function (n, r, e) {
            "use strict";
            var i = t(52),
                o = e,
                u = [].push,
                c = "length";if ("c" == "abbc".split(/(b)*/)[1] || 4 != "test".split(/(?:)/, -1)[c] || 2 != "ab".split(/(?:ab)*/)[c] || 4 != ".".split(/(.?)(.?)/)[c] || ".".split(/()()/)[c] > 1 || "".split(/.?/)[c]) {
                var a = void 0 === /()??/.exec("")[1];e = function e(t, n) {
                    var r = String(this);if (void 0 === t && 0 === n) return [];if (!i(t)) return o.call(r, t, n);var e,
                        f,
                        s,
                        l,
                        h,
                        v = [],
                        p = (t.ignoreCase ? "i" : "") + (t.multiline ? "m" : "") + (t.unicode ? "u" : "") + (t.sticky ? "y" : ""),
                        d = 0,
                        y = void 0 === n ? 4294967295 : n >>> 0,
                        g = new RegExp(t.source, p + "g");for (a || (e = new RegExp("^" + g.source + "$(?!\\s)", p)); (f = g.exec(r)) && !((s = f.index + f[0][c]) > d && (v.push(r.slice(d, f.index)), !a && f[c] > 1 && f[0].replace(e, function () {
                        for (h = 1; h < arguments[c] - 2; h++) {
                            void 0 === arguments[h] && (f[h] = void 0);
                        }
                    }), f[c] > 1 && f.index < r[c] && u.apply(v, f.slice(1)), l = f[0][c], d = s, v[c] >= y));) {
                        g.lastIndex === f.index && g.lastIndex++;
                    }return d === r[c] ? !l && g.test("") || v.push("") : v.push(r.slice(d)), v[c] > y ? v.slice(0, y) : v;
                };
            } else "0".split(void 0, 0)[c] && (e = function e(t, n) {
                return void 0 === t && 0 === n ? [] : o.call(this, t, n);
            });return [function split(t, i) {
                var o = n(this),
                    u = void 0 == t ? void 0 : t[r];return void 0 !== u ? u.call(t, o, i) : e.call(String(o), t, i);
            }, e];
        });
    }, { 36: 36, 52: 52 }], 230: [function (t, n, r) {
        "use strict";
        t(225);var e = t(7),
            i = t(37),
            o = t(29),
            u = /./.toString,
            c = function c(n) {
            t(94)(RegExp.prototype, "toString", n, !0);
        };t(35)(function () {
            return "/a/b" != u.call({ source: "a", flags: "b" });
        }) ? c(function toString() {
            var t = e(this);return "/".concat(t.source, "/", "flags" in t ? t.flags : !o && t instanceof RegExp ? i.call(t) : void 0);
        }) : "toString" != u.name && c(function toString() {
            return u.call(this);
        });
    }, { 225: 225, 29: 29, 35: 35, 37: 37, 7: 7, 94: 94 }], 231: [function (t, n, r) {
        "use strict";
        var e = t(19),
            i = t(125);n.exports = t(22)("Set", function (t) {
            return function Set() {
                return t(this, arguments.length > 0 ? arguments[0] : void 0);
            };
        }, { add: function add(t) {
                return e.def(i(this, "Set"), t = 0 === t ? 0 : t, t);
            } }, e);
    }, { 125: 125, 19: 19, 22: 22 }], 232: [function (t, n, r) {
        "use strict";
        t(108)("anchor", function (t) {
            return function anchor(n) {
                return t(this, "a", "name", n);
            };
        });
    }, { 108: 108 }], 233: [function (t, n, r) {
        "use strict";
        t(108)("big", function (t) {
            return function big() {
                return t(this, "big", "", "");
            };
        });
    }, { 108: 108 }], 234: [function (t, n, r) {
        "use strict";
        t(108)("blink", function (t) {
            return function blink() {
                return t(this, "blink", "", "");
            };
        });
    }, { 108: 108 }], 235: [function (t, n, r) {
        "use strict";
        t(108)("bold", function (t) {
            return function bold() {
                return t(this, "b", "", "");
            };
        });
    }, { 108: 108 }], 236: [function (t, n, r) {
        "use strict";
        var e = t(33),
            i = t(106)(!1);e(e.P, "String", { codePointAt: function codePointAt(t) {
                return i(this, t);
            } });
    }, { 106: 106, 33: 33 }], 237: [function (t, n, r) {
        "use strict";
        var e = t(33),
            i = t(118),
            o = t(107),
            u = "".endsWith;e(e.P + e.F * t(34)("endsWith"), "String", { endsWith: function endsWith(t) {
                var n = o(this, t, "endsWith"),
                    r = arguments.length > 1 ? arguments[1] : void 0,
                    e = i(n.length),
                    c = void 0 === r ? e : Math.min(i(r), e),
                    a = String(t);return u ? u.call(n, a, c) : n.slice(c - a.length, c) === a;
            } });
    }, { 107: 107, 118: 118, 33: 33, 34: 34 }], 238: [function (t, n, r) {
        "use strict";
        t(108)("fixed", function (t) {
            return function fixed() {
                return t(this, "tt", "", "");
            };
        });
    }, { 108: 108 }], 239: [function (t, n, r) {
        "use strict";
        t(108)("fontcolor", function (t) {
            return function fontcolor(n) {
                return t(this, "font", "color", n);
            };
        });
    }, { 108: 108 }], 240: [function (t, n, r) {
        "use strict";
        t(108)("fontsize", function (t) {
            return function fontsize(n) {
                return t(this, "font", "size", n);
            };
        });
    }, { 108: 108 }], 241: [function (t, n, r) {
        var e = t(33),
            i = t(114),
            o = String.fromCharCode,
            u = String.fromCodePoint;e(e.S + e.F * (!!u && 1 != u.length), "String", { fromCodePoint: function fromCodePoint(t) {
                for (var n, r = [], e = arguments.length, u = 0; e > u;) {
                    if (n = +arguments[u++], i(n, 1114111) !== n) throw RangeError(n + " is not a valid code point");r.push(n < 65536 ? o(n) : o(55296 + ((n -= 65536) >> 10), n % 1024 + 56320));
                }return r.join("");
            } });
    }, { 114: 114, 33: 33 }], 242: [function (t, n, r) {
        "use strict";
        var e = t(33),
            i = t(107);e(e.P + e.F * t(34)("includes"), "String", { includes: function includes(t) {
                return !!~i(this, t, "includes").indexOf(t, arguments.length > 1 ? arguments[1] : void 0);
            } });
    }, { 107: 107, 33: 33, 34: 34 }], 243: [function (t, n, r) {
        "use strict";
        t(108)("italics", function (t) {
            return function italics() {
                return t(this, "i", "", "");
            };
        });
    }, { 108: 108 }], 244: [function (t, n, r) {
        "use strict";
        var e = t(106)(!0);t(55)(String, "String", function (t) {
            this._t = String(t), this._i = 0;
        }, function () {
            var t,
                n = this._t,
                r = this._i;return r >= n.length ? { value: void 0, done: !0 } : (t = e(n, r), this._i += t.length, { value: t, done: !1 });
        });
    }, { 106: 106, 55: 55 }], 245: [function (t, n, r) {
        "use strict";
        t(108)("link", function (t) {
            return function link(n) {
                return t(this, "a", "href", n);
            };
        });
    }, { 108: 108 }], 246: [function (t, n, r) {
        var e = t(33),
            i = t(117),
            o = t(118);e(e.S, "String", { raw: function raw(t) {
                for (var n = i(t.raw), r = o(n.length), e = arguments.length, u = [], c = 0; r > c;) {
                    u.push(String(n[c++])), c < e && u.push(String(arguments[c]));
                }return u.join("");
            } });
    }, { 117: 117, 118: 118, 33: 33 }], 247: [function (t, n, r) {
        var e = t(33);e(e.P, "String", { repeat: t(110) });
    }, { 110: 110, 33: 33 }], 248: [function (t, n, r) {
        "use strict";
        t(108)("small", function (t) {
            return function small() {
                return t(this, "small", "", "");
            };
        });
    }, { 108: 108 }], 249: [function (t, n, r) {
        "use strict";
        var e = t(33),
            i = t(118),
            o = t(107),
            u = "".startsWith;e(e.P + e.F * t(34)("startsWith"), "String", { startsWith: function startsWith(t) {
                var n = o(this, t, "startsWith"),
                    r = i(Math.min(arguments.length > 1 ? arguments[1] : void 0, n.length)),
                    e = String(t);return u ? u.call(n, e, r) : n.slice(r, r + e.length) === e;
            } });
    }, { 107: 107, 118: 118, 33: 33, 34: 34 }], 250: [function (t, n, r) {
        "use strict";
        t(108)("strike", function (t) {
            return function strike() {
                return t(this, "strike", "", "");
            };
        });
    }, { 108: 108 }], 251: [function (t, n, r) {
        "use strict";
        t(108)("sub", function (t) {
            return function sub() {
                return t(this, "sub", "", "");
            };
        });
    }, { 108: 108 }], 252: [function (t, n, r) {
        "use strict";
        t(108)("sup", function (t) {
            return function sup() {
                return t(this, "sup", "", "");
            };
        });
    }, { 108: 108 }], 253: [function (t, n, r) {
        "use strict";
        t(111)("trim", function (t) {
            return function trim() {
                return t(this, 3);
            };
        });
    }, { 111: 111 }], 254: [function (t, n, r) {
        "use strict";
        var e = t(40),
            i = t(41),
            o = t(29),
            u = t(33),
            c = t(94),
            a = t(66).KEY,
            f = t(35),
            s = t(103),
            l = t(101),
            h = t(124),
            v = t(128),
            p = t(127),
            d = t(126),
            y = t(59),
            g = t(32),
            m = t(49),
            b = t(7),
            x = t(117),
            S = t(120),
            w = t(92),
            _ = t(71),
            E = t(76),
            O = t(75),
            P = t(72),
            M = t(81),
            F = O.f,
            I = P.f,
            A = E.f,
            k = e.Symbol,
            N = e.JSON,
            j = N && N.stringify,
            T = v("_hidden"),
            R = v("toPrimitive"),
            L = {}.propertyIsEnumerable,
            G = s("symbol-registry"),
            D = s("symbols"),
            C = s("op-symbols"),
            W = Object.prototype,
            U = "function" == typeof k,
            B = e.QObject,
            V = !B || !B.prototype || !B.prototype.findChild,
            z = o && f(function () {
            return 7 != _(I({}, "a", { get: function get() {
                    return I(this, "a", { value: 7 }).a;
                } })).a;
        }) ? function (t, n, r) {
            var e = F(W, n);e && delete W[n], I(t, n, r), e && t !== W && I(W, n, e);
        } : I,
            q = function q(t) {
            var n = D[t] = _(k.prototype);return n._k = t, n;
        },
            K = U && "symbol" == _typeof(k.iterator) ? function (t) {
            return "symbol" == (typeof t === "undefined" ? "undefined" : _typeof(t));
        } : function (t) {
            return t instanceof k;
        },
            Y = function defineProperty(t, n, r) {
            return t === W && Y(C, n, r), b(t), n = S(n, !0), b(r), i(D, n) ? (r.enumerable ? (i(t, T) && t[T][n] && (t[T][n] = !1), r = _(r, { enumerable: w(0, !1) })) : (i(t, T) || I(t, T, w(1, {})), t[T][n] = !0), z(t, n, r)) : I(t, n, r);
        },
            J = function defineProperties(t, n) {
            b(t);for (var r, e = g(n = x(n)), i = 0, o = e.length; o > i;) {
                Y(t, r = e[i++], n[r]);
            }return t;
        },
            H = function create(t, n) {
            return void 0 === n ? _(t) : J(_(t), n);
        },
            X = function propertyIsEnumerable(t) {
            var n = L.call(this, t = S(t, !0));return !(this === W && i(D, t) && !i(C, t)) && (!(n || !i(this, t) || !i(D, t) || i(this, T) && this[T][t]) || n);
        },
            $ = function getOwnPropertyDescriptor(t, n) {
            if (t = x(t), n = S(n, !0), t !== W || !i(D, n) || i(C, n)) {
                var r = F(t, n);return !r || !i(D, n) || i(t, T) && t[T][n] || (r.enumerable = !0), r;
            }
        },
            Z = function getOwnPropertyNames(t) {
            for (var n, r = A(x(t)), e = [], o = 0; r.length > o;) {
                i(D, n = r[o++]) || n == T || n == a || e.push(n);
            }return e;
        },
            Q = function getOwnPropertySymbols(t) {
            for (var n, r = t === W, e = A(r ? C : x(t)), o = [], u = 0; e.length > u;) {
                !i(D, n = e[u++]) || r && !i(W, n) || o.push(D[n]);
            }return o;
        };U || (k = function _Symbol() {
            if (this instanceof k) throw TypeError("Symbol is not a constructor!");var t = h(arguments.length > 0 ? arguments[0] : void 0),
                n = function n(r) {
                this === W && n.call(C, r), i(this, T) && i(this[T], t) && (this[T][t] = !1), z(this, t, w(1, r));
            };return o && V && z(W, t, { configurable: !0, set: n }), q(t);
        }, c(k.prototype, "toString", function toString() {
            return this._k;
        }), O.f = $, P.f = Y, t(77).f = E.f = Z, t(82).f = X, t(78).f = Q, o && !t(60) && c(W, "propertyIsEnumerable", X, !0), p.f = function (t) {
            return q(v(t));
        }), u(u.G + u.W + u.F * !U, { Symbol: k });for (var tt = "hasInstance,isConcatSpreadable,iterator,match,replace,search,species,split,toPrimitive,toStringTag,unscopables".split(","), nt = 0; tt.length > nt;) {
            v(tt[nt++]);
        }for (var rt = M(v.store), et = 0; rt.length > et;) {
            d(rt[et++]);
        }u(u.S + u.F * !U, "Symbol", { for: function _for(t) {
                return i(G, t += "") ? G[t] : G[t] = k(t);
            }, keyFor: function keyFor(t) {
                if (K(t)) return y(G, t);throw TypeError(t + " is not a symbol!");
            }, useSetter: function useSetter() {
                V = !0;
            }, useSimple: function useSimple() {
                V = !1;
            } }), u(u.S + u.F * !U, "Object", { create: H, defineProperty: Y, defineProperties: J, getOwnPropertyDescriptor: $, getOwnPropertyNames: Z, getOwnPropertySymbols: Q }), N && u(u.S + u.F * (!U || f(function () {
            var t = k();return "[null]" != j([t]) || "{}" != j({ a: t }) || "{}" != j(Object(t));
        })), "JSON", { stringify: function stringify(t) {
                if (void 0 !== t && !K(t)) {
                    for (var n, r, e = [t], i = 1; arguments.length > i;) {
                        e.push(arguments[i++]);
                    }return n = e[1], "function" == typeof n && (r = n), !r && m(n) || (n = function n(t, _n) {
                        if (r && (_n = r.call(this, t, _n)), !K(_n)) return _n;
                    }), e[1] = n, j.apply(N, e);
                }
            } }), k.prototype[R] || t(42)(k.prototype, R, k.prototype.valueOf), l(k, "Symbol"), l(Math, "Math", !0), l(e.JSON, "JSON", !0);
    }, { 101: 101, 103: 103, 117: 117, 120: 120, 124: 124, 126: 126, 127: 127, 128: 128, 29: 29, 32: 32, 33: 33, 35: 35, 40: 40, 41: 41, 42: 42, 49: 49, 59: 59, 60: 60, 66: 66, 7: 7, 71: 71, 72: 72, 75: 75, 76: 76, 77: 77, 78: 78, 81: 81, 82: 82, 92: 92, 94: 94 }], 255: [function (t, n, r) {
        "use strict";
        var e = t(33),
            i = t(123),
            o = t(122),
            u = t(7),
            c = t(114),
            a = t(118),
            f = t(51),
            s = t(40).ArrayBuffer,
            l = t(104),
            h = o.ArrayBuffer,
            v = o.DataView,
            p = i.ABV && s.isView,
            d = h.prototype.slice,
            y = i.VIEW;e(e.G + e.W + e.F * (s !== h), { ArrayBuffer: h }), e(e.S + e.F * !i.CONSTR, "ArrayBuffer", { isView: function isView(t) {
                return p && p(t) || f(t) && y in t;
            } }), e(e.P + e.U + e.F * t(35)(function () {
            return !new h(2).slice(1, void 0).byteLength;
        }), "ArrayBuffer", { slice: function slice(t, n) {
                if (void 0 !== d && void 0 === n) return d.call(u(this), t);for (var r = u(this).byteLength, e = c(t, r), i = c(void 0 === n ? r : n, r), o = new (l(this, h))(a(i - e)), f = new v(this), s = new v(o), p = 0; e < i;) {
                    s.setUint8(p++, f.getUint8(e++));
                }return o;
            } }), t(100)("ArrayBuffer");
    }, { 100: 100, 104: 104, 114: 114, 118: 118, 122: 122, 123: 123, 33: 33, 35: 35, 40: 40, 51: 51, 7: 7 }], 256: [function (t, n, r) {
        var e = t(33);e(e.G + e.W + e.F * !t(123).ABV, { DataView: t(122).DataView });
    }, { 122: 122, 123: 123, 33: 33 }], 257: [function (t, n, r) {
        t(121)("Float32", 4, function (t) {
            return function Float32Array(n, r, e) {
                return t(this, n, r, e);
            };
        });
    }, { 121: 121 }], 258: [function (t, n, r) {
        t(121)("Float64", 8, function (t) {
            return function Float64Array(n, r, e) {
                return t(this, n, r, e);
            };
        });
    }, { 121: 121 }], 259: [function (t, n, r) {
        t(121)("Int16", 2, function (t) {
            return function Int16Array(n, r, e) {
                return t(this, n, r, e);
            };
        });
    }, { 121: 121 }], 260: [function (t, n, r) {
        t(121)("Int32", 4, function (t) {
            return function Int32Array(n, r, e) {
                return t(this, n, r, e);
            };
        });
    }, { 121: 121 }], 261: [function (t, n, r) {
        t(121)("Int8", 1, function (t) {
            return function Int8Array(n, r, e) {
                return t(this, n, r, e);
            };
        });
    }, { 121: 121 }], 262: [function (t, n, r) {
        t(121)("Uint16", 2, function (t) {
            return function Uint16Array(n, r, e) {
                return t(this, n, r, e);
            };
        });
    }, { 121: 121 }], 263: [function (t, n, r) {
        t(121)("Uint32", 4, function (t) {
            return function Uint32Array(n, r, e) {
                return t(this, n, r, e);
            };
        });
    }, { 121: 121 }], 264: [function (t, n, r) {
        t(121)("Uint8", 1, function (t) {
            return function Uint8Array(n, r, e) {
                return t(this, n, r, e);
            };
        });
    }, { 121: 121 }], 265: [function (t, n, r) {
        t(121)("Uint8", 1, function (t) {
            return function Uint8ClampedArray(n, r, e) {
                return t(this, n, r, e);
            };
        }, !0);
    }, { 121: 121 }], 266: [function (t, n, r) {
        "use strict";
        var e,
            i = t(12)(0),
            o = t(94),
            u = t(66),
            c = t(70),
            a = t(21),
            f = t(51),
            s = t(35),
            l = t(125),
            h = u.getWeak,
            v = Object.isExtensible,
            p = a.ufstore,
            d = {},
            y = function y(t) {
            return function WeakMap() {
                return t(this, arguments.length > 0 ? arguments[0] : void 0);
            };
        },
            g = { get: function get(t) {
                if (f(t)) {
                    var n = h(t);return !0 === n ? p(l(this, "WeakMap")).get(t) : n ? n[this._i] : void 0;
                }
            }, set: function set(t, n) {
                return a.def(l(this, "WeakMap"), t, n);
            } },
            m = n.exports = t(22)("WeakMap", y, g, a, !0, !0);s(function () {
            return 7 != new m().set((Object.freeze || Object)(d), 7).get(d);
        }) && (e = a.getConstructor(y, "WeakMap"), c(e.prototype, g), u.NEED = !0, i(["delete", "has", "get", "set"], function (t) {
            var n = m.prototype,
                r = n[t];o(n, t, function (n, i) {
                if (f(n) && !v(n)) {
                    this._f || (this._f = new e());var o = this._f[t](n, i);return "set" == t ? this : o;
                }return r.call(this, n, i);
            });
        }));
    }, { 12: 12, 125: 125, 21: 21, 22: 22, 35: 35, 51: 51, 66: 66, 70: 70, 94: 94 }], 267: [function (t, n, r) {
        "use strict";
        var e = t(21),
            i = t(125);t(22)("WeakSet", function (t) {
            return function WeakSet() {
                return t(this, arguments.length > 0 ? arguments[0] : void 0);
            };
        }, { add: function add(t) {
                return e.def(i(this, "WeakSet"), t, !0);
            } }, e, !1, !0);
    }, { 125: 125, 21: 21, 22: 22 }], 268: [function (t, n, r) {
        "use strict";
        var e = t(33),
            i = t(38),
            o = t(119),
            u = t(118),
            c = t(3),
            a = t(15);e(e.P, "Array", { flatMap: function flatMap(t) {
                var n,
                    r,
                    e = o(this);return c(t), n = u(e.length), r = a(e, 0), i(r, e, e, n, 0, 1, t, arguments[1]), r;
            } }), t(5)("flatMap");
    }, { 118: 118, 119: 119, 15: 15, 3: 3, 33: 33, 38: 38, 5: 5 }], 269: [function (t, n, r) {
        "use strict";
        var e = t(33),
            i = t(38),
            o = t(119),
            u = t(118),
            c = t(116),
            a = t(15);e(e.P, "Array", { flatten: function flatten() {
                var t = arguments[0],
                    n = o(this),
                    r = u(n.length),
                    e = a(n, 0);return i(e, n, n, r, 0, void 0 === t ? 1 : c(t)), e;
            } }), t(5)("flatten");
    }, { 116: 116, 118: 118, 119: 119, 15: 15, 33: 33, 38: 38, 5: 5 }], 270: [function (t, n, r) {
        "use strict";
        var e = t(33),
            i = t(11)(!0);e(e.P, "Array", { includes: function includes(t) {
                return i(this, t, arguments.length > 1 ? arguments[1] : void 0);
            } }), t(5)("includes");
    }, { 11: 11, 33: 33, 5: 5 }], 271: [function (t, n, r) {
        var e = t(33),
            i = t(68)(),
            o = t(40).process,
            u = "process" == t(18)(o);e(e.G, { asap: function asap(t) {
                var n = u && o.domain;i(n ? n.bind(t) : t);
            } });
    }, { 18: 18, 33: 33, 40: 40, 68: 68 }], 272: [function (t, n, r) {
        var e = t(33),
            i = t(18);e(e.S, "Error", { isError: function isError(t) {
                return "Error" === i(t);
            } });
    }, { 18: 18, 33: 33 }], 273: [function (t, n, r) {
        var e = t(33);e(e.G, { global: t(40) });
    }, { 33: 33, 40: 40 }], 274: [function (t, n, r) {
        t(97)("Map");
    }, { 97: 97 }], 275: [function (t, n, r) {
        t(98)("Map");
    }, { 98: 98 }], 276: [function (t, n, r) {
        var e = t(33);e(e.P + e.R, "Map", { toJSON: t(20)("Map") });
    }, { 20: 20, 33: 33 }], 277: [function (t, n, r) {
        var e = t(33);e(e.S, "Math", { clamp: function clamp(t, n, r) {
                return Math.min(r, Math.max(n, t));
            } });
    }, { 33: 33 }], 278: [function (t, n, r) {
        var e = t(33);e(e.S, "Math", { DEG_PER_RAD: Math.PI / 180 });
    }, { 33: 33 }], 279: [function (t, n, r) {
        var e = t(33),
            i = 180 / Math.PI;e(e.S, "Math", { degrees: function degrees(t) {
                return t * i;
            } });
    }, { 33: 33 }], 280: [function (t, n, r) {
        var e = t(33),
            i = t(64),
            o = t(62);e(e.S, "Math", { fscale: function fscale(t, n, r, e, u) {
                return o(i(t, n, r, e, u));
            } });
    }, { 33: 33, 62: 62, 64: 64 }], 281: [function (t, n, r) {
        var e = t(33);e(e.S, "Math", { iaddh: function iaddh(t, n, r, e) {
                var i = t >>> 0,
                    o = n >>> 0,
                    u = r >>> 0;return o + (e >>> 0) + ((i & u | (i | u) & ~(i + u >>> 0)) >>> 31) | 0;
            } });
    }, { 33: 33 }], 282: [function (t, n, r) {
        var e = t(33);e(e.S, "Math", { imulh: function imulh(t, n) {
                var r = +t,
                    e = +n,
                    i = 65535 & r,
                    o = 65535 & e,
                    u = r >> 16,
                    c = e >> 16,
                    a = (u * o >>> 0) + (i * o >>> 16);return u * c + (a >> 16) + ((i * c >>> 0) + (65535 & a) >> 16);
            } });
    }, { 33: 33 }], 283: [function (t, n, r) {
        var e = t(33);e(e.S, "Math", { isubh: function isubh(t, n, r, e) {
                var i = t >>> 0,
                    o = n >>> 0,
                    u = r >>> 0;return o - (e >>> 0) - ((~i & u | ~(i ^ u) & i - u >>> 0) >>> 31) | 0;
            } });
    }, { 33: 33 }], 284: [function (t, n, r) {
        var e = t(33);e(e.S, "Math", { RAD_PER_DEG: 180 / Math.PI });
    }, { 33: 33 }], 285: [function (t, n, r) {
        var e = t(33),
            i = Math.PI / 180;e(e.S, "Math", { radians: function radians(t) {
                return t * i;
            } });
    }, { 33: 33 }], 286: [function (t, n, r) {
        var e = t(33);e(e.S, "Math", { scale: t(64) });
    }, { 33: 33, 64: 64 }], 287: [function (t, n, r) {
        var e = t(33);e(e.S, "Math", { signbit: function signbit(t) {
                return (t = +t) != t ? t : 0 == t ? 1 / t == 1 / 0 : t > 0;
            } });
    }, { 33: 33 }], 288: [function (t, n, r) {
        var e = t(33);e(e.S, "Math", { umulh: function umulh(t, n) {
                var r = +t,
                    e = +n,
                    i = 65535 & r,
                    o = 65535 & e,
                    u = r >>> 16,
                    c = e >>> 16,
                    a = (u * o >>> 0) + (i * o >>> 16);return u * c + (a >>> 16) + ((i * c >>> 0) + (65535 & a) >>> 16);
            } });
    }, { 33: 33 }], 289: [function (t, n, r) {
        "use strict";
        var e = t(33),
            i = t(119),
            o = t(3),
            u = t(72);t(29) && e(e.P + t(74), "Object", { __defineGetter__: function __defineGetter__(t, n) {
                u.f(i(this), t, { get: o(n), enumerable: !0, configurable: !0 });
            } });
    }, { 119: 119, 29: 29, 3: 3, 33: 33, 72: 72, 74: 74 }], 290: [function (t, n, r) {
        "use strict";
        var e = t(33),
            i = t(119),
            o = t(3),
            u = t(72);t(29) && e(e.P + t(74), "Object", { __defineSetter__: function __defineSetter__(t, n) {
                u.f(i(this), t, { set: o(n), enumerable: !0, configurable: !0 });
            } });
    }, { 119: 119, 29: 29, 3: 3, 33: 33, 72: 72, 74: 74 }], 291: [function (t, n, r) {
        var e = t(33),
            i = t(84)(!0);e(e.S, "Object", { entries: function entries(t) {
                return i(t);
            } });
    }, { 33: 33, 84: 84 }], 292: [function (t, n, r) {
        var e = t(33),
            i = t(85),
            o = t(117),
            u = t(75),
            c = t(24);e(e.S, "Object", { getOwnPropertyDescriptors: function getOwnPropertyDescriptors(t) {
                for (var n, r, e = o(t), a = u.f, f = i(e), s = {}, l = 0; f.length > l;) {
                    void 0 !== (r = a(e, n = f[l++])) && c(s, n, r);
                }return s;
            } });
    }, { 117: 117, 24: 24, 33: 33, 75: 75, 85: 85 }], 293: [function (t, n, r) {
        "use strict";
        var e = t(33),
            i = t(119),
            o = t(120),
            u = t(79),
            c = t(75).f;t(29) && e(e.P + t(74), "Object", { __lookupGetter__: function __lookupGetter__(t) {
                var n,
                    r = i(this),
                    e = o(t, !0);do {
                    if (n = c(r, e)) return n.get;
                } while (r = u(r));
            } });
    }, { 119: 119, 120: 120, 29: 29, 33: 33, 74: 74, 75: 75, 79: 79 }], 294: [function (t, n, r) {
        "use strict";
        var e = t(33),
            i = t(119),
            o = t(120),
            u = t(79),
            c = t(75).f;t(29) && e(e.P + t(74), "Object", { __lookupSetter__: function __lookupSetter__(t) {
                var n,
                    r = i(this),
                    e = o(t, !0);do {
                    if (n = c(r, e)) return n.set;
                } while (r = u(r));
            } });
    }, { 119: 119, 120: 120, 29: 29, 33: 33, 74: 74, 75: 75, 79: 79 }], 295: [function (t, n, r) {
        var e = t(33),
            i = t(84)(!1);e(e.S, "Object", { values: function values(t) {
                return i(t);
            } });
    }, { 33: 33, 84: 84 }], 296: [function (t, n, r) {
        "use strict";
        var e = t(33),
            i = t(40),
            o = t(23),
            u = t(68)(),
            c = t(128)("observable"),
            a = t(3),
            f = t(7),
            s = t(6),
            l = t(93),
            h = t(42),
            v = t(39),
            p = v.RETURN,
            d = function d(t) {
            return null == t ? void 0 : a(t);
        },
            y = function y(t) {
            var n = t._c;n && (t._c = void 0, n());
        },
            g = function g(t) {
            return void 0 === t._o;
        },
            m = function m(t) {
            g(t) || (t._o = void 0, y(t));
        },
            b = function b(t, n) {
            f(t), this._c = void 0, this._o = t, t = new x(this);try {
                var r = n(t),
                    e = r;null != r && ("function" == typeof r.unsubscribe ? r = function r() {
                    e.unsubscribe();
                } : a(r), this._c = r);
            } catch (n) {
                return void t.error(n);
            }g(this) && y(this);
        };b.prototype = l({}, { unsubscribe: function unsubscribe() {
                m(this);
            } });var x = function x(t) {
            this._s = t;
        };x.prototype = l({}, { next: function next(t) {
                var n = this._s;if (!g(n)) {
                    var r = n._o;try {
                        var e = d(r.next);if (e) return e.call(r, t);
                    } catch (t) {
                        try {
                            m(n);
                        } finally {
                            throw t;
                        }
                    }
                }
            }, error: function error(t) {
                var n = this._s;if (g(n)) throw t;var r = n._o;n._o = void 0;try {
                    var e = d(r.error);if (!e) throw t;t = e.call(r, t);
                } catch (t) {
                    try {
                        y(n);
                    } finally {
                        throw t;
                    }
                }return y(n), t;
            }, complete: function complete(t) {
                var n = this._s;if (!g(n)) {
                    var r = n._o;n._o = void 0;try {
                        var e = d(r.complete);t = e ? e.call(r, t) : void 0;
                    } catch (t) {
                        try {
                            y(n);
                        } finally {
                            throw t;
                        }
                    }return y(n), t;
                }
            } });var S = function Observable(t) {
            s(this, S, "Observable", "_f")._f = a(t);
        };l(S.prototype, { subscribe: function subscribe(t) {
                return new b(t, this._f);
            }, forEach: function forEach(t) {
                var n = this;return new (o.Promise || i.Promise)(function (r, e) {
                    a(t);var i = n.subscribe({ next: function next(n) {
                            try {
                                return t(n);
                            } catch (t) {
                                e(t), i.unsubscribe();
                            }
                        }, error: e, complete: r });
                });
            } }), l(S, { from: function from(t) {
                var n = "function" == typeof this ? this : S,
                    r = d(f(t)[c]);if (r) {
                    var e = f(r.call(t));return e.constructor === n ? e : new n(function (t) {
                        return e.subscribe(t);
                    });
                }return new n(function (n) {
                    var r = !1;return u(function () {
                        if (!r) {
                            try {
                                if (v(t, !1, function (t) {
                                    if (n.next(t), r) return p;
                                }) === p) return;
                            } catch (t) {
                                if (r) throw t;return void n.error(t);
                            }n.complete();
                        }
                    }), function () {
                        r = !0;
                    };
                });
            }, of: function of() {
                for (var t = 0, n = arguments.length, r = Array(n); t < n;) {
                    r[t] = arguments[t++];
                }return new ("function" == typeof this ? this : S)(function (t) {
                    var n = !1;return u(function () {
                        if (!n) {
                            for (var e = 0; e < r.length; ++e) {
                                if (t.next(r[e]), n) return;
                            }t.complete();
                        }
                    }), function () {
                        n = !0;
                    };
                });
            } }), h(S.prototype, c, function () {
            return this;
        }), e(e.G, { Observable: S }), t(100)("Observable");
    }, { 100: 100, 128: 128, 23: 23, 3: 3, 33: 33, 39: 39, 40: 40, 42: 42, 6: 6, 68: 68, 7: 7, 93: 93 }], 297: [function (t, n, r) {
        "use strict";
        var e = t(33),
            i = t(23),
            o = t(40),
            u = t(104),
            c = t(91);e(e.P + e.R, "Promise", { finally: function _finally(t) {
                var n = u(this, i.Promise || o.Promise),
                    r = "function" == typeof t;return this.then(r ? function (r) {
                    return c(n, t()).then(function () {
                        return r;
                    });
                } : t, r ? function (r) {
                    return c(n, t()).then(function () {
                        throw r;
                    });
                } : t);
            } });
    }, { 104: 104, 23: 23, 33: 33, 40: 40, 91: 91 }], 298: [function (t, n, r) {
        "use strict";
        var e = t(33),
            i = t(69),
            o = t(90);e(e.S, "Promise", { try: function _try(t) {
                var n = i.f(this),
                    r = o(t);return (r.e ? n.reject : n.resolve)(r.v), n.promise;
            } });
    }, { 33: 33, 69: 69, 90: 90 }], 299: [function (t, n, r) {
        var e = t(67),
            i = t(7),
            o = e.key,
            u = e.set;e.exp({ defineMetadata: function defineMetadata(t, n, r, e) {
                u(t, n, i(r), o(e));
            } });
    }, { 67: 67, 7: 7 }], 300: [function (t, n, r) {
        var e = t(67),
            i = t(7),
            o = e.key,
            u = e.map,
            c = e.store;e.exp({ deleteMetadata: function deleteMetadata(t, n) {
                var r = arguments.length < 3 ? void 0 : o(arguments[2]),
                    e = u(i(n), r, !1);if (void 0 === e || !e.delete(t)) return !1;if (e.size) return !0;var a = c.get(n);return a.delete(r), !!a.size || c.delete(n);
            } });
    }, { 67: 67, 7: 7 }], 301: [function (t, n, r) {
        var e = t(231),
            i = t(10),
            o = t(67),
            u = t(7),
            c = t(79),
            a = o.keys,
            f = o.key,
            s = function s(t, n) {
            var r = a(t, n),
                o = c(t);if (null === o) return r;var u = s(o, n);return u.length ? r.length ? i(new e(r.concat(u))) : u : r;
        };o.exp({ getMetadataKeys: function getMetadataKeys(t) {
                return s(u(t), arguments.length < 2 ? void 0 : f(arguments[1]));
            } });
    }, { 10: 10, 231: 231, 67: 67, 7: 7, 79: 79 }], 302: [function (t, n, r) {
        var e = t(67),
            i = t(7),
            o = t(79),
            u = e.has,
            c = e.get,
            a = e.key,
            f = function f(t, n, r) {
            if (u(t, n, r)) return c(t, n, r);var e = o(n);return null !== e ? f(t, e, r) : void 0;
        };e.exp({ getMetadata: function getMetadata(t, n) {
                return f(t, i(n), arguments.length < 3 ? void 0 : a(arguments[2]));
            } });
    }, { 67: 67, 7: 7, 79: 79 }], 303: [function (t, n, r) {
        var e = t(67),
            i = t(7),
            o = e.keys,
            u = e.key;e.exp({ getOwnMetadataKeys: function getOwnMetadataKeys(t) {
                return o(i(t), arguments.length < 2 ? void 0 : u(arguments[1]));
            } });
    }, { 67: 67, 7: 7 }], 304: [function (t, n, r) {
        var e = t(67),
            i = t(7),
            o = e.get,
            u = e.key;e.exp({ getOwnMetadata: function getOwnMetadata(t, n) {
                return o(t, i(n), arguments.length < 3 ? void 0 : u(arguments[2]));
            } });
    }, { 67: 67, 7: 7 }], 305: [function (t, n, r) {
        var e = t(67),
            i = t(7),
            o = t(79),
            u = e.has,
            c = e.key,
            a = function a(t, n, r) {
            if (u(t, n, r)) return !0;var e = o(n);return null !== e && a(t, e, r);
        };e.exp({ hasMetadata: function hasMetadata(t, n) {
                return a(t, i(n), arguments.length < 3 ? void 0 : c(arguments[2]));
            } });
    }, { 67: 67, 7: 7, 79: 79 }], 306: [function (t, n, r) {
        var e = t(67),
            i = t(7),
            o = e.has,
            u = e.key;e.exp({ hasOwnMetadata: function hasOwnMetadata(t, n) {
                return o(t, i(n), arguments.length < 3 ? void 0 : u(arguments[2]));
            } });
    }, { 67: 67, 7: 7 }], 307: [function (t, n, r) {
        var e = t(67),
            i = t(7),
            o = t(3),
            u = e.key,
            c = e.set;e.exp({ metadata: function metadata(t, n) {
                return function decorator(r, e) {
                    c(t, n, (void 0 !== e ? i : o)(r), u(e));
                };
            } });
    }, { 3: 3, 67: 67, 7: 7 }], 308: [function (t, n, r) {
        t(97)("Set");
    }, { 97: 97 }], 309: [function (t, n, r) {
        t(98)("Set");
    }, { 98: 98 }], 310: [function (t, n, r) {
        var e = t(33);e(e.P + e.R, "Set", { toJSON: t(20)("Set") });
    }, { 20: 20, 33: 33 }], 311: [function (t, n, r) {
        "use strict";
        var e = t(33),
            i = t(106)(!0);e(e.P, "String", { at: function at(t) {
                return i(this, t);
            } });
    }, { 106: 106, 33: 33 }], 312: [function (t, n, r) {
        "use strict";
        var e = t(33),
            i = t(28),
            o = t(118),
            u = t(52),
            c = t(37),
            a = RegExp.prototype,
            f = function f(t, n) {
            this._r = t, this._s = n;
        };t(54)(f, "RegExp String", function next() {
            var t = this._r.exec(this._s);return { value: t, done: null === t };
        }), e(e.P, "String", { matchAll: function matchAll(t) {
                if (i(this), !u(t)) throw TypeError(t + " is not a regexp!");var n = String(this),
                    r = "flags" in a ? String(t.flags) : c.call(t),
                    e = new RegExp(t.source, ~r.indexOf("g") ? r : "g" + r);return e.lastIndex = o(t.lastIndex), new f(e, n);
            } });
    }, { 118: 118, 28: 28, 33: 33, 37: 37, 52: 52, 54: 54 }], 313: [function (t, n, r) {
        "use strict";
        var e = t(33),
            i = t(109);e(e.P, "String", { padEnd: function padEnd(t) {
                return i(this, t, arguments.length > 1 ? arguments[1] : void 0, !1);
            } });
    }, { 109: 109, 33: 33 }], 314: [function (t, n, r) {
        "use strict";
        var e = t(33),
            i = t(109);e(e.P, "String", { padStart: function padStart(t) {
                return i(this, t, arguments.length > 1 ? arguments[1] : void 0, !0);
            } });
    }, { 109: 109, 33: 33 }], 315: [function (t, n, r) {
        "use strict";
        t(111)("trimLeft", function (t) {
            return function trimLeft() {
                return t(this, 1);
            };
        }, "trimStart");
    }, { 111: 111 }], 316: [function (t, n, r) {
        "use strict";
        t(111)("trimRight", function (t) {
            return function trimRight() {
                return t(this, 2);
            };
        }, "trimEnd");
    }, { 111: 111 }], 317: [function (t, n, r) {
        t(126)("asyncIterator");
    }, { 126: 126 }], 318: [function (t, n, r) {
        t(126)("observable");
    }, { 126: 126 }], 319: [function (t, n, r) {
        var e = t(33);e(e.S, "System", { global: t(40) });
    }, { 33: 33, 40: 40 }], 320: [function (t, n, r) {
        t(97)("WeakMap");
    }, { 97: 97 }], 321: [function (t, n, r) {
        t(98)("WeakMap");
    }, { 98: 98 }], 322: [function (t, n, r) {
        t(97)("WeakSet");
    }, { 97: 97 }], 323: [function (t, n, r) {
        t(98)("WeakSet");
    }, { 98: 98 }], 324: [function (t, n, r) {
        for (var e = t(141), i = t(81), o = t(94), u = t(40), c = t(42), a = t(58), f = t(128), s = f("iterator"), l = f("toStringTag"), h = a.Array, v = { CSSRuleList: !0, CSSStyleDeclaration: !1, CSSValueList: !1, ClientRectList: !1, DOMRectList: !1, DOMStringList: !1, DOMTokenList: !0, DataTransferItemList: !1, FileList: !1, HTMLAllCollection: !1, HTMLCollection: !1, HTMLFormElement: !1, HTMLSelectElement: !1, MediaList: !0, MimeTypeArray: !1, NamedNodeMap: !1, NodeList: !0, PaintRequestList: !1, Plugin: !1, PluginArray: !1, SVGLengthList: !1, SVGNumberList: !1, SVGPathSegList: !1, SVGPointList: !1, SVGStringList: !1, SVGTransformList: !1, SourceBufferList: !1, StyleSheetList: !0, TextTrackCueList: !1, TextTrackList: !1, TouchList: !1 }, p = i(v), d = 0; d < p.length; d++) {
            var y,
                g = p[d],
                m = v[g],
                b = u[g],
                x = b && b.prototype;if (x && (x[s] || c(x, s, h), x[l] || c(x, l, g), a[g] = h, m)) for (y in e) {
                x[y] || o(x, y, e[y], !0);
            }
        }
    }, { 128: 128, 141: 141, 40: 40, 42: 42, 58: 58, 81: 81, 94: 94 }], 325: [function (t, n, r) {
        var e = t(33),
            i = t(113);e(e.G + e.B, { setImmediate: i.set, clearImmediate: i.clear });
    }, { 113: 113, 33: 33 }], 326: [function (t, n, r) {
        var e = t(40),
            i = t(33),
            o = t(46),
            u = t(88),
            c = e.navigator,
            a = !!c && /MSIE .\./.test(c.userAgent),
            f = function f(t) {
            return a ? function (n, r) {
                return t(o(u, [].slice.call(arguments, 2), "function" == typeof n ? n : Function(n)), r);
            } : t;
        };i(i.G + i.B + i.F * a, { setTimeout: f(e.setTimeout), setInterval: f(e.setInterval) });
    }, { 33: 33, 40: 40, 46: 46, 88: 88 }], 327: [function (t, n, r) {
        t(254), t(191), t(193), t(192), t(195), t(197), t(202), t(196), t(194), t(204), t(203), t(199), t(200), t(198), t(190), t(201), t(205), t(206), t(157), t(159), t(158), t(208), t(207), t(178), t(188), t(189), t(179), t(180), t(181), t(182), t(183), t(184), t(185), t(186), t(187), t(161), t(162), t(163), t(164), t(165), t(166), t(167), t(168), t(169), t(170), t(171), t(172), t(173), t(174), t(175), t(176), t(177), t(241), t(246), t(253), t(244), t(236), t(237), t(242), t(247), t(249), t(232), t(233), t(234), t(235), t(238), t(239), t(240), t(243), t(245), t(248), t(250), t(251), t(252), t(152), t(154), t(153), t(156), t(155), t(140), t(138), t(145), t(142), t(148), t(150), t(137), t(144), t(134), t(149), t(132), t(147), t(146), t(139), t(143), t(131), t(133), t(136), t(135), t(151), t(141), t(224), t(230), t(225), t(226), t(227), t(228), t(229), t(209), t(160), t(231), t(266), t(267), t(255), t(256), t(261), t(264), t(265), t(259), t(262), t(260), t(263), t(257), t(258), t(210), t(211), t(212), t(213), t(214), t(217), t(215), t(216), t(218), t(219), t(220), t(221), t(223), t(222), t(270), t(268), t(269), t(311), t(314), t(313), t(315), t(316), t(312), t(317), t(318), t(292), t(295), t(291), t(289), t(290), t(293), t(294), t(276), t(310), t(275), t(309), t(321), t(323), t(274), t(308), t(320), t(322), t(273), t(319), t(272), t(277), t(278), t(279), t(280), t(281), t(283), t(282), t(284), t(285), t(286), t(288), t(287), t(297), t(298), t(299), t(300), t(302), t(301), t(304), t(303), t(305), t(306), t(307), t(271), t(296), t(326), t(325), t(324), n.exports = t(23);
    }, { 131: 131, 132: 132, 133: 133, 134: 134, 135: 135, 136: 136, 137: 137, 138: 138, 139: 139, 140: 140, 141: 141, 142: 142, 143: 143, 144: 144, 145: 145, 146: 146, 147: 147, 148: 148, 149: 149, 150: 150, 151: 151, 152: 152, 153: 153, 154: 154, 155: 155, 156: 156, 157: 157, 158: 158, 159: 159, 160: 160, 161: 161, 162: 162, 163: 163, 164: 164, 165: 165, 166: 166, 167: 167, 168: 168, 169: 169, 170: 170, 171: 171, 172: 172, 173: 173, 174: 174, 175: 175, 176: 176, 177: 177, 178: 178, 179: 179, 180: 180, 181: 181, 182: 182, 183: 183, 184: 184, 185: 185,
        186: 186, 187: 187, 188: 188, 189: 189, 190: 190, 191: 191, 192: 192, 193: 193, 194: 194, 195: 195, 196: 196, 197: 197, 198: 198, 199: 199, 200: 200, 201: 201, 202: 202, 203: 203, 204: 204, 205: 205, 206: 206, 207: 207, 208: 208, 209: 209, 210: 210, 211: 211, 212: 212, 213: 213, 214: 214, 215: 215, 216: 216, 217: 217, 218: 218, 219: 219, 220: 220, 221: 221, 222: 222, 223: 223, 224: 224, 225: 225, 226: 226, 227: 227, 228: 228, 229: 229, 23: 23, 230: 230, 231: 231, 232: 232, 233: 233, 234: 234, 235: 235, 236: 236, 237: 237, 238: 238, 239: 239, 240: 240, 241: 241, 242: 242, 243: 243, 244: 244, 245: 245, 246: 246, 247: 247, 248: 248, 249: 249, 250: 250, 251: 251, 252: 252, 253: 253, 254: 254, 255: 255, 256: 256, 257: 257, 258: 258, 259: 259, 260: 260, 261: 261, 262: 262, 263: 263, 264: 264, 265: 265, 266: 266, 267: 267, 268: 268, 269: 269, 270: 270, 271: 271, 272: 272, 273: 273, 274: 274, 275: 275, 276: 276, 277: 277, 278: 278, 279: 279, 280: 280, 281: 281, 282: 282, 283: 283, 284: 284, 285: 285, 286: 286, 287: 287, 288: 288, 289: 289, 290: 290, 291: 291, 292: 292, 293: 293, 294: 294, 295: 295, 296: 296, 297: 297, 298: 298, 299: 299, 300: 300, 301: 301, 302: 302, 303: 303, 304: 304, 305: 305, 306: 306, 307: 307, 308: 308, 309: 309, 310: 310, 311: 311, 312: 312, 313: 313, 314: 314, 315: 315, 316: 316, 317: 317, 318: 318, 319: 319, 320: 320, 321: 321, 322: 322, 323: 323, 324: 324, 325: 325, 326: 326 }], 328: [function (t, n, r) {
        (function (t) {
            !function (t) {
                "use strict";
                function wrap(t, n, r, e) {
                    var i = n && n.prototype instanceof Generator ? n : Generator,
                        o = Object.create(i.prototype),
                        u = new Context(e || []);return o._invoke = makeInvokeMethod(t, r, u), o;
                }function tryCatch(t, n, r) {
                    try {
                        return { type: "normal", arg: t.call(n, r) };
                    } catch (t) {
                        return { type: "throw", arg: t };
                    }
                }function Generator() {}function GeneratorFunction() {}function GeneratorFunctionPrototype() {}function defineIteratorMethods(t) {
                    ["next", "throw", "return"].forEach(function (n) {
                        t[n] = function (t) {
                            return this._invoke(n, t);
                        };
                    });
                }function AsyncIterator(n) {
                    function invoke(t, r, e, o) {
                        var u = tryCatch(n[t], n, r);if ("throw" !== u.type) {
                            var c = u.arg,
                                a = c.value;return a && "object" == (typeof a === "undefined" ? "undefined" : _typeof(a)) && i.call(a, "__await") ? Promise.resolve(a.__await).then(function (t) {
                                invoke("next", t, e, o);
                            }, function (t) {
                                invoke("throw", t, e, o);
                            }) : Promise.resolve(a).then(function (t) {
                                c.value = t, e(c);
                            }, o);
                        }o(u.arg);
                    }function enqueue(t, n) {
                        function callInvokeWithMethodAndArg() {
                            return new Promise(function (r, e) {
                                invoke(t, n, r, e);
                            });
                        }return r = r ? r.then(callInvokeWithMethodAndArg, callInvokeWithMethodAndArg) : callInvokeWithMethodAndArg();
                    }"object" == _typeof(t.process) && t.process.domain && (invoke = t.process.domain.bind(invoke));var r;this._invoke = enqueue;
                }function makeInvokeMethod(t, n, r) {
                    var e = l;return function invoke(i, o) {
                        if (e === v) throw new Error("Generator is already running");if (e === p) {
                            if ("throw" === i) throw o;return doneResult();
                        }for (r.method = i, r.arg = o;;) {
                            var u = r.delegate;if (u) {
                                var c = maybeInvokeDelegate(u, r);if (c) {
                                    if (c === d) continue;return c;
                                }
                            }if ("next" === r.method) r.sent = r._sent = r.arg;else if ("throw" === r.method) {
                                if (e === l) throw e = p, r.arg;r.dispatchException(r.arg);
                            } else "return" === r.method && r.abrupt("return", r.arg);e = v;var a = tryCatch(t, n, r);if ("normal" === a.type) {
                                if (e = r.done ? p : h, a.arg === d) continue;return { value: a.arg, done: r.done };
                            }"throw" === a.type && (e = p, r.method = "throw", r.arg = a.arg);
                        }
                    };
                }function maybeInvokeDelegate(t, n) {
                    var e = t.iterator[n.method];if (e === r) {
                        if (n.delegate = null, "throw" === n.method) {
                            if (t.iterator.return && (n.method = "return", n.arg = r, maybeInvokeDelegate(t, n), "throw" === n.method)) return d;n.method = "throw", n.arg = new TypeError("The iterator does not provide a 'throw' method");
                        }return d;
                    }var i = tryCatch(e, t.iterator, n.arg);if ("throw" === i.type) return n.method = "throw", n.arg = i.arg, n.delegate = null, d;var o = i.arg;return o ? o.done ? (n[t.resultName] = o.value, n.next = t.nextLoc, "return" !== n.method && (n.method = "next", n.arg = r), n.delegate = null, d) : o : (n.method = "throw", n.arg = new TypeError("iterator result is not an object"), n.delegate = null, d);
                }function pushTryEntry(t) {
                    var n = { tryLoc: t[0] };1 in t && (n.catchLoc = t[1]), 2 in t && (n.finallyLoc = t[2], n.afterLoc = t[3]), this.tryEntries.push(n);
                }function resetTryEntry(t) {
                    var n = t.completion || {};n.type = "normal", delete n.arg, t.completion = n;
                }function Context(t) {
                    this.tryEntries = [{ tryLoc: "root" }], t.forEach(pushTryEntry, this), this.reset(!0);
                }function values(t) {
                    if (t) {
                        var n = t[u];if (n) return n.call(t);if ("function" == typeof t.next) return t;if (!isNaN(t.length)) {
                            var e = -1,
                                o = function next() {
                                for (; ++e < t.length;) {
                                    if (i.call(t, e)) return next.value = t[e], next.done = !1, next;
                                }return next.value = r, next.done = !0, next;
                            };return o.next = o;
                        }
                    }return { next: doneResult };
                }function doneResult() {
                    return { value: r, done: !0 };
                }var r,
                    e = Object.prototype,
                    i = e.hasOwnProperty,
                    o = "function" == typeof Symbol ? Symbol : {},
                    u = o.iterator || "@@iterator",
                    c = o.asyncIterator || "@@asyncIterator",
                    a = o.toStringTag || "@@toStringTag",
                    f = "object" == (typeof n === "undefined" ? "undefined" : _typeof(n)),
                    s = t.regeneratorRuntime;if (s) return void (f && (n.exports = s));s = t.regeneratorRuntime = f ? n.exports : {}, s.wrap = wrap;var l = "suspendedStart",
                    h = "suspendedYield",
                    v = "executing",
                    p = "completed",
                    d = {},
                    y = {};y[u] = function () {
                    return this;
                };var g = Object.getPrototypeOf,
                    m = g && g(g(values([])));m && m !== e && i.call(m, u) && (y = m);var b = GeneratorFunctionPrototype.prototype = Generator.prototype = Object.create(y);GeneratorFunction.prototype = b.constructor = GeneratorFunctionPrototype, GeneratorFunctionPrototype.constructor = GeneratorFunction, GeneratorFunctionPrototype[a] = GeneratorFunction.displayName = "GeneratorFunction", s.isGeneratorFunction = function (t) {
                    var n = "function" == typeof t && t.constructor;return !!n && (n === GeneratorFunction || "GeneratorFunction" === (n.displayName || n.name));
                }, s.mark = function (t) {
                    return Object.setPrototypeOf ? Object.setPrototypeOf(t, GeneratorFunctionPrototype) : (t.__proto__ = GeneratorFunctionPrototype, a in t || (t[a] = "GeneratorFunction")), t.prototype = Object.create(b), t;
                }, s.awrap = function (t) {
                    return { __await: t };
                }, defineIteratorMethods(AsyncIterator.prototype), AsyncIterator.prototype[c] = function () {
                    return this;
                }, s.AsyncIterator = AsyncIterator, s.async = function (t, n, r, e) {
                    var i = new AsyncIterator(wrap(t, n, r, e));return s.isGeneratorFunction(n) ? i : i.next().then(function (t) {
                        return t.done ? t.value : i.next();
                    });
                }, defineIteratorMethods(b), b[a] = "Generator", b[u] = function () {
                    return this;
                }, b.toString = function () {
                    return "[object Generator]";
                }, s.keys = function (t) {
                    var n = [];for (var r in t) {
                        n.push(r);
                    }return n.reverse(), function next() {
                        for (; n.length;) {
                            var r = n.pop();if (r in t) return next.value = r, next.done = !1, next;
                        }return next.done = !0, next;
                    };
                }, s.values = values, Context.prototype = { constructor: Context, reset: function reset(t) {
                        if (this.prev = 0, this.next = 0, this.sent = this._sent = r, this.done = !1, this.delegate = null, this.method = "next", this.arg = r, this.tryEntries.forEach(resetTryEntry), !t) for (var n in this) {
                            "t" === n.charAt(0) && i.call(this, n) && !isNaN(+n.slice(1)) && (this[n] = r);
                        }
                    }, stop: function stop() {
                        this.done = !0;var t = this.tryEntries[0],
                            n = t.completion;if ("throw" === n.type) throw n.arg;return this.rval;
                    }, dispatchException: function dispatchException(t) {
                        function handle(e, i) {
                            return u.type = "throw", u.arg = t, n.next = e, i && (n.method = "next", n.arg = r), !!i;
                        }if (this.done) throw t;for (var n = this, e = this.tryEntries.length - 1; e >= 0; --e) {
                            var o = this.tryEntries[e],
                                u = o.completion;if ("root" === o.tryLoc) return handle("end");if (o.tryLoc <= this.prev) {
                                var c = i.call(o, "catchLoc"),
                                    a = i.call(o, "finallyLoc");if (c && a) {
                                    if (this.prev < o.catchLoc) return handle(o.catchLoc, !0);if (this.prev < o.finallyLoc) return handle(o.finallyLoc);
                                } else if (c) {
                                    if (this.prev < o.catchLoc) return handle(o.catchLoc, !0);
                                } else {
                                    if (!a) throw new Error("try statement without catch or finally");if (this.prev < o.finallyLoc) return handle(o.finallyLoc);
                                }
                            }
                        }
                    }, abrupt: function abrupt(t, n) {
                        for (var r = this.tryEntries.length - 1; r >= 0; --r) {
                            var e = this.tryEntries[r];if (e.tryLoc <= this.prev && i.call(e, "finallyLoc") && this.prev < e.finallyLoc) {
                                var o = e;break;
                            }
                        }o && ("break" === t || "continue" === t) && o.tryLoc <= n && n <= o.finallyLoc && (o = null);var u = o ? o.completion : {};return u.type = t, u.arg = n, o ? (this.method = "next", this.next = o.finallyLoc, d) : this.complete(u);
                    }, complete: function complete(t, n) {
                        if ("throw" === t.type) throw t.arg;return "break" === t.type || "continue" === t.type ? this.next = t.arg : "return" === t.type ? (this.rval = this.arg = t.arg, this.method = "return", this.next = "end") : "normal" === t.type && n && (this.next = n), d;
                    }, finish: function finish(t) {
                        for (var n = this.tryEntries.length - 1; n >= 0; --n) {
                            var r = this.tryEntries[n];if (r.finallyLoc === t) return this.complete(r.completion, r.afterLoc), resetTryEntry(r), d;
                        }
                    }, catch: function _catch(t) {
                        for (var n = this.tryEntries.length - 1; n >= 0; --n) {
                            var r = this.tryEntries[n];if (r.tryLoc === t) {
                                var e = r.completion;if ("throw" === e.type) {
                                    var i = e.arg;resetTryEntry(r);
                                }return i;
                            }
                        }throw new Error("illegal catch attempt");
                    }, delegateYield: function delegateYield(t, n, e) {
                        return this.delegate = { iterator: values(t), resultName: n, nextLoc: e }, "next" === this.method && (this.arg = r), d;
                    } };
            }("object" == (typeof t === "undefined" ? "undefined" : _typeof(t)) ? t : "object" == (typeof window === "undefined" ? "undefined" : _typeof(window)) ? window : "object" == (typeof self === "undefined" ? "undefined" : _typeof(self)) ? self : this);
        }).call(this, "undefined" != typeof global ? global : "undefined" != typeof self ? self : "undefined" != typeof window ? window : {});
    }, {}] }, {}, [1]);
/* jshint ignore:end */
// end file: _es6_polyfill.min.js

// file: _progressbar.js
/* jshint ignore:start */
!function (a) {
    if ("object" == (typeof exports === "undefined" ? "undefined" : _typeof(exports)) && "undefined" != typeof module) module.exports = a();else if ("function" == typeof define && define.amd) define([], a);else {
        var b;b = "undefined" != typeof window ? window : "undefined" != typeof global ? global : "undefined" != typeof self ? self : this, b.ProgressBar = a();
    }
}(function () {
    var a;return function a(b, c, d) {
        function e(g, h) {
            if (!c[g]) {
                if (!b[g]) {
                    var i = "function" == typeof require && require;if (!h && i) return i(g, !0);if (f) return f(g, !0);var j = new Error("Cannot find module '" + g + "'");throw j.code = "MODULE_NOT_FOUND", j;
                }var k = c[g] = { exports: {} };b[g][0].call(k.exports, function (a) {
                    var c = b[g][1][a];return e(c ? c : a);
                }, k, k.exports, a, b, c, d);
            }return c[g].exports;
        }for (var f = "function" == typeof require && require, g = 0; g < d.length; g++) {
            e(d[g]);
        }return e;
    }({ 1: [function (b, c, d) {
            (function () {
                var b = this || Function("return this")(),
                    e = function () {
                    "use strict";
                    function e() {}function f(a, b) {
                        var c;for (c in a) {
                            Object.hasOwnProperty.call(a, c) && b(c);
                        }
                    }function g(a, b) {
                        return f(b, function (c) {
                            a[c] = b[c];
                        }), a;
                    }function h(a, b) {
                        f(b, function (c) {
                            "undefined" == typeof a[c] && (a[c] = b[c]);
                        });
                    }function i(a, b, c, d, e, f, g) {
                        var h,
                            i,
                            k,
                            l = a < f ? 0 : (a - f) / e;for (h in b) {
                            b.hasOwnProperty(h) && (i = g[h], k = "function" == typeof i ? i : o[i], b[h] = j(c[h], d[h], k, l));
                        }return b;
                    }function j(a, b, c, d) {
                        return a + (b - a) * c(d);
                    }function k(a, b) {
                        var c = n.prototype.filter,
                            d = a._filterArgs;f(c, function (e) {
                            "undefined" != typeof c[e][b] && c[e][b].apply(a, d);
                        });
                    }function l(a, b, c, d, e, f, g, h, j, l, m) {
                        v = b + c + d, w = Math.min(m || u(), v), x = w >= v, y = d - (v - w), a.isPlaying() && (x ? (j(g, a._attachment, y), a.stop(!0)) : (a._scheduleId = l(a._timeoutHandler, s), k(a, "beforeTween"), w < b + c ? i(1, e, f, g, 1, 1, h) : i(w, e, f, g, d, b + c, h), k(a, "afterTween"), j(e, a._attachment, y)));
                    }function m(a, b) {
                        var c = {},
                            d = typeof b === "undefined" ? "undefined" : _typeof(b);return "string" === d || "function" === d ? f(a, function (a) {
                            c[a] = b;
                        }) : f(a, function (a) {
                            c[a] || (c[a] = b[a] || q);
                        }), c;
                    }function n(a, b) {
                        this._currentState = a || {}, this._configured = !1, this._scheduleFunction = p, "undefined" != typeof b && this.setConfig(b);
                    }var o,
                        p,
                        q = "linear",
                        r = 500,
                        s = 1e3 / 60,
                        t = Date.now ? Date.now : function () {
                        return +new Date();
                    },
                        u = "undefined" != typeof SHIFTY_DEBUG_NOW ? SHIFTY_DEBUG_NOW : t;p = "undefined" != typeof window ? window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.oRequestAnimationFrame || window.msRequestAnimationFrame || window.mozCancelRequestAnimationFrame && window.mozRequestAnimationFrame || setTimeout : setTimeout;var v, w, x, y;return n.prototype.tween = function (a) {
                        return this._isTweening ? this : (void 0 === a && this._configured || this.setConfig(a), this._timestamp = u(), this._start(this.get(), this._attachment), this.resume());
                    }, n.prototype.setConfig = function (a) {
                        a = a || {}, this._configured = !0, this._attachment = a.attachment, this._pausedAtTime = null, this._scheduleId = null, this._delay = a.delay || 0, this._start = a.start || e, this._step = a.step || e, this._finish = a.finish || e, this._duration = a.duration || r, this._currentState = g({}, a.from || this.get()), this._originalState = this.get(), this._targetState = g({}, a.to || this.get());var b = this;this._timeoutHandler = function () {
                            l(b, b._timestamp, b._delay, b._duration, b._currentState, b._originalState, b._targetState, b._easing, b._step, b._scheduleFunction);
                        };var c = this._currentState,
                            d = this._targetState;return h(d, c), this._easing = m(c, a.easing || q), this._filterArgs = [c, this._originalState, d, this._easing], k(this, "tweenCreated"), this;
                    }, n.prototype.get = function () {
                        return g({}, this._currentState);
                    }, n.prototype.set = function (a) {
                        this._currentState = a;
                    }, n.prototype.pause = function () {
                        return this._pausedAtTime = u(), this._isPaused = !0, this;
                    }, n.prototype.resume = function () {
                        return this._isPaused && (this._timestamp += u() - this._pausedAtTime), this._isPaused = !1, this._isTweening = !0, this._timeoutHandler(), this;
                    }, n.prototype.seek = function (a) {
                        a = Math.max(a, 0);var b = u();return this._timestamp + a === 0 ? this : (this._timestamp = b - a, this.isPlaying() || (this._isTweening = !0, this._isPaused = !1, l(this, this._timestamp, this._delay, this._duration, this._currentState, this._originalState, this._targetState, this._easing, this._step, this._scheduleFunction, b), this.pause()), this);
                    }, n.prototype.stop = function (a) {
                        return this._isTweening = !1, this._isPaused = !1, this._timeoutHandler = e, (b.cancelAnimationFrame || b.webkitCancelAnimationFrame || b.oCancelAnimationFrame || b.msCancelAnimationFrame || b.mozCancelRequestAnimationFrame || b.clearTimeout)(this._scheduleId), a && (k(this, "beforeTween"), i(1, this._currentState, this._originalState, this._targetState, 1, 0, this._easing), k(this, "afterTween"), k(this, "afterTweenEnd"), this._finish.call(this, this._currentState, this._attachment)), this;
                    }, n.prototype.isPlaying = function () {
                        return this._isTweening && !this._isPaused;
                    }, n.prototype.setScheduleFunction = function (a) {
                        this._scheduleFunction = a;
                    }, n.prototype.dispose = function () {
                        var a;for (a in this) {
                            this.hasOwnProperty(a) && delete this[a];
                        }
                    }, n.prototype.filter = {}, n.prototype.formula = { linear: function linear(a) {
                            return a;
                        } }, o = n.prototype.formula, g(n, { now: u, each: f, tweenProps: i, tweenProp: j, applyFilter: k, shallowCopy: g, defaults: h, composeEasingObject: m }), "function" == typeof SHIFTY_DEBUG_NOW && (b.timeoutHandler = l), "object" == (typeof d === "undefined" ? "undefined" : _typeof(d)) ? c.exports = n : "function" == typeof a && a.amd ? a(function () {
                        return n;
                    }) : "undefined" == typeof b.Tweenable && (b.Tweenable = n), n;
                }();!function () {
                    e.shallowCopy(e.prototype.formula, { easeInQuad: function easeInQuad(a) {
                            return Math.pow(a, 2);
                        }, easeOutQuad: function easeOutQuad(a) {
                            return -(Math.pow(a - 1, 2) - 1);
                        }, easeInOutQuad: function easeInOutQuad(a) {
                            return (a /= .5) < 1 ? .5 * Math.pow(a, 2) : -.5 * ((a -= 2) * a - 2);
                        }, easeInCubic: function easeInCubic(a) {
                            return Math.pow(a, 3);
                        }, easeOutCubic: function easeOutCubic(a) {
                            return Math.pow(a - 1, 3) + 1;
                        }, easeInOutCubic: function easeInOutCubic(a) {
                            return (a /= .5) < 1 ? .5 * Math.pow(a, 3) : .5 * (Math.pow(a - 2, 3) + 2);
                        }, easeInQuart: function easeInQuart(a) {
                            return Math.pow(a, 4);
                        }, easeOutQuart: function easeOutQuart(a) {
                            return -(Math.pow(a - 1, 4) - 1);
                        }, easeInOutQuart: function easeInOutQuart(a) {
                            return (a /= .5) < 1 ? .5 * Math.pow(a, 4) : -.5 * ((a -= 2) * Math.pow(a, 3) - 2);
                        }, easeInQuint: function easeInQuint(a) {
                            return Math.pow(a, 5);
                        }, easeOutQuint: function easeOutQuint(a) {
                            return Math.pow(a - 1, 5) + 1;
                        }, easeInOutQuint: function easeInOutQuint(a) {
                            return (a /= .5) < 1 ? .5 * Math.pow(a, 5) : .5 * (Math.pow(a - 2, 5) + 2);
                        }, easeInSine: function easeInSine(a) {
                            return -Math.cos(a * (Math.PI / 2)) + 1;
                        }, easeOutSine: function easeOutSine(a) {
                            return Math.sin(a * (Math.PI / 2));
                        }, easeInOutSine: function easeInOutSine(a) {
                            return -.5 * (Math.cos(Math.PI * a) - 1);
                        }, easeInExpo: function easeInExpo(a) {
                            return 0 === a ? 0 : Math.pow(2, 10 * (a - 1));
                        }, easeOutExpo: function easeOutExpo(a) {
                            return 1 === a ? 1 : -Math.pow(2, -10 * a) + 1;
                        }, easeInOutExpo: function easeInOutExpo(a) {
                            return 0 === a ? 0 : 1 === a ? 1 : (a /= .5) < 1 ? .5 * Math.pow(2, 10 * (a - 1)) : .5 * (-Math.pow(2, -10 * --a) + 2);
                        }, easeInCirc: function easeInCirc(a) {
                            return -(Math.sqrt(1 - a * a) - 1);
                        }, easeOutCirc: function easeOutCirc(a) {
                            return Math.sqrt(1 - Math.pow(a - 1, 2));
                        }, easeInOutCirc: function easeInOutCirc(a) {
                            return (a /= .5) < 1 ? -.5 * (Math.sqrt(1 - a * a) - 1) : .5 * (Math.sqrt(1 - (a -= 2) * a) + 1);
                        }, easeOutBounce: function easeOutBounce(a) {
                            return a < 1 / 2.75 ? 7.5625 * a * a : a < 2 / 2.75 ? 7.5625 * (a -= 1.5 / 2.75) * a + .75 : a < 2.5 / 2.75 ? 7.5625 * (a -= 2.25 / 2.75) * a + .9375 : 7.5625 * (a -= 2.625 / 2.75) * a + .984375;
                        }, easeInBack: function easeInBack(a) {
                            var b = 1.70158;return a * a * ((b + 1) * a - b);
                        }, easeOutBack: function easeOutBack(a) {
                            var b = 1.70158;return (a -= 1) * a * ((b + 1) * a + b) + 1;
                        }, easeInOutBack: function easeInOutBack(a) {
                            var b = 1.70158;return (a /= .5) < 1 ? .5 * (a * a * (((b *= 1.525) + 1) * a - b)) : .5 * ((a -= 2) * a * (((b *= 1.525) + 1) * a + b) + 2);
                        }, elastic: function elastic(a) {
                            return -1 * Math.pow(4, -8 * a) * Math.sin((6 * a - 1) * (2 * Math.PI) / 2) + 1;
                        }, swingFromTo: function swingFromTo(a) {
                            var b = 1.70158;return (a /= .5) < 1 ? .5 * (a * a * (((b *= 1.525) + 1) * a - b)) : .5 * ((a -= 2) * a * (((b *= 1.525) + 1) * a + b) + 2);
                        }, swingFrom: function swingFrom(a) {
                            var b = 1.70158;return a * a * ((b + 1) * a - b);
                        }, swingTo: function swingTo(a) {
                            var b = 1.70158;return (a -= 1) * a * ((b + 1) * a + b) + 1;
                        }, bounce: function bounce(a) {
                            return a < 1 / 2.75 ? 7.5625 * a * a : a < 2 / 2.75 ? 7.5625 * (a -= 1.5 / 2.75) * a + .75 : a < 2.5 / 2.75 ? 7.5625 * (a -= 2.25 / 2.75) * a + .9375 : 7.5625 * (a -= 2.625 / 2.75) * a + .984375;
                        }, bouncePast: function bouncePast(a) {
                            return a < 1 / 2.75 ? 7.5625 * a * a : a < 2 / 2.75 ? 2 - (7.5625 * (a -= 1.5 / 2.75) * a + .75) : a < 2.5 / 2.75 ? 2 - (7.5625 * (a -= 2.25 / 2.75) * a + .9375) : 2 - (7.5625 * (a -= 2.625 / 2.75) * a + .984375);
                        }, easeFromTo: function easeFromTo(a) {
                            return (a /= .5) < 1 ? .5 * Math.pow(a, 4) : -.5 * ((a -= 2) * Math.pow(a, 3) - 2);
                        }, easeFrom: function easeFrom(a) {
                            return Math.pow(a, 4);
                        }, easeTo: function easeTo(a) {
                            return Math.pow(a, .25);
                        } });
                }(), function () {
                    function a(a, b, c, d, e, f) {
                        function g(a) {
                            return ((n * a + o) * a + p) * a;
                        }function h(a) {
                            return ((q * a + r) * a + s) * a;
                        }function i(a) {
                            return (3 * n * a + 2 * o) * a + p;
                        }function j(a) {
                            return 1 / (200 * a);
                        }function k(a, b) {
                            return h(m(a, b));
                        }function l(a) {
                            return a >= 0 ? a : 0 - a;
                        }function m(a, b) {
                            var c, d, e, f, h, j;for (e = a, j = 0; j < 8; j++) {
                                if (f = g(e) - a, l(f) < b) return e;if (h = i(e), l(h) < 1e-6) break;e -= f / h;
                            }if (c = 0, d = 1, e = a, e < c) return c;if (e > d) return d;for (; c < d;) {
                                if (f = g(e), l(f - a) < b) return e;a > f ? c = e : d = e, e = .5 * (d - c) + c;
                            }return e;
                        }var n = 0,
                            o = 0,
                            p = 0,
                            q = 0,
                            r = 0,
                            s = 0;return p = 3 * b, o = 3 * (d - b) - p, n = 1 - p - o, s = 3 * c, r = 3 * (e - c) - s, q = 1 - s - r, k(a, j(f));
                    }function b(b, c, d, e) {
                        return function (f) {
                            return a(f, b, c, d, e, 1);
                        };
                    }e.setBezierFunction = function (a, c, d, f, g) {
                        var h = b(c, d, f, g);return h.displayName = a, h.x1 = c, h.y1 = d, h.x2 = f, h.y2 = g, e.prototype.formula[a] = h;
                    }, e.unsetBezierFunction = function (a) {
                        delete e.prototype.formula[a];
                    };
                }(), function () {
                    function a(a, b, c, d, f, g) {
                        return e.tweenProps(d, b, a, c, 1, g, f);
                    }var b = new e();b._filterArgs = [], e.interpolate = function (c, d, f, g, h) {
                        var i = e.shallowCopy({}, c),
                            j = h || 0,
                            k = e.composeEasingObject(c, g || "linear");b.set({});var l = b._filterArgs;l.length = 0, l[0] = i, l[1] = c, l[2] = d, l[3] = k, e.applyFilter(b, "tweenCreated"), e.applyFilter(b, "beforeTween");var m = a(c, i, d, f, k, j);return e.applyFilter(b, "afterTween"), m;
                    };
                }(), function (a) {
                    function b(a, b) {
                        var c,
                            d = [],
                            e = a.length;for (c = 0; c < e; c++) {
                            d.push("_" + b + "_" + c);
                        }return d;
                    }function c(a) {
                        var b = a.match(v);return b ? (1 === b.length || a.charAt(0).match(u)) && b.unshift("") : b = ["", ""], b.join(A);
                    }function d(b) {
                        a.each(b, function (a) {
                            var c = b[a];"string" == typeof c && c.match(z) && (b[a] = e(c));
                        });
                    }function e(a) {
                        return i(z, a, f);
                    }function f(a) {
                        var b = g(a);return "rgb(" + b[0] + "," + b[1] + "," + b[2] + ")";
                    }function g(a) {
                        return a = a.replace(/#/, ""), 3 === a.length && (a = a.split(""), a = a[0] + a[0] + a[1] + a[1] + a[2] + a[2]), B[0] = h(a.substr(0, 2)), B[1] = h(a.substr(2, 2)), B[2] = h(a.substr(4, 2)), B;
                    }function h(a) {
                        return parseInt(a, 16);
                    }function i(a, b, c) {
                        var d = b.match(a),
                            e = b.replace(a, A);if (d) for (var f, g = d.length, h = 0; h < g; h++) {
                            f = d.shift(), e = e.replace(A, c(f));
                        }return e;
                    }function j(a) {
                        return i(x, a, k);
                    }function k(a) {
                        for (var b = a.match(w), c = b.length, d = a.match(y)[0], e = 0; e < c; e++) {
                            d += parseInt(b[e], 10) + ",";
                        }return d = d.slice(0, -1) + ")";
                    }function l(d) {
                        var e = {};return a.each(d, function (a) {
                            var f = d[a];if ("string" == typeof f) {
                                var g = r(f);e[a] = { formatString: c(f), chunkNames: b(g, a) };
                            }
                        }), e;
                    }function m(b, c) {
                        a.each(c, function (a) {
                            for (var d = b[a], e = r(d), f = e.length, g = 0; g < f; g++) {
                                b[c[a].chunkNames[g]] = +e[g];
                            }delete b[a];
                        });
                    }function n(b, c) {
                        a.each(c, function (a) {
                            var d = b[a],
                                e = o(b, c[a].chunkNames),
                                f = p(e, c[a].chunkNames);d = q(c[a].formatString, f), b[a] = j(d);
                        });
                    }function o(a, b) {
                        for (var c, d = {}, e = b.length, f = 0; f < e; f++) {
                            c = b[f], d[c] = a[c], delete a[c];
                        }return d;
                    }function p(a, b) {
                        C.length = 0;for (var c = b.length, d = 0; d < c; d++) {
                            C.push(a[b[d]]);
                        }return C;
                    }function q(a, b) {
                        for (var c = a, d = b.length, e = 0; e < d; e++) {
                            c = c.replace(A, +b[e].toFixed(4));
                        }return c;
                    }function r(a) {
                        return a.match(w);
                    }function s(b, c) {
                        a.each(c, function (a) {
                            var d,
                                e = c[a],
                                f = e.chunkNames,
                                g = f.length,
                                h = b[a];if ("string" == typeof h) {
                                var i = h.split(" "),
                                    j = i[i.length - 1];for (d = 0; d < g; d++) {
                                    b[f[d]] = i[d] || j;
                                }
                            } else for (d = 0; d < g; d++) {
                                b[f[d]] = h;
                            }delete b[a];
                        });
                    }function t(b, c) {
                        a.each(c, function (a) {
                            var d = c[a],
                                e = d.chunkNames,
                                f = e.length,
                                g = b[e[0]],
                                h = typeof g === "undefined" ? "undefined" : _typeof(g);if ("string" === h) {
                                for (var i = "", j = 0; j < f; j++) {
                                    i += " " + b[e[j]], delete b[e[j]];
                                }b[a] = i.substr(1);
                            } else b[a] = g;
                        });
                    }var u = /(\d|\-|\.)/,
                        v = /([^\-0-9\.]+)/g,
                        w = /[0-9.\-]+/g,
                        x = new RegExp("rgb\\(" + w.source + /,\s*/.source + w.source + /,\s*/.source + w.source + "\\)", "g"),
                        y = /^.*\(/,
                        z = /#([0-9]|[a-f]){3,6}/gi,
                        A = "VAL",
                        B = [],
                        C = [];a.prototype.filter.token = { tweenCreated: function tweenCreated(a, b, c, e) {
                            d(a), d(b), d(c), this._tokenData = l(a);
                        }, beforeTween: function beforeTween(a, b, c, d) {
                            s(d, this._tokenData), m(a, this._tokenData), m(b, this._tokenData), m(c, this._tokenData);
                        }, afterTween: function afterTween(a, b, c, d) {
                            n(a, this._tokenData), n(b, this._tokenData), n(c, this._tokenData), t(d, this._tokenData);
                        } };
                }(e);
            }).call(null);
        }, {}], 2: [function (a, b, c) {
            var d = a("./shape"),
                e = a("./utils"),
                f = function f(a, b) {
                this._pathTemplate = "M 50,50 m 0,-{radius} a {radius},{radius} 0 1 1 0,{2radius} a {radius},{radius} 0 1 1 0,-{2radius}", this.containerAspectRatio = 1, d.apply(this, arguments);
            };f.prototype = new d(), f.prototype.constructor = f, f.prototype._pathString = function (a) {
                var b = a.strokeWidth;a.trailWidth && a.trailWidth > a.strokeWidth && (b = a.trailWidth);var c = 50 - b / 2;return e.render(this._pathTemplate, { radius: c, "2radius": 2 * c });
            }, f.prototype._trailString = function (a) {
                return this._pathString(a);
            }, b.exports = f;
        }, { "./shape": 7, "./utils": 9 }], 3: [function (a, b, c) {
            var d = a("./shape"),
                e = a("./utils"),
                f = function f(a, b) {
                this._pathTemplate = "M 0,{center} L 100,{center}", d.apply(this, arguments);
            };f.prototype = new d(), f.prototype.constructor = f, f.prototype._initializeSvg = function (a, b) {
                a.setAttribute("viewBox", "0 0 100 " + b.strokeWidth), a.setAttribute("preserveAspectRatio", "none");
            }, f.prototype._pathString = function (a) {
                return e.render(this._pathTemplate, { center: a.strokeWidth / 2 });
            }, f.prototype._trailString = function (a) {
                return this._pathString(a);
            }, b.exports = f;
        }, { "./shape": 7, "./utils": 9 }], 4: [function (a, b, c) {
            b.exports = { Line: a("./line"), Circle: a("./circle"), SemiCircle: a("./semicircle"), Square: a("./square"), Path: a("./path"), Shape: a("./shape"), utils: a("./utils") };
        }, { "./circle": 2, "./line": 3, "./path": 5, "./semicircle": 6, "./shape": 7, "./square": 8, "./utils": 9 }], 5: [function (a, b, c) {
            var d = a("shifty"),
                e = a("./utils"),
                f = { easeIn: "easeInCubic", easeOut: "easeOutCubic", easeInOut: "easeInOutCubic" },
                g = function a(b, c) {
                if (!(this instanceof a)) throw new Error("Constructor was called without new keyword");c = e.extend({ duration: 800, easing: "linear", from: {}, to: {}, step: function step() {} }, c);var d;d = e.isString(b) ? document.querySelector(b) : b, this.path = d, this._opts = c, this._tweenable = null;var f = this.path.getTotalLength();this.path.style.strokeDasharray = f + " " + f, this.set(0);
            };g.prototype.value = function () {
                var a = this._getComputedDashOffset(),
                    b = this.path.getTotalLength(),
                    c = 1 - a / b;return parseFloat(c.toFixed(6), 10);
            }, g.prototype.set = function (a) {
                this.stop(), this.path.style.strokeDashoffset = this._progressToOffset(a);var b = this._opts.step;if (e.isFunction(b)) {
                    var c = this._easing(this._opts.easing),
                        d = this._calculateTo(a, c),
                        f = this._opts.shape || this;b(d, f, this._opts.attachment);
                }
            }, g.prototype.stop = function () {
                this._stopTween(), this.path.style.strokeDashoffset = this._getComputedDashOffset();
            }, g.prototype.animate = function (a, b, c) {
                b = b || {}, e.isFunction(b) && (c = b, b = {});var f = e.extend({}, b),
                    g = e.extend({}, this._opts);b = e.extend(g, b);var h = this._easing(b.easing),
                    i = this._resolveFromAndTo(a, h, f);this.stop(), this.path.getBoundingClientRect();var j = this._getComputedDashOffset(),
                    k = this._progressToOffset(a),
                    l = this;this._tweenable = new d(), this._tweenable.tween({ from: e.extend({ offset: j }, i.from), to: e.extend({ offset: k }, i.to), duration: b.duration, easing: h, step: function step(a) {
                        l.path.style.strokeDashoffset = a.offset;var c = b.shape || l;b.step(a, c, b.attachment);
                    }, finish: function finish(a) {
                        e.isFunction(c) && c();
                    } });
            }, g.prototype._getComputedDashOffset = function () {
                var a = window.getComputedStyle(this.path, null);return parseFloat(a.getPropertyValue("stroke-dashoffset"), 10);
            }, g.prototype._progressToOffset = function (a) {
                var b = this.path.getTotalLength();return b - a * b;
            }, g.prototype._resolveFromAndTo = function (a, b, c) {
                return c.from && c.to ? { from: c.from, to: c.to } : { from: this._calculateFrom(b), to: this._calculateTo(a, b) };
            }, g.prototype._calculateFrom = function (a) {
                return d.interpolate(this._opts.from, this._opts.to, this.value(), a);
            }, g.prototype._calculateTo = function (a, b) {
                return d.interpolate(this._opts.from, this._opts.to, a, b);
            }, g.prototype._stopTween = function () {
                null !== this._tweenable && (this._tweenable.stop(), this._tweenable = null);
            }, g.prototype._easing = function (a) {
                return f.hasOwnProperty(a) ? f[a] : a;
            }, b.exports = g;
        }, { "./utils": 9, shifty: 1 }], 6: [function (a, b, c) {
            var d = a("./shape"),
                e = a("./circle"),
                f = a("./utils"),
                g = function g(a, b) {
                this._pathTemplate = "M 50,50 m -{radius},0 a {radius},{radius} 0 1 1 {2radius},0", this.containerAspectRatio = 2, d.apply(this, arguments);
            };g.prototype = new d(), g.prototype.constructor = g, g.prototype._initializeSvg = function (a, b) {
                a.setAttribute("viewBox", "0 0 100 50");
            }, g.prototype._initializeTextContainer = function (a, b, c) {
                a.text.style && (c.style.top = "auto", c.style.bottom = "0", a.text.alignToBottom ? f.setStyle(c, "transform", "translate(-50%, 0)") : f.setStyle(c, "transform", "translate(-50%, 50%)"));
            }, g.prototype._pathString = e.prototype._pathString, g.prototype._trailString = e.prototype._trailString, b.exports = g;
        }, { "./circle": 2, "./shape": 7, "./utils": 9 }], 7: [function (a, b, c) {
            var d = a("./path"),
                e = a("./utils"),
                f = "Object is destroyed",
                g = function a(b, c) {
                if (!(this instanceof a)) throw new Error("Constructor was called without new keyword");if (0 !== arguments.length) {
                    this._opts = e.extend({ color: "#555", strokeWidth: 1, trailColor: null, trailWidth: null, fill: null, text: { style: { color: null, position: "absolute", left: "50%", top: "50%", padding: 0, margin: 0, transform: { prefix: !0, value: "translate(-50%, -50%)" } }, autoStyleContainer: !0, alignToBottom: !0, value: null, className: "progressbar-text" }, svgStyle: { display: "block", width: "100%" }, warnings: !1 }, c, !0), e.isObject(c) && void 0 !== c.svgStyle && (this._opts.svgStyle = c.svgStyle), e.isObject(c) && e.isObject(c.text) && void 0 !== c.text.style && (this._opts.text.style = c.text.style);var f,
                        g = this._createSvgView(this._opts);if (f = e.isString(b) ? document.querySelector(b) : b, !f) throw new Error("Container does not exist: " + b);this._container = f, this._container.appendChild(g.svg), this._opts.warnings && this._warnContainerAspectRatio(this._container), this._opts.svgStyle && e.setStyles(g.svg, this._opts.svgStyle), this.svg = g.svg, this.path = g.path, this.trail = g.trail, this.text = null;var h = e.extend({ attachment: void 0, shape: this }, this._opts);this._progressPath = new d(g.path, h), e.isObject(this._opts.text) && null !== this._opts.text.value && this.setText(this._opts.text.value);
                }
            };g.prototype.animate = function (a, b, c) {
                if (null === this._progressPath) throw new Error(f);this._progressPath.animate(a, b, c);
            }, g.prototype.stop = function () {
                if (null === this._progressPath) throw new Error(f);void 0 !== this._progressPath && this._progressPath.stop();
            }, g.prototype.destroy = function () {
                if (null === this._progressPath) throw new Error(f);this.stop(), this.svg.parentNode.removeChild(this.svg), this.svg = null, this.path = null, this.trail = null, this._progressPath = null, null !== this.text && (this.text.parentNode.removeChild(this.text), this.text = null);
            }, g.prototype.set = function (a) {
                if (null === this._progressPath) throw new Error(f);this._progressPath.set(a);
            }, g.prototype.value = function () {
                if (null === this._progressPath) throw new Error(f);return void 0 === this._progressPath ? 0 : this._progressPath.value();
            }, g.prototype.setText = function (a) {
                if (null === this._progressPath) throw new Error(f);null === this.text && (this.text = this._createTextContainer(this._opts, this._container), this._container.appendChild(this.text)), e.isObject(a) ? (e.removeChildren(this.text), this.text.appendChild(a)) : this.text.innerHTML = a;
            }, g.prototype._createSvgView = function (a) {
                var b = document.createElementNS("http://www.w3.org/2000/svg", "svg");this._initializeSvg(b, a);var c = null;(a.trailColor || a.trailWidth) && (c = this._createTrail(a), b.appendChild(c));var d = this._createPath(a);return b.appendChild(d), { svg: b, path: d, trail: c };
            }, g.prototype._initializeSvg = function (a, b) {
                a.setAttribute("viewBox", "0 0 100 100");
            }, g.prototype._createPath = function (a) {
                var b = this._pathString(a);return this._createPathElement(b, a);
            }, g.prototype._createTrail = function (a) {
                var b = this._trailString(a),
                    c = e.extend({}, a);return c.trailColor || (c.trailColor = "#eee"), c.trailWidth || (c.trailWidth = c.strokeWidth), c.color = c.trailColor, c.strokeWidth = c.trailWidth, c.fill = null, this._createPathElement(b, c);
            }, g.prototype._createPathElement = function (a, b) {
                var c = document.createElementNS("http://www.w3.org/2000/svg", "path");return c.setAttribute("d", a), c.setAttribute("stroke", b.color), c.setAttribute("stroke-width", b.strokeWidth), b.fill ? c.setAttribute("fill", b.fill) : c.setAttribute("fill-opacity", "0"), c;
            }, g.prototype._createTextContainer = function (a, b) {
                var c = document.createElement("div");c.className = a.text.className;var d = a.text.style;return d && (a.text.autoStyleContainer && (b.style.position = "relative"), e.setStyles(c, d), d.color || (c.style.color = a.color)), this._initializeTextContainer(a, b, c), c;
            }, g.prototype._initializeTextContainer = function (a, b, c) {}, g.prototype._pathString = function (a) {
                throw new Error("Override this function for each progress bar");
            }, g.prototype._trailString = function (a) {
                throw new Error("Override this function for each progress bar");
            }, g.prototype._warnContainerAspectRatio = function (a) {
                if (this.containerAspectRatio) {
                    var b = window.getComputedStyle(a, null),
                        c = parseFloat(b.getPropertyValue("width"), 10),
                        d = parseFloat(b.getPropertyValue("height"), 10);e.floatEquals(this.containerAspectRatio, c / d) || (console.warn("Incorrect aspect ratio of container", "#" + a.id, "detected:", b.getPropertyValue("width") + "(width)", "/", b.getPropertyValue("height") + "(height)", "=", c / d), console.warn("Aspect ratio of should be", this.containerAspectRatio));
                }
            }, b.exports = g;
        }, { "./path": 5, "./utils": 9 }], 8: [function (a, b, c) {
            var d = a("./shape"),
                e = a("./utils"),
                f = function f(a, b) {
                this._pathTemplate = "M 0,{halfOfStrokeWidth} L {width},{halfOfStrokeWidth} L {width},{width} L {halfOfStrokeWidth},{width} L {halfOfStrokeWidth},{strokeWidth}", this._trailTemplate = "M {startMargin},{halfOfStrokeWidth} L {width},{halfOfStrokeWidth} L {width},{width} L {halfOfStrokeWidth},{width} L {halfOfStrokeWidth},{halfOfStrokeWidth}", d.apply(this, arguments);
            };f.prototype = new d(), f.prototype.constructor = f, f.prototype._pathString = function (a) {
                var b = 100 - a.strokeWidth / 2;return e.render(this._pathTemplate, { width: b, strokeWidth: a.strokeWidth, halfOfStrokeWidth: a.strokeWidth / 2 });
            }, f.prototype._trailString = function (a) {
                var b = 100 - a.strokeWidth / 2;return e.render(this._trailTemplate, { width: b, strokeWidth: a.strokeWidth, halfOfStrokeWidth: a.strokeWidth / 2, startMargin: a.strokeWidth / 2 - a.trailWidth / 2 });
            }, b.exports = f;
        }, { "./shape": 7, "./utils": 9 }], 9: [function (a, b, c) {
            function d(a, b, c) {
                a = a || {}, b = b || {}, c = c || !1;for (var e in b) {
                    if (b.hasOwnProperty(e)) {
                        var f = a[e],
                            g = b[e];c && l(f) && l(g) ? a[e] = d(f, g, c) : a[e] = g;
                    }
                }return a;
            }function e(a, b) {
                var c = a;for (var d in b) {
                    if (b.hasOwnProperty(d)) {
                        var e = b[d],
                            f = "\\{" + d + "\\}",
                            g = new RegExp(f, "g");c = c.replace(g, e);
                    }
                }return c;
            }function f(a, b, c) {
                for (var d = a.style, e = 0; e < p.length; ++e) {
                    var f = p[e];d[f + h(b)] = c;
                }d[b] = c;
            }function g(a, b) {
                m(b, function (b, c) {
                    null !== b && void 0 !== b && (l(b) && b.prefix === !0 ? f(a, c, b.value) : a.style[c] = b);
                });
            }function h(a) {
                return a.charAt(0).toUpperCase() + a.slice(1);
            }function i(a) {
                return "string" == typeof a || a instanceof String;
            }function j(a) {
                return "function" == typeof a;
            }function k(a) {
                return "[object Array]" === Object.prototype.toString.call(a);
            }function l(a) {
                if (k(a)) return !1;var b = typeof a === "undefined" ? "undefined" : _typeof(a);return "object" === b && !!a;
            }function m(a, b) {
                for (var c in a) {
                    if (a.hasOwnProperty(c)) {
                        var d = a[c];b(d, c);
                    }
                }
            }function n(a, b) {
                return Math.abs(a - b) < q;
            }function o(a) {
                for (; a.firstChild;) {
                    a.removeChild(a.firstChild);
                }
            }var p = "Webkit Moz O ms".split(" "),
                q = .001;b.exports = { extend: d, render: e, setStyle: f, setStyles: g, capitalize: h, isString: i, isFunction: j, isObject: l, forEachObject: m, floatEquals: n, removeChildren: o };
        }, {}] }, {}, [4])(4);
});
//# sourceMappingURL=progressbar.min.js.map
/* jshint ignore:end */
// end file: _progressbar.js

// file: _slicknav.js
/*!
 * SlickNav Responsive Mobile Menu v1.0.10
 * (c) 2016 Josh Cope
 * licensed under MIT
 */
;(function ($, document, window) {
    var
    // default settings object.
    defaults = {
        label: 'MENU',
        duplicate: true,
        duration: 200,
        easingOpen: 'swing',
        easingClose: 'swing',
        closedSymbol: '&#9658;',
        openedSymbol: '&#9660;',
        prependTo: 'body',
        appendTo: '',
        parentTag: 'a',
        slideDirection: 'left',
        closeOnClick: false,
        allowParentLinks: false,
        nestedParentLinks: true,
        showChildren: false,
        removeIds: true,
        removeClasses: false,
        removeStyles: false,
        brand: '',
        animations: 'jquery',
        init: function init() {},
        beforeOpen: function beforeOpen() {},
        beforeClose: function beforeClose() {},
        afterOpen: function afterOpen() {},
        afterClose: function afterClose() {}
    },
        mobileMenu = 'slicknav',
        prefix = 'slicknav',
        Keyboard = {
        DOWN: 40,
        ENTER: 13,
        ESCAPE: 27,
        LEFT: 37,
        RIGHT: 39,
        SPACE: 32,
        TAB: 9,
        UP: 38
    };

    function Plugin(element, options) {
        this.element = element;

        // jQuery has an extend method which merges the contents of two or
        // more objects, storing the result in the first object. The first object
        // is generally empty as we don't want to alter the default options for
        // future instances of the plugin
        this.settings = $.extend({}, defaults, options);

        // Don't remove IDs by default if duplicate is false
        if (!this.settings.duplicate && !options.hasOwnProperty("removeIds")) {
            this.settings.removeIds = false;
        }

        this._defaults = defaults;
        this._name = mobileMenu;

        this.init();
    }

    Plugin.prototype.init = function () {
        var $this = this,
            menu = $(this.element),
            settings = this.settings,
            iconClass,
            menuBar;

        // clone menu if needed
        if (settings.duplicate) {
            $this.mobileNav = menu.clone();
        } else {
            $this.mobileNav = menu;
        }

        // remove IDs if set
        if (settings.removeIds) {
            $this.mobileNav.removeAttr('id');
            $this.mobileNav.find('*').each(function (i, e) {
                $(e).removeAttr('id');
            });
        }

        // remove classes if set
        if (settings.removeClasses) {
            $this.mobileNav.removeAttr('class');
            $this.mobileNav.find('*').each(function (i, e) {
                $(e).removeAttr('class');
            });
        }

        // remove styles if set
        if (settings.removeStyles) {
            $this.mobileNav.removeAttr('style');
            $this.mobileNav.find('*').each(function (i, e) {
                $(e).removeAttr('style');
            });
        }

        // styling class for the button
        iconClass = prefix + '_icon';

        if (settings.label === '') {
            iconClass += ' ' + prefix + '_no-text';
        }

        if (settings.parentTag == 'a') {
            settings.parentTag = 'a href="#"';
        }

        // create menu bar
        $this.mobileNav.attr('class', prefix + '_nav');
        menuBar = $('<div class="' + prefix + '_menu"></div>');
        if (settings.brand !== '') {
            var brand = $('<div class="' + prefix + '_brand">' + settings.brand + '</div>');
            $(menuBar).append(brand);
        }
        $this.btn = $(['<' + settings.parentTag + ' aria-haspopup="true" role="button" tabindex="0" class="' + prefix + '_btn ' + prefix + '_collapsed">', '<span class="' + prefix + '_menutxt">' + settings.label + '</span>', '<span class="' + iconClass + '">', '<span class="' + prefix + '_icon-bar"></span>', '<span class="' + prefix + '_icon-bar"></span>', '<span class="' + prefix + '_icon-bar"></span>', '</span>', '</' + settings.parentTag + '>'].join(''));
        $(menuBar).append($this.btn);
        if (settings.appendTo !== '') {
            $(settings.appendTo).append(menuBar);
        } else {
            $(settings.prependTo).prepend(menuBar);
        }
        menuBar.append($this.mobileNav);

        // iterate over structure adding additional structure
        var items = $this.mobileNav.find('li');
        $(items).each(function () {
            var item = $(this),
                data = {};
            data.children = item.children('ul').attr('role', 'menu');
            item.data('menu', data);

            // if a list item has a nested menu
            if (data.children.length > 0) {

                // select all text before the child menu
                // check for anchors

                var a = item.contents(),
                    containsAnchor = false,
                    nodes = [];

                $(a).each(function () {
                    if (!$(this).is('ul')) {
                        nodes.push(this);
                    } else {
                        return false;
                    }

                    if ($(this).is("a")) {
                        containsAnchor = true;
                    }
                });

                var wrapElement = $('<' + settings.parentTag + ' role="menuitem" aria-haspopup="true" tabindex="-1" class="' + prefix + '_item"/>');

                // wrap item text with tag and add classes unless we are separating parent links
                if (!settings.allowParentLinks || settings.nestedParentLinks || !containsAnchor) {
                    var $wrap = $(nodes).wrapAll(wrapElement).parent();
                    $wrap.addClass(prefix + '_row');
                } else $(nodes).wrapAll('<span class="' + prefix + '_parent-link ' + prefix + '_row"/>').parent();

                if (!settings.showChildren) {
                    item.addClass(prefix + '_collapsed');
                } else {
                    item.addClass(prefix + '_open');
                }

                item.addClass(prefix + '_parent');

                // create parent arrow. wrap with link if parent links and separating
                var arrowElement = $('<span class="' + prefix + '_arrow">' + (settings.showChildren ? settings.openedSymbol : settings.closedSymbol) + '</span>');

                if (settings.allowParentLinks && !settings.nestedParentLinks && containsAnchor) arrowElement = arrowElement.wrap(wrapElement).parent();

                //append arrow
                $(nodes).last().after(arrowElement);
            } else if (item.children().length === 0) {
                item.addClass(prefix + '_txtnode');
            }

            // accessibility for links
            item.children('a').attr('role', 'menuitem').click(function (event) {
                //Ensure that it's not a parent
                if (settings.closeOnClick && !$(event.target).parent().closest('li').hasClass(prefix + '_parent')) {
                    //Emulate menu close if set
                    $($this.btn).click();
                }
            });

            //also close on click if parent links are set
            if (settings.closeOnClick && settings.allowParentLinks) {
                item.children('a').children('a').click(function (event) {
                    //Emulate menu close
                    $($this.btn).click();
                });

                item.find('.' + prefix + '_parent-link a:not(.' + prefix + '_item)').click(function (event) {
                    //Emulate menu close
                    $($this.btn).click();
                });
            }
        });

        // structure is in place, now hide appropriate items
        $(items).each(function () {
            var data = $(this).data('menu');
            if (!settings.showChildren) {
                $this._visibilityToggle(data.children, null, false, null, true);
            }
        });

        // finally toggle entire menu
        if (settings.slideDirection === 'left') {
            settings.init();
            $this.mobileNav.stop(true, true).hide('slide', {
                direction: 'left'
            }, settings.duration);
        } else {
            $this._visibilityToggle($this.mobileNav, null, false, 'init', true);
        }

        // accessibility for menu button
        $this.mobileNav.attr('role', 'menu');

        // outline prevention when using mouse
        $(document).mousedown(function () {
            $this._outlines(false);
        });

        $(document).keyup(function () {
            $this._outlines(true);
        });

        // menu button click
        $($this.btn).click(function (e) {
            e.preventDefault();
            $this._menuToggle();
        });

        // click on menu parent
        $this.mobileNav.on('click', '.' + prefix + '_item', function (e) {
            e.preventDefault();
            $this._itemClick($(this));
        });

        // check for keyboard events on menu button and menu parents
        $($this.btn).keydown(function (e) {
            var ev = e || event;

            switch (ev.keyCode) {
                case Keyboard.ENTER:
                case Keyboard.SPACE:
                case Keyboard.DOWN:
                    e.preventDefault();
                    if (ev.keyCode !== Keyboard.DOWN || !$($this.btn).hasClass(prefix + '_open')) {
                        $this._menuToggle();
                    }

                    $($this.btn).next().find('[role="menuitem"]').first().focus();
                    break;
            }
        });

        $this.mobileNav.on('keydown', '.' + prefix + '_item', function (e) {
            var ev = e || event;

            switch (ev.keyCode) {
                case Keyboard.ENTER:
                    e.preventDefault();
                    $this._itemClick($(e.target));
                    break;
                case Keyboard.RIGHT:
                    e.preventDefault();
                    if ($(e.target).parent().hasClass(prefix + '_collapsed')) {
                        $this._itemClick($(e.target));
                    }
                    $(e.target).next().find('[role="menuitem"]').first().focus();
                    break;
            }
        });

        $this.mobileNav.on('keydown', '[role="menuitem"]', function (e) {
            var ev = e || event;

            switch (ev.keyCode) {
                case Keyboard.DOWN:
                    e.preventDefault();
                    var allItems = $(e.target).parent().parent().children().children('[role="menuitem"]:visible');
                    var idx = allItems.index(e.target);
                    var nextIdx = idx + 1;
                    if (allItems.length <= nextIdx) {
                        nextIdx = 0;
                    }
                    var next = allItems.eq(nextIdx);
                    next.focus();
                    break;
                case Keyboard.UP:
                    e.preventDefault();
                    var allItems = $(e.target).parent().parent().children().children('[role="menuitem"]:visible');
                    var idx = allItems.index(e.target);
                    var next = allItems.eq(idx - 1);
                    next.focus();
                    break;
                case Keyboard.LEFT:
                    e.preventDefault();
                    if ($(e.target).parent().parent().parent().hasClass(prefix + '_open')) {
                        var parent = $(e.target).parent().parent().prev();
                        parent.focus();
                        $this._itemClick(parent);
                    } else if ($(e.target).parent().parent().hasClass(prefix + '_nav')) {
                        $this._menuToggle();
                        $($this.btn).focus();
                    }
                    break;
                case Keyboard.ESCAPE:
                    e.preventDefault();
                    $this._menuToggle();
                    $($this.btn).focus();
                    break;
            }
        });

        // allow links clickable within parent tags if set
        if (settings.allowParentLinks && settings.nestedParentLinks) {
            $('.' + prefix + '_item a').click(function (e) {
                e.stopImmediatePropagation();
            });
        }
    };

    //toggle menu
    Plugin.prototype._menuToggle = function (el) {
        var $this = this;
        var btn = $this.btn;
        var mobileNav = $this.mobileNav;

        if (btn.hasClass(prefix + '_collapsed')) {
            btn.removeClass(prefix + '_collapsed');
            btn.addClass(prefix + '_open');
        } else {
            btn.removeClass(prefix + '_open');
            btn.addClass(prefix + '_collapsed');
        }
        btn.addClass(prefix + '_animating');
        $this._visibilityToggle(mobileNav, btn.parent(), true, btn);
    };

    // toggle clicked items
    Plugin.prototype._itemClick = function (el) {
        var $this = this;
        var settings = $this.settings;
        var data = el.data('menu');
        if (!data) {
            data = {};
            data.arrow = el.children('.' + prefix + '_arrow');
            data.ul = el.next('ul');
            data.parent = el.parent();
            //Separated parent link structure
            if (data.parent.hasClass(prefix + '_parent-link')) {
                data.parent = el.parent().parent();
                data.ul = el.parent().next('ul');
            }
            el.data('menu', data);
        }
        if (data.parent.hasClass(prefix + '_collapsed')) {
            data.arrow.html(settings.openedSymbol);
            data.parent.removeClass(prefix + '_collapsed');
            data.parent.addClass(prefix + '_open');
            data.parent.addClass(prefix + '_animating');
            $this._visibilityToggle(data.ul, data.parent, true, el);
        } else {
            data.arrow.html(settings.closedSymbol);
            data.parent.addClass(prefix + '_collapsed');
            data.parent.removeClass(prefix + '_open');
            data.parent.addClass(prefix + '_animating');
            $this._visibilityToggle(data.ul, data.parent, true, el);
        }
    };

    // toggle actual visibility and accessibility tags
    Plugin.prototype._visibilityToggle = function (el, parent, animate, trigger, init) {
        var $this = this;
        var settings = $this.settings;
        var items = $this._getActionItems(el);
        var duration = 0;
        if (animate) {
            duration = settings.duration;
        }

        function afterOpen(trigger, parent) {
            $(trigger).removeClass(prefix + '_animating');
            $(parent).removeClass(prefix + '_animating');

            //Fire afterOpen callback
            if (!init) {
                settings.afterOpen(trigger);
            }
        }

        function afterClose(trigger, parent) {
            el.attr('aria-hidden', 'true');
            items.attr('tabindex', '-1');
            $this._setVisAttr(el, true);
            el.hide(); //jQuery 1.7 bug fix

            $(trigger).removeClass(prefix + '_animating');
            $(parent).removeClass(prefix + '_animating');

            //Fire init or afterClose callback
            if (!init) {
                settings.afterClose(trigger);
            } else if (trigger == 'init') {
                settings.init();
            }
        }

        if (el.hasClass(prefix + '_hidden')) {
            el.removeClass(prefix + '_hidden');
            //Fire beforeOpen callback
            if (!init) {
                settings.beforeOpen(trigger);
            }
            if (settings.animations === 'jquery') {
                if (settings.slideDirection === 'left') {
                    if (!el.hasClass("slicknav_nav")) {
                        el.stop(true, true).slideDown(duration, settings.easingOpen, function () {
                            afterOpen(trigger, parent);
                        });
                    } else {
                        el.stop(true, true).show('slide', {
                            direction: 'left'
                        }, duration, function () {
                            afterOpen(trigger, parent);
                        });
                    }
                } else {
                    el.stop(true, true).slideDown(duration, settings.easingOpen, function () {
                        afterOpen(trigger, parent);
                    });
                }
            } else if (settings.animations === 'velocity') {
                el.velocity("finish").velocity("slideDown", {
                    duration: duration,
                    easing: settings.easingOpen,
                    complete: function complete() {
                        afterOpen(trigger, parent);
                    }
                });
            }
            el.attr('aria-hidden', 'false');
            items.attr('tabindex', '0');
            $this._setVisAttr(el, false);
        } else {
            el.addClass(prefix + '_hidden');

            //Fire init or beforeClose callback
            if (!init) {
                settings.beforeClose(trigger);
            }

            if (settings.animations === 'jquery') {
                if (settings.slideDirection === 'left') {
                    if (!el.hasClass("slicknav_nav")) {
                        el.stop(true, true).slideUp(duration, this.settings.easingClose, function () {
                            afterClose(trigger, parent);
                        });
                    } else {
                        el.stop(true, true).hide('slide', {
                            direction: 'left'
                        }, duration, function () {
                            afterClose(trigger, parent);
                        });
                    }
                } else {
                    el.stop(true, true).slideUp(duration, this.settings.easingClose, function () {
                        afterClose(trigger, parent);
                    });
                }
            } else if (settings.animations === 'velocity') {

                el.velocity("finish").velocity("slideUp", {
                    duration: duration,
                    easing: settings.easingClose,
                    complete: function complete() {
                        afterClose(trigger, parent);
                    }
                });
            }
        }
    };

    // set attributes of element and children based on visibility
    Plugin.prototype._setVisAttr = function (el, hidden) {
        var $this = this;

        // select all parents that aren't hidden
        var nonHidden = el.children('li').children('ul').not('.' + prefix + '_hidden');

        // iterate over all items setting appropriate tags
        if (!hidden) {
            nonHidden.each(function () {
                var ul = $(this);
                ul.attr('aria-hidden', 'false');
                var items = $this._getActionItems(ul);
                items.attr('tabindex', '0');
                $this._setVisAttr(ul, hidden);
            });
        } else {
            nonHidden.each(function () {
                var ul = $(this);
                ul.attr('aria-hidden', 'true');
                var items = $this._getActionItems(ul);
                items.attr('tabindex', '-1');
                $this._setVisAttr(ul, hidden);
            });
        }
    };

    // get all 1st level items that are clickable
    Plugin.prototype._getActionItems = function (el) {
        var data = el.data("menu");
        if (!data) {
            data = {};
            var items = el.children('li');
            var anchors = items.find('a');
            data.links = anchors.add(items.find('.' + prefix + '_item'));
            el.data('menu', data);
        }
        return data.links;
    };

    Plugin.prototype._outlines = function (state) {
        if (!state) {
            $('.' + prefix + '_item, .' + prefix + '_btn').css('outline', 'none');
        } else {
            $('.' + prefix + '_item, .' + prefix + '_btn').css('outline', '');
        }
    };

    Plugin.prototype.toggle = function () {
        var $this = this;
        $this._menuToggle();
    };

    Plugin.prototype.open = function () {
        var $this = this;
        if ($this.btn.hasClass(prefix + '_collapsed')) {
            $this._menuToggle();
        }
    };

    Plugin.prototype.close = function () {
        var $this = this;
        if ($this.btn.hasClass(prefix + '_open')) {
            $this._menuToggle();
        }
    };

    $.fn[mobileMenu] = function (options) {
        var args = arguments;

        // Is the first parameter an object (options), or was omitted, instantiate a new instance
        if (options === undefined || (typeof options === "undefined" ? "undefined" : _typeof(options)) === 'object') {
            return this.each(function () {

                // Only allow the plugin to be instantiated once due to methods
                if (!$.data(this, 'plugin_' + mobileMenu)) {

                    // if it has no instance, create a new one, pass options to our plugin constructor,
                    // and store the plugin instance in the elements jQuery data object.
                    $.data(this, 'plugin_' + mobileMenu, new Plugin(this, options));
                }
            });

            // If is a string and doesn't start with an underscore or 'init' function, treat this as a call to a public method.
        } else if (typeof options === 'string' && options[0] !== '_' && options !== 'init') {

            // Cache the method call to make it possible to return a value
            var returns;

            this.each(function () {
                var instance = $.data(this, 'plugin_' + mobileMenu);

                // Tests that there's already a plugin-instance and checks that the requested public method exists
                if (instance instanceof Plugin && typeof instance[options] === 'function') {

                    // Call the method of our plugin instance, and pass it the supplied arguments.
                    returns = instance[options].apply(instance, Array.prototype.slice.call(args, 1));
                }
            });

            // If the earlier cached method gives a value back return the value, otherwise return this to preserve chainability.
            return returns !== undefined ? returns : this;
        }
    };
})(jQuery, document, window);
// end file: _slicknav.js

// file: footer.js
function releaseTheMargin(footerWrapper, bodyWrapper) {

    var footerWrap = $(footerWrapper),
        bodyWrap = $(bodyWrapper),
        headerHeight = void 0;
    if ($('header.header-standard').length) {
        headerHeight = $('header.header-standard > div').height();
        bodyWrap.css({
            'margin-top': headerHeight,
            'margin-bottom': footerWrap.height() - 3
        });
    } else if ($('header.permanent-white').length) {
        headerHeight = $('header.permanent-white > div').height();
        bodyWrap.css({
            'margin-top': headerHeight,
            'margin-bottom': footerWrap.height() - 3
        });
    } else if ($('header.header-landing--logo-only').length) {
        headerHeight = $('header.header-landing--logo-only > div').height();
        bodyWrap.css({
            'margin-top': headerHeight,
            'margin-bottom': footerWrap.height() - 3
        });
    } else {
        bodyWrap.css('margin-bottom', footerWrap.height() - 3);
    }
    if ($('.header-container-wrapper').height() + $('.body-container-wrapper').height() <= $(window).height()) {
        console.log('uwaga krotka strona - regular footer init');
        bodyWrap.css('margin-bottom', 0);
        $('.footer-container-wrapper').css('position', 'relative');
    }
}

$(document).ready(function () {
    var browserWindow = $(window);
    browserWindow.resize(function () {
        return releaseTheMargin('body:not(.landing) .footer-container-wrapper', 'body:not(.landing) .body-container-wrapper');
    }).resize();

    var arrowUp = $("<div class=\"arrow-up-wrapper\">\n                        <i class=\"fa fa-chevron-up\" aria-hidden=\"true\"></i>\n                    </div>");
    $('body').append(arrowUp);
    browserWindow.scroll(function () {
        return checkIfArrowIsNeeded(arrowUp);
    }).scroll();
    arrowUp.on('click', directionMountain);
});

function checkIfArrowIsNeeded(arrowEl) {
    if (window.innerWidth > 768) {
        var windowHeight = window.innerHeight;
        arrowEl.offset().top > windowHeight ? arrowEl.addClass('visible') : arrowEl.removeClass('visible');
    }
}
function directionMountain() {
    $('body, html').animate({
        scrollTop: 0
    }, 500);
}
// end file: footer.js

// file: form.js
function hideEmptyLabel() {
    $('form label').each(function (i, e) {
        if ($(this).text() == "*") {
            $(this).css({ "display": "none" });
        }
    });
}

// Input type file

function customInputFile() {

    $('input[type=file]').parent().parent().find('label:first-child').addClass("file-name");
    $('input[type=file]').addClass("inputfile");
    $('input[type=file]').parent().parent().addClass("inputfile-fake");

    var inputs = $('.inputfile');
    Array.prototype.forEach.call(inputs, function (input) {

        var label = document.querySelectorAll('.file-name')[0];

        input.addEventListener('change', function (e) {
            var fileName = e.target.value.split('\\').pop();

            if (fileName) label.querySelector('.file-name span').innerHTML = fileName;else label.innerHTML = labelVal;
        });
    });
}
var firstload = true;
function fakePlaceholders() {
    // wyjatek dla type file
    // console.log('fakeplaceholders init');
    var inputName;
    // if (firstload == true) {
    // console.log('fakeplaceholders inside');
    $('input[type=file]').each(function () {

        inputName = $(this).parent().parent().find('label:first-child span').text();
        $(this).attr('placeholder', inputName);
        $(this).parent().parent().find('label:first-child span').text('');
    });

    var $inputs = $('input[type=text], input[type=tel], input[type=email], input[type=file], textarea');
    $inputs.each(function () {
        if ($(this).attr('placeholder') != undefined) {
            var el = $(this),
                placeholder = el.attr('placeholder'),
                id = el.attr('id');

            if ($(this).val().length > 0) {
                $(this).siblings(".fake-placeholder").addClass("has-value");
            } else {
                $(this).siblings(".fake-placeholder").removeClass("has-value");
            }

            el.attr('placeholder', '');
            el.parent().append('<label for="' + id + '" class="fake-placeholder">' + placeholder + '</label>');
        }
    });
    $inputs.on('change', function () {
        if ($(this).val().length > 0) {
            $(this).siblings(".fake-placeholder").addClass("has-value");
        } else {
            $(this).siblings(".fake-placeholder").removeClass("has-value");
        }
    });

    $inputs.map(function () {
        if ($(this).val().length > 0) {
            $(this).siblings(".fake-placeholder").addClass("has-value");
        } else {
            $(this).siblings(".fake-placeholder").removeClass("has-value");
        }
    });
    // }

    firstload = false;
}

// Autogrow textarea while typing
(function ($) {
    /**
     * Auto-growing textareas; technique ripped from Facebook
     * 
     * 
     * http://github.com/jaz303/jquery-grab-bag/tree/master/javascripts/jquery.autogrow-textarea.js
     */
    $.fn.autogrow = function (options) {
        return this.filter('textarea').each(function () {
            var self = this;
            var $self = $(self);
            var minHeight = $self.height();
            var noFlickerPad = $self.hasClass('autogrow-short') ? 0 : parseInt($self.css('lineHeight')) || 0;
            var settings = $.extend({
                preGrowCallback: null,
                postGrowCallback: null
            }, options);

            var shadow = $('<div></div>').css({
                position: 'absolute',
                top: -10000,
                left: -10000,
                width: $self.width(),
                fontSize: $self.css('fontSize'),
                fontFamily: $self.css('fontFamily'),
                fontWeight: $self.css('fontWeight'),
                lineHeight: $self.css('lineHeight'),
                resize: 'none',
                'word-wrap': 'break-word'
            }).appendTo(document.body);

            var update = function update(event) {
                var times = function times(string, number) {
                    for (var i = 0, r = ''; i < number; i++) {
                        r += string;
                    }return r;
                };

                var val = self.value.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/\n$/, '<br/>&#xa0;').replace(/\n/g, '<br/>').replace(/ {2,}/g, function (space) {
                    return times('&#xa0;', space.length - 1) + ' ';
                });

                // Did enter get pressed?  Resize in this keydown event so that the flicker doesn't occur.
                if (event && event.data && event.data.event === 'keydown' && event.keyCode === 13) {
                    val += '<br />';
                }

                shadow.css('width', $self.width());
                shadow.html(val + (noFlickerPad === 0 ? '...' : '')); // Append '...' to resize pre-emptively.

                var newHeight = Math.max(shadow.height() + noFlickerPad, minHeight);
                if (settings.preGrowCallback != null) {
                    newHeight = settings.preGrowCallback($self, shadow, newHeight, minHeight);
                }

                $self.height(newHeight);

                if (settings.postGrowCallback != null) {
                    settings.postGrowCallback($self);
                }
            };

            $self.change(update).keyup(update).keydown({ event: 'keydown' }, update);
            $(window).resize(update);

            update();
        });
    };
})(jQuery);

function autoGrow() {
    $('textarea').autogrow();
}

// Select
function select($wrapper, $form) {
    if ($form.hasClass('new-select-custom')) {
        return;
    }
    $form.addClass('new-select-custom');
    $form.find("select").each(function () {
        var parent = $(this).parent(),
            options = $(this).find('option'),
            placeholder = options.first().text() ? options.first().text() : '-Select-';

        parent.addClass('dropdown_select');
        parent.append('<div class="dropdown-header"><span>' + placeholder + '</span></div>');
        parent.append('<ul class="dropdown-list" style="display: none;"></ul>');
        options.not(":disabled").each(function () {
            // Zmienilem val() na text()
            if ($(this).text() != "") {
                parent.find("ul.dropdown-list").append('<li value="' + $(this).text() + '">' + $(this).text() + '</li>');
            }
        });
    });

    // $form.find('.dropdown_select.input .dropdown-header').click(function (event, element) {
    $form.find('.dropdown-header').click(function (event, element) {
        // console.log('dasd');
        if (!$(this).hasClass('slide-down')) {
            $(this).addClass('slide-down');
            $(this).siblings('.dropdown-list').slideDown();
        } else {
            $(this).removeClass('slide-down');
            $(this).siblings('.dropdown-list').slideUp();
        }
        $(this).children('.arrow-white').toggle();
    });

    $form.find('.dropdown-list li').click(function () {
        var choose = $(this).text();
        $(this).parent().siblings('.dropdown-header').find('span').text(choose);
        $(this).parent().siblings('select').find('option').removeAttr('selected');
        $(this).parent().siblings('select').val(choose).find(' option[value="' + choose + '"] ').attr('selected', 'selected').change();
        $(this).parent().find('li').removeClass('selected');
        $(this).addClass('selected');
        $(this).parent().siblings('.dropdown-header').toggleClass('slide-down').siblings('.dropdown-list').slideUp();
        $(this).parent().siblings('.dropdown-header').children('.arrow-white').toggle();
    });
    $form.find('.dropdown_select .input').click(function (event) {
        event.stopPropagation();
    });
}

// end file: form.js

// file: global.js
$(document).ready(function () {

    iOS();
    if (/MSIE 9/i.test(navigator.userAgent) || /rv:11.0/i.test(navigator.userAgent) || /MSIE 10/i.test(navigator.userAgent) || /Edge\/12./i.test(navigator.userAgent) || window.navigator.userAgent.indexOf("Edge") > -1) {
        // console.log('ms browser');
        $('body').addClass('ms-browser');
    }
});

function cloneCtaWrapper() {
    if ($('.header-main-content .cta-header').length) {
        var ctawrapperEl = $("<li class='hs-menu-item hs-menu-depth-1 cta-element'></li>");
        ctawrapperEl.appendTo('.slicknav_nav .hs-menu-wrapper > ul');
        $('.header-main-content .cta-header').clone(true).appendTo('.slicknav_nav .hs-menu-wrapper > ul > li:last-of-type');
    }
}

function waitForLoad(wrapper, element, callback) {
    if ($(wrapper).length > 0) {
        $(wrapper).each(function (i, el) {
            var waitForLoad = setInterval(function () {
                if ($(el).length == $(el).find(element).length) {
                    clearInterval(waitForLoad);
                    callback($(el), $(el).find(element));
                }
            }, 50);
        });
    }
}

waitForLoad(".widget-type-form, .form-wrapper, .widget-type-blog_comments, .hs_cos_wrapper_type_form", "form", customInputFile);
waitForLoad(".widget-type-form, .form-wrapper, .widget-type-blog_comments, .hs_cos_wrapper_type_form", "form", autoGrow);
waitForLoad(".widget-type-form, .form-wrapper, .widget-type-blog_comments, .hs_cos_wrapper_type_form", "form", fakePlaceholders);
waitForLoad(".widget-type-form, .form-wrapper, .widget-type-blog_comments, .hs_cos_wrapper_type_form", "form", hideEmptyLabel);
waitForLoad(".widget-type-form, .form-wrapper, .widget-type-blog_comments, .hs_cos_wrapper_type_form", "form", select);
waitForLoad(".header-main-content", ".cta-header", cloneCtaWrapper);

function iOS() {
    var iOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;
    if (iOS) $('body').addClass('ios');
}

// console.log(window.navigator.userAgent);
if (/MSIE 9/i.test(navigator.userAgent) || /rv:11.0/i.test(navigator.userAgent) || /MSIE 10/i.test(navigator.userAgent) || /Edge\/12./i.test(navigator.userAgent) || window.navigator.userAgent.indexOf("Edge") > -1) {
    // console.log('ms browser');
    $('body').addClass('ms-browser');
}
if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
    $('body').addClass('ff');
}
(function ($) {
    $.fn.cutWrappers = function (targetSelector) {
        $(this).each(function (i, container) {
            if ($(container).find(">" + targetSelector).length == 0) {
                $(container).find(targetSelector).appendTo($(container));
                $(container).find('.hs_cos_wrapper_type_custom_widget').remove();
                $(container).find('.hs_cos_wrapper_type_widget_container').remove();
                $(container).find('.widget-span').remove();
            }
        });
    };
})(jQuery);

//NAV-TABS Landing Offer


+function ($) {
    'use strict';

    // CSS TRANSITION SUPPORT (Shoutout: http://www.modernizr.com/)
    // ============================================================

    function transitionEnd() {
        var el = document.createElement('bootstrap');

        var transEndEventNames = {
            WebkitTransition: 'webkitTransitionEnd',
            MozTransition: 'transitionend',
            OTransition: 'oTransitionEnd otransitionend',
            transition: 'transitionend'
        };

        for (var name in transEndEventNames) {
            if (el.style[name] !== undefined) {
                return { end: transEndEventNames[name] };
            }
        }

        return false; // explicit for ie8 (  ._.)
    }

    // http://blog.alexmaccaw.com/css-transitions
    $.fn.emulateTransitionEnd = function (duration) {
        var called = false;
        var $el = this;
        $(this).one('bsTransitionEnd', function () {
            called = true;
        });
        var callback = function callback() {
            if (!called) $($el).trigger($.support.transition.end);
        };
        setTimeout(callback, duration);
        return this;
    };

    $(function () {
        $.support.transition = transitionEnd();

        if (!$.support.transition) return;

        $.event.special.bsTransitionEnd = {
            bindType: $.support.transition.end,
            delegateType: $.support.transition.end,
            handle: function handle(e) {
                if ($(e.target).is(this)) return e.handleObj.handler.apply(this, arguments);
            }
        };
    });
}(jQuery);

/* ========================================================================
 * Bootstrap: tab.js v3.3.7
 * http://getbootstrap.com/javascript/#tabs
 * ========================================================================
 * Copyright 2011-2016 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */

+function ($) {
    'use strict';

    // TAB CLASS DEFINITION
    // ====================

    var Tab = function Tab(element) {
        // jscs:disable requireDollarBeforejQueryAssignment
        this.element = $(element);
        // jscs:enable requireDollarBeforejQueryAssignment
    };

    Tab.VERSION = '3.3.7';

    Tab.TRANSITION_DURATION = 150;

    Tab.prototype.show = function () {
        var $this = this.element;
        var $ul = $this.closest('ul:not(.dropdown-menu)');
        var selector = $this.data('target');

        if (!selector) {
            selector = $this.attr('href');
            selector = selector && selector.replace(/.*(?=#[^\s]*$)/, ''); // strip for ie7
        }

        if ($this.parent('li').hasClass('active')) return;

        var $previous = $ul.find('.active:last a');
        var hideEvent = $.Event('hide.bs.tab', {
            relatedTarget: $this[0]
        });
        var showEvent = $.Event('show.bs.tab', {
            relatedTarget: $previous[0]
        });

        $previous.trigger(hideEvent);
        $this.trigger(showEvent);

        if (showEvent.isDefaultPrevented() || hideEvent.isDefaultPrevented()) return;

        var $target = $(selector);

        this.activate($this.closest('li'), $ul);
        this.activate($target, $target.parent(), function () {
            $previous.trigger({
                type: 'hidden.bs.tab',
                relatedTarget: $this[0]
            });
            $this.trigger({
                type: 'shown.bs.tab',
                relatedTarget: $previous[0]
            });
        });
    };

    Tab.prototype.activate = function (element, container, callback) {
        var $active = container.find('> .active');
        var transition = callback && $.support.transition && ($active.length && $active.hasClass('fade') || !!container.find('> .fade').length);

        function next() {
            $active.removeClass('active').find('> .dropdown-menu > .active').removeClass('active').end().find('[data-toggle="tab"]').attr('aria-expanded', false);

            element.addClass('active').find('[data-toggle="tab"]').attr('aria-expanded', true);

            if (transition) {
                element[0].offsetWidth; // reflow for transition
                element.addClass('in');
            } else {
                element.removeClass('fade');
            }

            if (element.parent('.dropdown-menu').length) {
                element.closest('li.dropdown').addClass('active').end().find('[data-toggle="tab"]').attr('aria-expanded', true);
            }

            callback && callback();
        }

        $active.length && transition ? $active.one('bsTransitionEnd', next).emulateTransitionEnd(Tab.TRANSITION_DURATION) : next();

        $active.removeClass('in');
    };

    // TAB PLUGIN DEFINITION
    // =====================

    function Plugin(option) {
        return this.each(function () {
            var $this = $(this);
            var data = $this.data('bs.tab');

            if (!data) $this.data('bs.tab', data = new Tab(this));
            if (typeof option == 'string') data[option]();
        });
    }

    var old = $.fn.tab;

    $.fn.tab = Plugin;
    $.fn.tab.Constructor = Tab;

    // TAB NO CONFLICT
    // ===============

    $.fn.tab.noConflict = function () {
        $.fn.tab = old;
        return this;
    };

    // TAB DATA-API
    // ============

    var clickHandler = function clickHandler(e) {
        e.preventDefault();
        Plugin.call($(this), 'show');
    };

    $(document).on('click.bs.tab.data-api', '[data-toggle="tab"]', clickHandler).on('click.bs.tab.data-api', '[data-toggle="pill"]', clickHandler);
}(jQuery);
(function ($) {
    if ($('.landing-offer').length == 1) {
        /* $('.tab-content .hs_cos_wrapper_type_widget_container div').each(function() {
             $(this).addClass('tab-pane');
             $(this).attr("id","newId");
          });*/
        $('.tab-pane').unwrap();
        $('.tab-pane').unwrap();
        $('.tab-pane:first').addClass('active');
        $('.tab-pane:first').addClass('in');
        var count = 0;
        $('.tab-content .tab-pane').each(function () {
            this.id = this.id + count;
            if (count == 0) {
                $(".nav-tabs").append("<li class='active'><a data-toggle='tab' href=#" + this.id + " aria-expanded='true'>" + $(this).attr("name") + "</a></li>");
            } else {
                $(".nav-tabs").append("<li><a data-toggle='tab' href=#" + this.id + " aria-expanded='true'>" + $(this).attr("name") + "</a></li>");
            }
            count++;
        });
    }
})(jQuery);

// end file: global.js

// file: header.js
// Opening modal header
function manageSearchHamburger() {
    $('header #search-field').bind('keypress', function (e) {
        // console.log(e);
        if (e.which == 13) {
            // console.log($(e.target).val().split(' '));
            // Search Query variables
            // const examplequery = 'https://www.google.com/search?q=site:http://marketing.clickray.eu+hello+from+the+other+side+54';
            var SQLocation = location.origin;
            var completeSQ = 'https://www.google.pl/search?q=site:';
            completeSQ += SQLocation;
            var SQArr = $(e.target).val().split(' ');
            SQArr.map(function (e, i) {
                completeSQ += "+" + e;
            });
            window.open(completeSQ);
        }
    });
}
function toggleHeader() {

    var hamburger = $('.hamburger'),
        hamburgerBar = hamburger.find('div'),
        modal = $('.menu-modal').parent().parent(),
        body = $('body'),
        html = $('html'),
        header = $('header > div');

    hamburger.on("click", function (event) {
        hamburger.toggleClass('hamburger-animation');
        hamburger.hasClass('hamburger-animation') ? hamburgerBar.css({ "background-color": "white" }) : hamburgerBar.css({ "background-color": "" });
        modal.toggleClass('open');
        modal.hasClass('open') ? null : modal.addClass('close');
        modal.one("webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend", function (event) {
            return modal.removeClass('close');
        });
        body.toggleClass('overflow-hidden');
        html.css('margin-right') == "0px" ? html.css({ "margin-right": getScrollBarWidth() + "px" }) : html.css({ "margin-right": "0" });
        header.css('padding-right') == "0px" ? header.css({ "padding-right": getScrollBarWidth() + "px" }) : header.css({ "padding-right": "0" });
    });
}

// Change header on scroll
function scrollHeader() {
    if (!$('header').hasClass('permanent-white')) {
        if ($(window).scrollTop > 10) {
            header.addClass('header-scroll');
        }
        $(window).on('scroll', function () {
            var scroll = $(window).scrollTop();
            var header = $('header');
            if (scroll > 10) {
                header.addClass('header-scroll');
            } else {
                header.removeClass('header-scroll');
            }
        });
        if ($('header.swap-logo .logo img').length != 0) {
            var scroll = void 0;
            var _header = $('header > div'),
                logo = $('.logo img'),
                logoSrc = logo.attr('src'),
                logoNewSrc = logoSrc.replace('-color', '-white'),
                hamburger = $('.hamburger');
            if ($('header.header-transparent.swap-logo').length) {
                logo.attr('src', logoNewSrc);
            }

            if ($('body:not(.same-logo)').length != 1) {
                logo.attr('src', logoNewSrc);
            }
            $(window).on('scroll', function () {
                scroll = $(window).scrollTop();
                if ($('body:not(.same-logo)').length == 1) {
                    if (scroll > 10) {
                        _header.addClass('header-white');
                        logo.attr('src', logoSrc);
                    } else {
                        _header.removeClass('header-white');
                        logo.attr('src', logoNewSrc);
                    }
                } else {

                    if (scroll > 10) {
                        _header.addClass('header-white');
                    } else {
                        _header.removeClass('header-white');
                    }
                }
            }).scroll();
        }
    }
}

// Open submenu
function toggleChildMenu() {
    var menuItem = $('.hs-item-has-children > a');

    menuItem.on('click', function () {
        var childrenMenu = $(this).siblings('.hs-menu-children-wrapper');

        $(this).parent().siblings('.hs-item-has-children').each(function () {
            $(this).children('.hs-menu-children-wrapper').slideUp();
        });

        childrenMenu.slideToggle();
    });
}

function manageHeaderSearch() {
    $('.search-opt #search-btn').on('click', function (e) {
        $(e.target).closest('.input-wrapper').find('.search-popup').fadeIn(500);
        $('body').css('overflow', 'hidden');
    });
    $('.search-opt .search-popup').on('click', function (e) {
        $(e.target).closest('.input-wrapper').find('.search-popup').fadeOut(500);
        e.stopPropagation();
        $('body').css('overflow', '');
    });
    $('.search-opt .popup-inner').on('click', function (e) {
        e.stopPropagation();
    });
}
// Scrollbar width
function getScrollBarWidth() {

    var $outer = $('<div>').css({ visibility: 'hidden', width: 100, overflow: 'scroll' }).appendTo('body'),
        widthWithScroll = $('<div>').css({ width: '100%' }).appendTo($outer).outerWidth();

    $outer.remove();
    return 100 - widthWithScroll;
};

$(document).ready(function () {
    toggleHeader();
    scrollHeader();
    toggleChildMenu();
    manageSearchHamburger();
    if ($('header.header-landing--logo-only').length > 0) {
        // var logoImage = $('header.header-landing--logo-only .hs-image-widget');
        // var imageParent = logoImage.parent();
        // imageParent.css('display', 'block');
        // console.log(logoImage.width());
        // $('header.header-landing--logo-only .hs-image-widget').parent().width($('header.header-landing--logo-only .hs-image-widget').width());
    }
    if ($('header .search-opt').length) {
        manageHeaderSearch();
    }
    if ($('header .hamburger-search').length == 0) {
        $('header nav .container > div > div:last-child .widget-type-menu').css('padding-bottom', 100);
    }
    if ($('.language-switcher').length) {
        var currentLangEl = $('#lang-current');
        var currentLang = $('header .lang_list_class li:first-of-type a').text().substr(0, 2);
        currentLangEl.text(currentLang);
    }
    if ($('.advanced-menu-header').length) {
        $(function () {
            $('.advanced-menu-header').slicknav({
                appendTo: '.header-main-content >div >div',
                label: '',
                duration: 500,
                openedSymbol: '+',
                closedSymbol: '+',
                slideDirection: 'left',
                init: function init() {
                    $('.slicknav_nav').addClass('preventDefault').addClass("slicknav_hidden");
                },
                beforeOpen: function beforeOpen(trigger) {
                    $('.slicknav_nav').removeClass('preventDefault');
                    if ($(trigger).hasClass('slicknav_btn')) {
                        $('.mobile-menu--mask').fadeIn(700);
                    }
                },
                afterClose: function afterClose(trigger) {
                    if ($(trigger).hasClass('slicknav_btn')) {
                        $('.mobile-menu--mask').fadeOut(700);
                    }
                }
            });
            var exitBtn = $('<div class="mobile-menu--exit"></div>'),
                mobileMenuMask = $('<div class="mobile-menu--mask"></div>');
            $('.slicknav_menu').append(mobileMenuMask);
            $('.slicknav_menu .hs-menu-wrapper > ul').append(exitBtn);
            $('.slicknav_menu').on('click', '.mobile-menu--exit, .mobile-menu--mask', function () {
                $('.advanced-menu-header').slicknav('close');
            });
        });
    }
});

// end file: header.js

// file: Blog/blog-301.js
$(document).ready(function () {
    if ($('.fixed-socials').length) {
        var spaceTop = $('.fixed-socials').offset().top - 150;
        $(window).scroll(function () {
            $(window).scrollTop() > spaceTop ? $('.fixed-socials').addClass('fixed') : $('.fixed-socials').removeClass('fixed');
        }).scroll();
    }
    $('.blog .follow-us .no-base, .blog .fixed-socials .no-base, .share-socials .socials-wrapper .no-base ').on('click', function (e, i) {
        var socialUrl = $(e.target).parent().data('href') || '#';
        // console.log(socialUrl);
        var requirement = socialUrl.startsWith('http') || socialUrl.startsWith('//');
        // console.log(requirement);
        requirement ? window.open(socialUrl) : window.location = socialUrl;
    });
});

function appendHeading(i, e) {
    if ($('.blog').length) {
        var textToAppend = "<p class=\"big\">Write a comment</p>";
        $(e).closest('#comment-form').prepend(textToAppend);
    }
}

waitForLoad(".widget-type-blog_comments", "#comment-form form", appendHeading);

// end file: Blog/blog-301.js

// file: Blog/blog-303-3.js
// Select
function blogflitersSelect($wrapper, $form) {
    if ($form.hasClass('new-select-custom')) {
        return;
    }
    $form.addClass('new-select-custom');
    $form.find("select").each(function () {
        var parent = $(this).parent(),
            options = $(this).find('option'),
            placeholder = options.first().text() ? options.first().text() : '-Select-';

        parent.addClass('dropdown_select');
        parent.append('<div class="dropdown-header"><span>' + placeholder + '</span></div>');
        parent.append('<ul class="dropdown-list" style="display: none;"></ul>');
        options.not(":disabled").each(function (index) {

            if ($(this).text() != "" && index > 0) {

                parent.find("ul.dropdown-list").append('<li value="' + $(this).text() + '">' + $(this).text() + '</li>');
            } else {
                parent.find("ul.dropdown-list").append('<li value="">' + $(this).text() + '</li>');
            }
        });
    });

    $form.find('.dropdown-header').click(function (event, element) {

        if (!$(this).hasClass('slide-down')) {
            $(this).addClass('slide-down');
            $(this).siblings('.dropdown-list').slideDown();
        } else {
            $(this).removeClass('slide-down');
            $(this).siblings('.dropdown-list').slideUp();
        }
        $(this).children('.arrow-white').toggle();
    });

    $form.find('.dropdown-list li').click(function () {
        var choose = $(this).attr("value");
        var text = $(this).text();

        $(this).parent().siblings('.dropdown-header').find('span').text(text);
        $(this).parent().siblings('select').find('option').removeAttr('selected');
        $(this).parent().siblings('select').val(choose).find(' option[value="' + choose + '"] ').attr('selected', 'selected').change();
        $(this).parent().find('li').removeClass('selected');
        $(this).addClass('selected');
        $(this).parent().siblings('.dropdown-header').toggleClass('slide-down').siblings('.dropdown-list').slideUp();
        $(this).parent().siblings('.dropdown-header').children('.arrow-white').toggle();
    });
    $form.find('.dropdown_select .input').click(function (event) {
        event.stopPropagation();
    });
}

waitForLoad(".blog-filtter", "form", blogflitersSelect);

// Share hero banner

$(".blog.blog-3 .social").click(function (e) {

    $(".item").removeClass("no");

    if (e.target != this) return;
    $('.blog.blog-3 .item').toggleClass("active");
});

// Comments

waitForLoad(".blog.blog-3 .widget-type-blog_comments", "form", function () {

    $('.comment-reply-to').text(replayText); // zmienna zdefiniowana w templacie
    $('.comment-form .big').text(formTitle); // zmienna zdefiniowana w templacie

    if ($('#comments-listing .comment').length != 0) {
        $('#comments-listing').parent().prepend('<p class="comments-tilte">' + commentsTitle + '</p>'); // zmienna zdefiniowana w templacie
    }
});

// end file: Blog/blog-303-3.js

// file: Blog/blog-case-study.js
$(document).ready(function () {
    if ($('.case-study-blog-single video').length) {
        var videos = Array.from(document.querySelectorAll('video'));
        var wrapper = $('<div class="video-blog-wrapper"></div>');
        $('video').wrap(wrapper);
        $('body.case-study-blog-single').on('click', wrapper, function (event) {
            if ($(event.target).hasClass('video-blog-wrapper')) {
                var clickedVideo = $(event.target).find('video');
                var clickedVideoJSDOM = clickedVideo.get(0);
                if (clickedVideoJSDOM.paused) {
                    clickedVideoJSDOM.play();
                    clickedVideo.parent().addClass('no-bg');
                    clickedVideo.attr('controls', true);
                } else {
                    clickedVideoJSDOM.pause();
                    clickedVideo.parent().removeClass('no-bg');
                    clickedVideo.attr('controls', false);
                }
            }
        });
    }
    $(".video-blog-wrapper").on("mousemove", function (e) {
        var cursorPos = e.offsetY;
        var height = $(this).height();
        if (height - cursorPos <= 50) {
            $(this).addClass("mouseover");
        } else {
            $(this).removeClass("mouseover");
        }
    });
});

// end file: Blog/blog-case-study.js

// file: Modules/FAQ-accordion.js
$(document).ready(function () {
    questionAccordion();
});

function questionAccordion() {
    if ($("html.hs-inline-edit").length == 0) {
        $("section.question").each(function (i, e) {
            $(e).find(".panel-group").append($(e).find(".faq-box"));
            $(e).find(".hs_cos_wrapper_type_widget_container,.hs_cos_wrapper_type_custom_widget").remove();
        });
    }
    $("section.question .panel-group").attr("id", "accordion");
    $("section.question .panel-group .panel").each(function (i, e) {
        var href = $(e).find("h4.panel-title > a").attr("href");
        $(e).find("h4.panel-title > a").attr("href", href + (i + 1));

        var id = $(e).find(".panel-collapse").attr("id");
        $(e).find(".panel-collapse").attr("id", id + (i + 1));
    });
}
// end file: Modules/FAQ-accordion.js

// file: Modules/brand-slider.js
$(document).ready(function () {
    initBrandsSlider();
});
function initBrandsSlider() {
    if ($(".brands-slider").length > 0) {
        $('.brands-slider').cutWrappers('.brands-slider__slide');
        $('.brands-slider').slick({
            autoplay: true,
            autoplaySpeed: 3000,
            slidesToShow: 4,
            slidesToScroll: 1,
            arrows: false,
            dots: false,
            responsive: [{
                breakpoint: 1150,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1
                }
            }, {
                breakpoint: 900,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            }, {
                breakpoint: 760,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }]
        });
    }
}
(function ($) {
    $.fn.cutWrappers = function (targetSelector) {
        $(this).find(targetSelector).appendTo($(this));
        $(this).find('.hs_cos_wrapper_type_custom_widget').remove();
        $(this).find('.hs_cos_wrapper_type_widget_container').remove();
        $(this).find('.widget-span').remove();
    };
})(jQuery);
// end file: Modules/brand-slider.js

// file: Modules/counters.js

$(window).scroll(function () {

    checkCounterPosition();
});

$(document).ready(function () {

    checkCounterPosition();
});

var checkcounter = 0;

function countTo() {

    $('.counter-box .count-to').each(function () {

        var $this = $(this);
        var countTo = $this.attr('data-count');

        $({ countNum: $this.text() }).animate({
            countNum: countTo
        }, {

            duration: 4000,
            easing: 'swing',
            step: function step() {
                $this.text(Math.floor(this.countNum));
            },
            complete: function complete() {
                $this.text(this.countNum);
            }

        });
    });
    checkcounter = 1;
}

function checkCounterPosition() {
    var counterposition;
    if ($(".counter-box .count-to").length != 0) {
        counterposition = $(".counter-box .count-to").position().top + 100;
    }
    if ($(window).scrollTop() + $(window).height() > counterposition && checkcounter == 0) {
        countTo();
    }
}
// end file: Modules/counters.js

// file: Modules/feature-box.js
function matchWidths() {
    var featureBox = $('.feature-box-redirect');
    featureBox.map(function (i, e) {
        if ($(e).hasClass('quarter')) {
            $(e).parent().addClass('quarter');
        } else if ($(e).hasClass('one-fifth')) {
            $(e).parent().addClass('one-fifth');
        } else if ($(e).hasClass('thirty')) {
            $(e).parent().addClass('thirty');
        } else if ($(e).hasClass('seventy')) {
            $(e).parent().addClass('seventy');
        } else if ($(e).hasClass('half')) {
            $(e).parent().addClass('half');
        } else if ($(e).hasClass('fourty')) {
            $(e).parent().addClass('fourty');
        }
    });
}
matchWidths();
// end file: Modules/feature-box.js

// file: Modules/form-and-map.js

waitForLoad('.hs_cos_wrapper', 'form', setSameHeight);

function setSameHeight() {

    var maxHeight = 0;
    var mapHeight = $('#map').innerHeight();

    $('.form-map-customize').find('.item').each(function () {
        var thisH = $(this).innerHeight();
        if (thisH > maxHeight) {
            maxHeight = thisH;
        }
    });

    if (mapHeight < maxHeight) {
        $('#map').css('height', maxHeight);
    }
}

// end file: Modules/form-and-map.js

// file: Modules/fullscreen-slider.js
$(document).ready(function () {

    // retina 
    if ($(window).innerWidth() > 2000) {
        var setHeight = $('.footer-container-wrapper').offset().top;
        $('.portfolio-blur-slider-wrapper ').css('min-height', setHeight - 145);
        $('.portfolio-blur-slider-wrapper > div').css('max-height', setHeight);
    }

    $('.portfolio-slider').append('<div class="portfolio-nav"></div>');
    $('.portfolio-slider > span > div').each(function (index) {
        $('.portfolio-nav').append('<span>' + (index + 1) + '</span>');
    });

    var t = '';

    $('.portfolio-nav span').each(function () {

        if ($(this).text().length < 2) {
            t = $(this).text();

            $(this).text('0' + t);
        }
    });

    $('.portfolio-slider > span').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: false,
        dots: false,
        focusOnSelect: true,
        arrows: true,
        asNavFor: '.portfolio-nav',
        speed: 600,
        fade: true,
        responsive: [{
            breakpoint: 485,
            settings: {
                arrows: false
            }
        }]
    });

    $('.portfolio-nav').slick({
        slidesToShow: 3,
        centerMode: true,
        arrows: false,
        dots: false,
        vertical: true,
        focusOnSelect: true,
        slidesToScroll: 1,
        asNavFor: '.portfolio-slider > span',
        responsive: [{
            breakpoint: 1201,
            settings: {
                vertical: false
            }
        }, {
            breakpoint: 410,
            settings: {
                vertical: false,
                slidesToShow: 3,
                centerMode: false
            }
        }]

    });

    $('.portfolio-blur-slider-wrapper').next().addClass('portfolio-slider-wrapper');

    bgURL = $('.portfolio-slider .slick-track .slick-active').find('.portfolio-slide .slide').css('background-image');

    $('.portfolio-blur-slider-wrapper div').css('background-image', bgURL);

    $('.portfolio-slider > span').on('beforeChange', function (event, slick, currentSlide, nextSlide) {
        setBackgroundImage(nextSlide);
    });
});

var bgURL = '';

function setBackgroundImage(nextSlide) {

    $('.portfolio-slider .slick-track div').each(function () {

        if ($(this).attr('data-slick-index') == nextSlide) {
            bgURL = $(this).find('.portfolio-slide .slide').css('background-image');
            $('.portfolio-blur-slider-wrapper div').css('background-image', bgURL);
        }
    });
}

// end file: Modules/fullscreen-slider.js

// file: Modules/hero-banner-image-fixed.js
if ($('body.landing-2').length == 1) {

    // VARIABELS FOR START SCROLL 
    var imageTopPosition = $('.hero-banner-text-image .right img').offset().top - 10;
    console.log(imageTopPosition);
    var imageLeftMargin = $('.hero-banner-text-image .right img').css('margin-left');
    // VARIABELS FOR END SCROLL 
    var offsetSection = $('.color-wrapper').offset().top + 80;
    var positionToSet = offsetSection - 160; // manipulate value if image will be jumping 

    // SET HEIGH FOR BANENR WHEN IMAGE IS MOVE
    var bannerH = $('.hero-banner-text-image').innerHeight();
    console.log(imageTopPosition);
}
// check width device

function checkDeviceWidth() {
    var widthDevice = $(window).innerWidth();

    if (widthDevice < 1024) {
        $('body.landing-2').addClass('aniamtion-off');
    } else {
        $('body.landing-2').removeClass('aniamtion-off');
    }
}

$(document).ready(function () {
    checkDeviceWidth();
});

$(window).resize(function () {
    checkDeviceWidth();
});

//  console.log(imageTopPosition);
$(window).scroll(function () {
    // console.log(imageTopPosition);
    if ($('body.landing-2.aniamtion-off').length == 0 && $('body.landing-2').length == 1) {
        /// START ON SCROLL ////
        var scroll = $(window).scrollTop();
        if (0 <= scroll) {
            // SET HEIGH FOR BANENR WHEN IMAGE IS MOVE
            $('.hero-banner-text-image').css('height', bannerH);
            // BE SCROLL 
            $('.hero-banner-text-image .right ').css({
                'position': 'fixed',
                'top': imageTopPosition + 'px',
                'right': '0px',
                'width': '50%'
            });
            $('.hero-banner-text-image .right img').css({
                'margin-left': imageLeftMargin,
                'margin-right': 'auto'
            });
        } else {

            $('.hero-banner-text-image .right ').css({
                'position': 'absolute',
                'width': '50%'
            });
            $('.hero-banner-text-image .right img').css({
                'margin-left': 'auto',
                'margin-right': '0'
            });
        }

        //// STOP ON BOTTOM ////

        var elementActualOffset = $('.hero-banner-text-image .right').offset().top - 0;

        if (offsetSection <= elementActualOffset) {
            $('.hero-banner-text-image .right ').css({
                'position': 'absolute',
                'top': positionToSet + 'px'
            });
        }
    }
});
// end file: Modules/hero-banner-image-fixed.js

// file: Modules/nav-tabs.js

// end file: Modules/nav-tabs.js

// file: Modules/parallax.js
$(function () {
    if ($('.parallax-container').length) {
        if (navigator.userAgent.match(/(iPod|iPhone|iPad|Android)/)) {
            $('#ios-notice').removeClass('hidden');
            $('.parallax-container').height($(window).height() * 0.5 | 0);
        } else {
            $(window).resize(function () {
                var parallaxHeight = Math.max($(window).height() * 0.7, 200) | 0;
                $('.parallax-container').height(parallaxHeight);
            }).trigger('resize');
        }
    }
});

/*!
 * parallax.js v1.4.2 (https://pixelcog.github.io/parallax.js/)
 * @copyright 2016 PixelCog, Inc.
 * @license MIT (https://github.com/pixelcog/parallax.js/blob/master/LICENSE)
 */
!function (t, i, e, s) {
    function o(i, e) {
        var h = this;"object" == (typeof e === "undefined" ? "undefined" : _typeof(e)) && (delete e.refresh, delete e.render, t.extend(this, e)), this.$element = t(i), !this.imageSrc && this.$element.is("img") && (this.imageSrc = this.$element.attr("src"));var r = (this.position + "").toLowerCase().match(/\S+/g) || [];if (r.length < 1 && r.push("center"), 1 == r.length && r.push(r[0]), ("top" == r[0] || "bottom" == r[0] || "left" == r[1] || "right" == r[1]) && (r = [r[1], r[0]]), this.positionX != s && (r[0] = this.positionX.toLowerCase()), this.positionY != s && (r[1] = this.positionY.toLowerCase()), h.positionX = r[0], h.positionY = r[1], "left" != this.positionX && "right" != this.positionX && (this.positionX = isNaN(parseInt(this.positionX)) ? "center" : parseInt(this.positionX)), "top" != this.positionY && "bottom" != this.positionY && (this.positionY = isNaN(parseInt(this.positionY)) ? "center" : parseInt(this.positionY)), this.position = this.positionX + (isNaN(this.positionX) ? "" : "px") + " " + this.positionY + (isNaN(this.positionY) ? "" : "px"), navigator.userAgent.match(/(iPod|iPhone|iPad)/)) return this.imageSrc && this.iosFix && !this.$element.is("img") && this.$element.css({ backgroundImage: "url(" + this.imageSrc + ")", backgroundSize: "cover", backgroundPosition: this.position }), this;if (navigator.userAgent.match(/(Android)/)) return this.imageSrc && this.androidFix && !this.$element.is("img") && this.$element.css({ backgroundImage: "url(" + this.imageSrc + ")", backgroundSize: "cover", backgroundPosition: this.position }), this;this.$mirror = t("<div />").prependTo("body");var a = this.$element.find(">.parallax-slider"),
            n = !1;0 == a.length ? this.$slider = t("<img />").prependTo(this.$mirror) : (this.$slider = a.prependTo(this.$mirror), n = !0), this.$mirror.addClass("parallax-mirror").css({ visibility: "hidden", zIndex: this.zIndex, position: "fixed", top: 0, left: 0, overflow: "hidden" }), this.$slider.addClass("parallax-slider").one("load", function () {
            h.naturalHeight && h.naturalWidth || (h.naturalHeight = this.naturalHeight || this.height || 1, h.naturalWidth = this.naturalWidth || this.width || 1), h.aspectRatio = h.naturalWidth / h.naturalHeight, o.isSetup || o.setup(), o.sliders.push(h), o.isFresh = !1, o.requestRender();
        }), n || (this.$slider[0].src = this.imageSrc), (this.naturalHeight && this.naturalWidth || this.$slider[0].complete || a.length > 0) && this.$slider.trigger("load");
    }function h(s) {
        return this.each(function () {
            var h = t(this),
                r = "object" == (typeof s === "undefined" ? "undefined" : _typeof(s)) && s;this == i || this == e || h.is("body") ? o.configure(r) : h.data("px.parallax") ? "object" == (typeof s === "undefined" ? "undefined" : _typeof(s)) && t.extend(h.data("px.parallax"), r) : (r = t.extend({}, h.data(), r), h.data("px.parallax", new o(this, r))), "string" == typeof s && ("destroy" == s ? o.destroy(this) : o[s]());
        });
    }!function () {
        for (var t = 0, e = ["ms", "moz", "webkit", "o"], s = 0; s < e.length && !i.requestAnimationFrame; ++s) {
            i.requestAnimationFrame = i[e[s] + "RequestAnimationFrame"], i.cancelAnimationFrame = i[e[s] + "CancelAnimationFrame"] || i[e[s] + "CancelRequestAnimationFrame"];
        }i.requestAnimationFrame || (i.requestAnimationFrame = function (e) {
            var s = new Date().getTime(),
                o = Math.max(0, 16 - (s - t)),
                h = i.setTimeout(function () {
                e(s + o);
            }, o);return t = s + o, h;
        }), i.cancelAnimationFrame || (i.cancelAnimationFrame = function (t) {
            clearTimeout(t);
        });
    }(), t.extend(o.prototype, { speed: .2, bleed: 0, zIndex: -100, iosFix: !0, androidFix: !0, position: "center", overScrollFix: !1, refresh: function refresh() {
            this.boxWidth = this.$element.outerWidth(), this.boxHeight = this.$element.outerHeight() + 2 * this.bleed, this.boxOffsetTop = this.$element.offset().top - this.bleed, this.boxOffsetLeft = this.$element.offset().left, this.boxOffsetBottom = this.boxOffsetTop + this.boxHeight;var t = o.winHeight,
                i = o.docHeight,
                e = Math.min(this.boxOffsetTop, i - t),
                s = Math.max(this.boxOffsetTop + this.boxHeight - t, 0),
                h = this.boxHeight + (e - s) * (1 - this.speed) | 0,
                r = (this.boxOffsetTop - e) * (1 - this.speed) | 0;if (h * this.aspectRatio >= this.boxWidth) {
                this.imageWidth = h * this.aspectRatio | 0, this.imageHeight = h, this.offsetBaseTop = r;var a = this.imageWidth - this.boxWidth;this.offsetLeft = "left" == this.positionX ? 0 : "right" == this.positionX ? -a : isNaN(this.positionX) ? -a / 2 | 0 : Math.max(this.positionX, -a);
            } else {
                this.imageWidth = this.boxWidth, this.imageHeight = this.boxWidth / this.aspectRatio | 0, this.offsetLeft = 0;var a = this.imageHeight - h;this.offsetBaseTop = "top" == this.positionY ? r : "bottom" == this.positionY ? r - a : isNaN(this.positionY) ? r - a / 2 | 0 : r + Math.max(this.positionY, -a);
            }
        }, render: function render() {
            var t = o.scrollTop,
                i = o.scrollLeft,
                e = this.overScrollFix ? o.overScroll : 0,
                s = t + o.winHeight;this.boxOffsetBottom > t && this.boxOffsetTop <= s ? (this.visibility = "visible", this.mirrorTop = this.boxOffsetTop - t, this.mirrorLeft = this.boxOffsetLeft - i, this.offsetTop = this.offsetBaseTop - this.mirrorTop * (1 - this.speed)) : this.visibility = "hidden", this.$mirror.css({ transform: "translate3d(0px, 0px, 0px)", visibility: this.visibility, top: this.mirrorTop - e, left: this.mirrorLeft, height: this.boxHeight, width: this.boxWidth }), this.$slider.css({ transform: "translate3d(0px, 0px, 0px)", position: "absolute", top: this.offsetTop, left: this.offsetLeft, height: this.imageHeight, width: this.imageWidth, maxWidth: "none" });
        } }), t.extend(o, { scrollTop: 0, scrollLeft: 0, winHeight: 0, winWidth: 0, docHeight: 1 << 30, docWidth: 1 << 30, sliders: [], isReady: !1, isFresh: !1, isBusy: !1, setup: function setup() {
            if (!this.isReady) {
                var s = t(e),
                    h = t(i),
                    r = function r() {
                    o.winHeight = h.height(), o.winWidth = h.width(), o.docHeight = s.height(), o.docWidth = s.width();
                },
                    a = function a() {
                    var t = h.scrollTop(),
                        i = o.docHeight - o.winHeight,
                        e = o.docWidth - o.winWidth;o.scrollTop = Math.max(0, Math.min(i, t)), o.scrollLeft = Math.max(0, Math.min(e, h.scrollLeft())), o.overScroll = Math.max(t - i, Math.min(t, 0));
                };h.on("resize.px.parallax load.px.parallax", function () {
                    r(), o.isFresh = !1, o.requestRender();
                }).on("scroll.px.parallax load.px.parallax", function () {
                    a(), o.requestRender();
                }), r(), a(), this.isReady = !0;
            }
        }, configure: function configure(i) {
            "object" == (typeof i === "undefined" ? "undefined" : _typeof(i)) && (delete i.refresh, delete i.render, t.extend(this.prototype, i));
        }, refresh: function refresh() {
            t.each(this.sliders, function () {
                this.refresh();
            }), this.isFresh = !0;
        }, render: function render() {
            this.isFresh || this.refresh(), t.each(this.sliders, function () {
                this.render();
            });
        }, requestRender: function requestRender() {
            var t = this;this.isBusy || (this.isBusy = !0, i.requestAnimationFrame(function () {
                t.render(), t.isBusy = !1;
            }));
        }, destroy: function destroy(e) {
            var s,
                h = t(e).data("px.parallax");for (h.$mirror.remove(), s = 0; s < this.sliders.length; s += 1) {
                this.sliders[s] == h && this.sliders.splice(s, 1);
            }t(e).data("px.parallax", !1), 0 === this.sliders.length && (t(i).off("scroll.px.parallax resize.px.parallax load.px.parallax"), this.isReady = !1, o.isSetup = !1);
        } });var r = t.fn.parallax;t.fn.parallax = h, t.fn.parallax.Constructor = o, t.fn.parallax.noConflict = function () {
        return t.fn.parallax = r, this;
    }, t(e).on("ready.px.parallax.data-api", function () {
        t('[data-parallax="scroll"]').parallax();
    });
}(jQuery, window, document);
// end file: Modules/parallax.js

// file: Modules/photo-3d-effect.js
$(window).load(function () {
    initTilt();
});

$(window).resize(function () {
    initTilt();
});

function initTilt() {

    var deviceWidth = $(window).innerWidth();
    var element = $('.tilt');

    if (deviceWidth < 1200) {

        element.tilt.destroy.call(element);
    } else {
        element.tilt({
            maxTilt: 15,
            perspective: 1000,
            scale: 1, //scale 1 = 100%
            glare: false, // enable glare
            disableAxis: null, // Can be X or Y.
            maxGlare: 1 // From 0 - 1.
        });
    }
}
// end file: Modules/photo-3d-effect.js

// file: Modules/portfolio-item-v1.js
$(document).ready(function () {

    $('.portfolio-item-v1-wrapper').cutWrappers('.portfolio-item-v1');

    setTimeout(function () {
        $('.portfolio-item-v1-wrapper').isotope();
    }, 10);
});

// end file: Modules/portfolio-item-v1.js

// file: Modules/questions-v2.js
$(document).ready(function () {

    if ($('.question-v2').length) {
        var img_src = $('img.inner-img').attr('src');
        $('img.inner-img').parent().css('background-image', 'url("' + img_src + '")');
    }
    accordionPanles();

    if ($('.accordion-wrapper').length) {
        setTimeout(function () {
            $(".accordion-wrapper span > div").first().find('.header').click();
        }, 10);
    }
});

function accordionPanles() {

    $('.accordion-panel .header').click(function () {

        var $this = $(this).parent();

        if ($this.hasClass('open')) {

            $this.removeClass('open');
            $this.children('.body').slideUp(350);
        } else {

            $this.parent().parent().find('.accordion-panel').removeClass('open');
            $this.parent().parent().find('.body').slideUp(350);
            $this.toggleClass('open');
            $this.children('.body').slideToggle(350);
        }
    });
}

// end file: Modules/questions-v2.js

// file: Modules/rounded-bar.js
$(document).ready(function () {
    if ($('.rounded-bar--single').length > 0) {
        (function () {
            var progressBars = $('.rounded-bar--single');

            var _loop = function _loop(i) {
                var currentEl = progressBars.eq(i).find('.rounded-bar__svg');
                var currentPercentage = makePercentageVal(progressBars.eq(i).find('.rounded-bar__percentage').text());
                var bar = new ProgressBar.Circle(currentEl[0], {
                    strokeWidth: 5,
                    easing: 'linear',
                    duration: 700,
                    color: '#ebebeb',
                    trailColor: '#ebebeb',
                    trailWidth: 5,
                    svgStyle: null
                });
                bar.path.style.strokeLinecap = 'round';
                progressBars.eq(i).data("bars_done", false);
                $(window).on('scroll', function () {
                    var windowHeight = $(window).height();
                    var windowScrollTop = $(window).scrollTop();
                    if (progressBars.eq(i).data("bars_done") == false) {
                        if (windowHeight + windowScrollTop > progressBars.eq(i).offset().top + 300) {
                            progressBars.eq(i).data("bars_done", true);
                            bar.animate(currentPercentage);
                        }
                    }
                });
                // console.log(currentPercentage);
            };

            for (var i = 0; i < progressBars.length; i++) {
                _loop(i);
            }
            $(window).scroll();
        })();
    }
});

function makePercentageVal(val) {
    val = val.slice(0, -1).trim();
    return val / 100;
}

if (/MSIE 9/i.test(navigator.userAgent) || /rv:11.0/i.test(navigator.userAgent) || /MSIE 10/i.test(navigator.userAgent) || /Edge\/12./i.test(navigator.userAgent) || window.navigator.userAgent.indexOf("Edge") > -1) {
    // console.log('ms browser');
    $('body').addClass('ms-browser');
}
// end file: Modules/rounded-bar.js

// file: Modules/single-person-box.js
$('.single-person-box .social a').on('click touchend', function (e) {

    var link = $(this).attr('href');
    window.location = link;
});

// end file: Modules/single-person-box.js

// file: Modules/skill-prgoress-bar.js

$(window).scroll(function () {

    checkSkillPosition();
});

$(document).ready(function () {

    checkSkillPosition();
});

var checkcounter = 0;

function skillProgress() {

    $('.skill-progress-bar').each(function () {

        var dot = $(this).find('.dot');
        var maxSkill = $(this).find('.dot').attr('data-skill');

        dot.animate({
            left: maxSkill + '%'
        }, 3000, "swing");

        $({ countNum: dot.find('.text').text() }).animate({
            countNum: maxSkill
        }, {
            duration: 3000,
            step: function step() {
                dot.find('.text').text(Math.floor(this.countNum) + '%');
            },
            complete: function complete() {
                dot.find('.text').text(this.countNum + '%');
            }
        });
    });
    checkcounter = 1;
}

function checkSkillPosition() {
    var counterposition;
    if ($(".skill-progress-bar").length != 0) {
        counterposition = $(".skill-progress-bar").position().top + 100;
    }
    if ($(window).scrollTop() + $(window).height() > counterposition && checkcounter == 0) {

        skillProgress();
    }
}
// end file: Modules/skill-prgoress-bar.js

// file: Modules/text-image-video-popup-opt.js
function managePopup() {
    var header = $('.header-container-wrapper');
    var arrowup = $('.arrow-up-wrapper');
    var body = $('body');
    var popup = void 0;
    $('.image-video-text-module .video-popup-btn').on('click', function (e) {
        popup = $(e.target).parent().closest('.image-video-text-module').find('.popup-outer');

        header.addClass('popup-fallback'), body.css('overflow', 'hidden'), arrowup.addClass('popup-fallback'), popup.fadeIn();
    });
    $('.image-video-text-module .popup-outer, .image-video-text-module .popup-outer .exit-popup').on('click', function (e) {
        popup.fadeOut();
        header.removeClass('popup-fallback'), body.css('overflow', ''), arrowup.removeClass('popup-fallback');
    });
    $('.image-video-text-module .popup-outer .popup-inner').on('click', function (event) {
        event.stopPropagation();
    });
};
$(document).ready(function () {
    if ($('.image-video-text-module .popup-outer').length) {
        managePopup();
    }
});
// end file: Modules/text-image-video-popup-opt.js

// file: Modules/timeline-items.js
$(document).ready(function () {

    if ($('body.about-us').length != 0) timelineProgress();
});

function timelineProgress() {

    var check = 0;
    $('.timeline-item-wrapper>span').append('<div class="progress-bar"></div>');

    var progressBar = $('.progress-bar');

    var timeLineOffsetTop = $('.timeline-item-wrapper>span').offset().top;
    var timelineheight = $('.timeline-item-wrapper>span').innerHeight();

    var timeLineEnd = timeLineOffsetTop + timelineheight;

    var timeAnimation = timelineheight * 4;

    var endText = $('.timeline-end-text span').html();

    $(window).scroll(function () {
        var scroll = $(window).scrollTop();

        if (scroll > timeLineOffsetTop - 800 && scroll < timeLineEnd) {
            // console.log(scroll - timeLineOffsetTop);
            // progressBar.css('height', (scroll - timeLineOffsetTop + 400) );
            progressBar.animate({
                height: timelineheight - 140 + "px"
            }, timeAnimation);

            $('.timeline-item-wrapper').addClass('animate');
        }

        if ($('.timeline-item-wrapper.animate').length != 0 && check == 0) {
            check = 1;
            setTimeout(function () {
                $('.timeline-item-wrapper>span').append('<div class="dot-end wow fadeInDown"></div>');
                $('.timeline-item-wrapper>span').append('<div class="text-end wow fadeInUp">' + endText + '</div>');
            }, timeAnimation);
        }
    });
}

// end file: Modules/timeline-items.js

// file: Modules/video.js
// file: Storylead/stl-product-slider.js
$(document).ready(function () {
    $('.stl-product-slider .hs_cos_wrapper_type_widget_container').slick({
        infinite: true,
        autoplay: true,
        dots: false,
        arrows: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        responsive: [{
            breakpoint: 991,
            settings: {
                arrows: false,
                slidesToShow: 2,
                slidesToScroll: 1
            }
        }, {
            breakpoint: 767,
            settings: {
                arrows: false,
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }]
    });
});
// end file: Storylead/stl-product-slider.js

// file: Storylead/stl-product-video-slider.js
$(document).ready(function () {
    $('.stl-product-video-slider .hs_cos_wrapper_type_widget_container').slick({
        infinite: true,
        autoplay: true,
        autoplaySpeed: 5000,
        dots: false,
        arrows: false,
        slidesToShow: 4,
        variableWidth: true,
        slidesToScroll: 1,
        responsive: [{
            breakpoint: 1200,
            settings: {
                slidesToShow: 1,
                centerMode: true
            }
        }]
    });

    videoAutoplayFix();
});

$(".stl-product-video-slider a.popup.video-url, .stl-product-video-slide a.popup.video-url").click(function () {
    var videoSrc = $(this).data('video');
    var $this = $(this);

    $.magnificPopup.open({
        items: {
            type: 'inline',
            src: '<video controls preload="auto" width="100%" height="100%" src="' + videoSrc + '"></video>'
        },
        callbacks: {
            open: function open() {
                $('html').css('margin-right', 0);
                $this.parent().parent().addClass("hover");
                $(this.content)[0].play();
            },
            close: function close() {
                $this.parent().parent().removeClass("hover");
                $(this.content)[0].load();
            }
        }
    });
});

$(".stl-product-video-slider a.popup.video-embed, .stl-product-video-slide a.popup.video-embed").click(function () {
    var videoSrc = $(this).data('video');
    var $this = $(this);

    $.magnificPopup.open({
        items: {
            type: 'inline',
            src: '<div class="iframe-container"><iframe src="' + videoSrc + '?autoplay=1" frameborder="0" allowfullscreen></iframe></div>'
        },
        callbacks: {
            open: function open() {
                $('html').css('margin-right', 0);
                $this.parent().parent().addClass("hover");
            },
            close: function close() {
                $this.parent().parent().removeClass("hover");
            }
        }
    });
});

$(".stl-product-video-slider a.popup.image, .stl-product-video-slide a.popup.image").click(function () {
    var imageSrc = $(this).data('image');
    var $this = $(this);

    $.magnificPopup.open({
        items: {
            type: 'inline',
            src: '<div><img width="100%" src="' + imageSrc + '"></div>'
        },
        callbacks: {
            open: function open() {
                $('html').css('margin-right', 0);
                $this.parent().parent().addClass("hover");
            },
            close: function close() {
                $this.parent().parent().removeClass("hover");
            }
        }
    });
});

function videoAutoplayFix() {
    $('.stl-product-video-slider video, .stl-product-video-slide video').each(function () {
        if ($(this).attr('autoplay')) {
            $(this)[0].load();
            $(this)[0].play();
        }
    });

    $('.stl-product-video-slider .vimeo, .stl-product-video-slide vimeo').each(function () {
        var video = $(this)[0];

        //Create a new Vimeo.Player object
        var player = new Vimeo.Player(video);

        //When the player is ready, set the volume to 0
        player.ready().then(function () {
            player.setVolume(0);
        });
    });
}
// end file: Storylead/stl-product-video-slider.js
// end file: Modules/video.js

// file: Website/about-us-2.js
$(document).ready(function () {

    $('.simple-slider-wrapper > span').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        dots: true,
        arrows: false
    });
});

// end file: Website/about-us-2.js

// file: Website/about-us-3.js
$(document).ready(function () {

    if ($('body.about-us-3').length != 0) timelineProgress3();
});

function timelineProgress3() {

    var check = 0;
    $('.timeline-item-wrapper-v2>span').append('<div class="progress-bar"></div>');

    var progressBar = $('.progress-bar');

    var timeLineOffsetTop = $('.timeline-item-wrapper-v2>span').offset().top;
    var timelineheight = $('.timeline-item-wrapper-v2>span').innerHeight();
    var singleItemHeight = $('.timeline-item-v2').innerHeight();

    var timeLineEnd = timeLineOffsetTop + timelineheight;

    var timeAnimation = timelineheight * 4;

    var endText = $('.timeline-end-text span').html();

    $(window).scroll(function () {
        var scroll = $(window).scrollTop();

        if (scroll > timeLineOffsetTop - 800 && scroll < timeLineEnd) {
            progressBar.animate({
                height: timelineheight - singleItemHeight + "px"
            }, timeAnimation);

            $('.timeline-item-wrapper-v2').addClass('animate');
        }

        //      if( $('.timeline-item-wrapper-v2.animate').length != 0 && check == 0){
        //      	check = 1;
        //      	 setTimeout(function(){
        // 		$('.timeline-item-wrapper-v2>span').append('<div class="dot-end wow fadeInDown"></div>');
        // 		$('.timeline-item-wrapper-v2>span').append('<div class="text-end wow fadeInUp">'+ endText +'</div>');
        // },timeAnimation)
        //      }
    });
}

// end file: Website/about-us-3.js

// file: Website/about-us.js
$(document).ready(function () {});

$('.testimonials-slider').append('<div class="testimonials-main-slider"></div> <div class="testimonials-nav-slider"></div>');

$('.testimonials-slider .testimonials-slide').each(function () {

    $(this).appendTo('.testimonials-main-slider');
});

$('.testimonials-slider .pagination-element').each(function () {

    $(this).appendTo('.testimonials-nav-slider');
});

$('.testimonials-main-slider').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    fade: true,
    arrows: false,
    adaptiveHeight: true,
    asNavFor: '.testimonials-nav-slider',
    focusOnSelect: true,
    initialSlide: findMiddleSlide('.testimonials-slide')
});
$('.testimonials-nav-slider').slick({
    slidesToShow: 5,
    centerMode: true,
    arrows: false,
    slidesToScroll: 1,
    asNavFor: '.testimonials-main-slider',
    focusOnSelect: true,
    initialSlide: findMiddleSlide('.testimonials-slide'),
    responsive: [{
        breakpoint: 1024,
        settings: {
            slidesToShow: 3,
            slidesToScroll: 1
        }
    }, {
        breakpoint: 680,
        settings: {
            slidesToShow: 1,
            slidesToScroll: 1
        }
    }]

});

function findMiddleSlide(slideClass) {
    var slidesNum, initialSlide;
    slidesNum = $(slideClass).length;
    if (slidesNum > 2) {
        if (slidesNum % 2 == 0) {
            initialSlide = Math.floor(slidesNum / 2) - 1;
        } else {
            initialSlide = Math.floor(slidesNum / 2);
        }
    } else {
        initialSlide = 0;
    }
    return initialSlide;
}

// Certification Slider 

$('.brands-slider-wrapper > span').slick({
    slidesToShow: 4,
    slidesToScroll: 4,
    focusOnSelect: true,
    autoplay: true,
    dots: false,
    responsive: [{
        breakpoint: 1024,
        settings: {
            slidesToShow: 3,
            slidesToScroll: 1
        }
    }, {
        breakpoint: 680,
        settings: {
            slidesToShow: 1,
            slidesToScroll: 1
        }
    }]

});

// end file: Website/about-us.js

// file: Website/case-study-website.js
var visibleElementscount = 9;
var numberOfElements = 0;
$(document).ready(function () {
    if ($('body').hasClass('case-study-website')) {
        filterGridCaseStudies();

        var initalLoad = true;
        if (initalLoad == true) {
            $('.post-item').map(function (i, e) {
                numberOfElements++;
                if (i >= visibleElementscount) {
                    $(e).addClass('invis');
                }
            });
            initalLoad = false;
        }

        $('.case-study-filter-single').eq(0).addClass('active');
        $('.case-study-filter-single.active').trigger('click');
        $('.load-more-button a').on('click', function (event) {
            event.preventDefault();
            loadMoreCaseStudies('.post-item', 3);
        });
    }
});

function filterGridCaseStudies() {
    var resourcesContainer = $('.case-study-blog.website .case-studies-listing-wrapper');
    $('.listing-tag-filters .case-study-filter-single').on('click', function () {
        console.log($(this).attr('data-filter'));
        $('.case-study-filter-single').removeClass('active');
        $(this).addClass('active');
        var filterValue = $(this).attr('data-filter');
        resourcesContainer.isotope({
            filter: filterValue,
            itemSelector: '.post-item',
            layoutMode: 'fitRows',
            masonry: {
                columnWidth: '25%'
            }

        });
    });
}
function loadMoreCaseStudies(element, increment) {
    $(element).map(function (i, e) {
        if (i >= visibleElementscount && i < visibleElementscount + increment) {
            console.log(i);
            $(e).removeClass('invis');
        }
    });
    visibleElementscount = visibleElementscount + increment;

    if (visibleElementscount >= numberOfElements) {
        $('.load-more-button').fadeOut();
    }
    $('.case-study-filter-single').removeClass('active');
    $('.case-study-filter-single').eq(0).addClass('active');
    $('.case-study-filter-single.active').trigger('click');
}
// end file: Website/case-study-website.js

// file: Website/contact.js
function setMapHeight() {
    var getHeight = $('.contact-3 .main-content').height();
    $('#map').css('height', getHeight);

    var deviceWidth = $(window).innerWidth();

    if ($('.contact-3').length && deviceWidth < 768) {
        $('#map').css('height', '500px');
    }
}

$(document).ready(function () {

    setMapHeight();
});

waitForLoad(".widget-type-form", "form", setMapHeight);
// end file: Website/contact.js

// file: Website/home-slide.js
if ($('.hero-banner.hero-slide').length > 0) {
    $('.hero-banner-slider').append('<div class="hero-banner-main-slider"></div> <div class="hero-banner-nav-slider"></div>');

    $('.hero-banner-slider .hero-banner.hero-slide').each(function () {
        $(this).appendTo('.hero-banner-main-slider');
    });

    $('.hero-banner-slider .hero-slide--element').each(function () {
        $(this).appendTo('.hero-banner-nav-slider');
    });

    $('.hero-banner-main-slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        fade: true,
        arrows: false,
        adaptiveHeight: true,
        asNavFor: '.hero-banner-nav-slider',
        focusOnSelect: true
    });

    $('.hero-banner-nav-slider').slick({
        slidesToShow: 3,
        centerMode: true,
        arrows: false,
        slidesToScroll: 1,
        asNavFor: '.hero-banner-main-slider',
        focusOnSelect: true,
        responsive: [{
            breakpoint: 680,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }]
    });
}

// end file: Website/home-slide.js

// file: Website/portfolio-1.js
// $(document).ready(function(){


// if ( $('body.portfolio-1').length == 1){

// // preapre sections
// $('.portfolio-item-filter-wrapper').cutWrappers('.portfolio-item-v1');
// // $('.category-wrapper').cutWrappers('.category-name-filter');


// // first load 
// $('.category-wrapper span > div:first-child .category-name-filter').addClass('active');


// // add label
// var menuLabel = $('.menu-label span').text();
// $('.category-wrapper').prepend('<span class="menu-title">'+menuLabel+'</span>');


// // start filter 

// filterGrid();

// //first Run

// setTimeout(function() {

//    $('.category-name-filter.active').click();
//  }, 100);


// }

// });

// if ( $('body.portfolio-1').length == 1 ){

//     // Variabels 

//     var current_page = 1;
//     var filterValue = "";
//     var max_page = 1;
//     var item_per_page = 9;
//     var filterValue = "";
//     var fillterContainer = $('.portfolio-item-filter-wrapper');


//      // set item per page

//     $(".portfolio-item-filter-wrapper  > section").each(function(i, item) {

//         $(item).attr("data-page", Math.ceil((i + 1) / item_per_page));

//      });


// }

//     function scrollTop() {
//         $("html, body").animate({
//             scrollTop: $(".category-wrapper").position().top - 60
//         }, 1000);
//      }


// function filterGrid() {

//     $('.category-wrapper .category-name-filter').on('click', function(e) {

//         current_page = 1;
//         var category_items_length = $(".portfolio-item-v1" + $(this).data('filter')).length;
//         max_page = Math.ceil(category_items_length / item_per_page);

//         var category_items = $(".portfolio-item-v1" + $(this).data('filter'));
//         category_items.removeAttr("data-page");
//         category_items.each(function(i, item) {
//             $(item).attr("data-page", Math.ceil((i + 1) / item_per_page));
//         });

//         generatePagination(current_page, max_page);

//         $('.category-wrapper .category-name-filter').removeClass('active');
//         $(this).addClass('active');

//         filterValue = $(this).attr('data-filter');


//         fillterContainer.isotope({
//             filter: function() {
//                 var name;

//                 if (filterValue != '*'){

//                     name = $(this).hasClass(filterValue.replace('.', '').trim());
//                 }else{
//                     name = true;
//                 } 
//                 if (name == true && parseInt($(this).attr("data-page")) == current_page) {
//                     return true;
//                 } 
//                 else {
//                     return false;
//                 }
//             },
//             itemSelector: '.portfolio-item-v1'
//         });
//     })
// }


// function changePage(page) {
//     current_page = page;
//     fillterContainer.isotope({
//         filter: function() {
//             var name;
//             if (filterValue != '*')
//                 name = $(this).hasClass(filterValue.replace('.', ''));
//             else name = true;
//             if (name == true && parseInt($(this).attr("data-page")) == current_page) {
//                 return true;
//             } else {
//                 return false;
//             }
//         },
//         itemSelector: '.portfolio-item-v1'
//     });

//     scrollTop();
//     generatePagination(current_page, max_page);
// }

// function generatePagination(currentPage, totalPages) {
//     $(".portfolio-pagination").remove();
//     var currentPage = currentPage;
//     var page = currentPage;
//     var total_pages = totalPages;
//     var more_pages = total_pages - currentPage;
//     var html = "";
//     html = $("<nav class='portfolio-pagination'><ul class='clearfix'></ul></nav>");
//     html.find("ul").append("<li><a><i class='fa fa-angle-left'></i></a></li>");
//     if (currentPage == 1) {
//         html.find("li a").attr("href", "javascript:changePage(" + page + ")");
//         html.find("li").addClass("disabled");
//     } else {
//         html.find("li a").attr("href", "javascript:changePage(" + (page - 1) + ")");
//     }

//     if (more_pages == 0) {
//         if (currentPage - 4 == 1)
//             html.find("ul li:last").after("<li><a href='javascript:changePage(" + (page - 4) + ")'>" + (page - 4) + "</a></li>");
//         if (currentPage - 4 > 1)
//             html.find("ul li:last").after("<li><a href='javascript:changePage(" + (page - 4) + ")'>" + (page - 4) + "</a></li>");

//     }
//     if (more_pages <= 1) {
//         if (currentPage - 3 == 1)
//             html.find("ul li:last").after("<li><a href='javascript:changePage(" + (page - 3) + ")'>" + (page - 3) + "</a></li>");
//         if (currentPage - 3 > 1)
//             html.find("ul li:last").after("<li><a href='javascript:changePage(" + (page - 3) + ")'>" + (page - 3) + "</a></li>");

//     }
//     if (currentPage - 2 == 1)
//         html.find("ul li:last").after("<li><a href='javascript:changePage(" + (page - 2) + ")'>" + (page - 2) + "</a></li>");
//     if (currentPage - 2 > 1)
//         html.find("ul li:last").after("<li><a href='javascript:changePage(" + (page - 2) + ")'>" + (page - 2) + "</a></li>");
//     if (currentPage - 1 == 1)
//         html.find("ul li:last").after("<li><a href='javascript:changePage(" + (page - 1) + ")'>" + (page - 1) + "</a></li>");
//     if (currentPage - 1 > 1)
//         html.find("ul li:last").after("<li><a href='javascript:changePage(" + (page - 1) + ")'>" + (page - 1) + "</a></li>");

//     html.find("ul li:last").after("<li><a href='javascript:changePage(" + (page) + ")'>" + (page) + "</a></li>");
//     html.find("ul li:last").addClass("active");

//     if (currentPage + 1 <= total_pages)
//         html.find("ul li:last").after("<li><a href='javascript:changePage(" + (page + 1) + ")'>" + (page + 1) + "</a></li>");
//     if (currentPage + 2 <= total_pages)
//         html.find("ul li:last").after("<li><a href='javascript:changePage(" + (page + 2) + ")'>" + (page + 2) + "</a></li>");

//     if (currentPage <= 2) {
//         if (currentPage + 3 <= total_pages)
//             html.find("ul li:last").after("<li><a href='javascript:changePage(" + (page + 3) + ")'>" + (page + 3) + "</a></li>");
//     }
//     if (currentPage <= 1) {
//         if (currentPage + 4 <= total_pages)
//             html.find("ul li:last").after("<li><a href='javascript:changePage(" + (page + 4) + ")'>" + (page + 4) + "</a></li>");
//     }

//     html.find("ul li:last").after("<li><a><i class='fa fa-angle-right'></i></a></li>");

//     if (currentPage == total_pages) {
//         html.find("li:last a").attr("href", "javascript:changePage(" + (page) + ")");
//         html.find("li:last").addClass("disabled");
//     } else {
//         html.find("li:last a").attr("href", "javascript:changePage(" + (page + 1) + ")");
//     }
//     if (total_pages > 1 && !$(".portfolio-item-filter-wrapper").next().is("nav"))
//         $(".portfolio-item-filter-wrapper").parent().after(html);
// }


// // no pagnation 

// // $(document).ready(function () {

// //     // preapre sections
// //     $('.portfolio-item-filter-wrapper').cutWrappers('.portfolio-item-v1');
// //     $('.category-wrapper').cutWrappers('.category-name-filter');

// //     // first load 
// //     $('.category-wrapper .category-name-filter:first-child').addClass('active');

// //     setTimeout(function () {
// //         $('.category-name-filter.active').click();
// //     }, 10);

// //     //

// //     // add label
// //     var menuLabel = $('.menu-label span').text();
// //     $('.category-wrapper').prepend('<span class="menu-title">' + menuLabel + '</span>');

// //     filterGrid();
// // });

// // var filterValue = "";
// // var fillterContainer = $('.portfolio-item-filter-wrapper');

// // function filterGrid() {

// //     $('.category-wrapper .category-name-filter').on('click', function (e) {

// //         $('.category-wrapper .category-name-filter').removeClass('active');
// //         $(this).addClass('active');

// //         filterValue = $(this).attr('data-filter');

// //         console.log(filterValue);

// //         fillterContainer.isotope({
// //             filter: filterValue,
// //             itemSelector: '.portfolio-item-v1'
// //         });
// //     });
// // }


// end file: Website/portfolio-1.js

// file: Website/portfolio-4.js
// $(document).ready(function(){


// // first load 
//     $('.category-wrapper span > div:first-child .category-name-filter').addClass('active');

//     // add label
//     var menuLabel = $('.menu-label span').text();
//     $('.category-wrapper').prepend('<span class="menu-title">'+menuLabel+'</span>');


//     // start filter 

//     filterGrid();

//     //first Run

//     setTimeout(function() {
//        $('.category-name-filter.active').click();
//      }, 10);


// });


//     // Variabels 

//     var current_page = 1;
//     var filterValue = "";
//     var max_page = 1;
//     var item_per_page = 9;
//     var filterValue = "";
//     var fillterContainer = $('.portfolio-item-filter-wrapper > span');


//      // set item per page

//     $(".portfolio-item-filter-wrapper  > section").each(function(i, item) {
//         $(item).attr("data-page", Math.ceil((i + 1) / item_per_page));
//      });


//     function scrollTop() {
//         $("html, body").animate({
//             scrollTop: $(".category-wrapper").position().top - 60
//         }, 1000);
//     }


//     function filterGrid() {

//         $('.category-wrapper .category-name-filter').on('click', function(e) {

//             current_page = 1;
//             var category_items_length = $(".portfolio-item-v2" + $(this).data('filter')).length;
//             max_page = Math.ceil(category_items_length / item_per_page);

//             var category_items = $(".portfolio-item-v2" + $(this).data('filter'));
//             category_items.removeAttr("data-page");
//             category_items.each(function(i, item) {
//                 $(item).attr("data-page", Math.ceil((i + 1) / item_per_page));
//             });

//             generatePagination(current_page, max_page);

//             $('.category-wrapper .category-name-filter').removeClass('active');
//             $(this).addClass('active');

//             filterValue = $(this).attr('data-filter');


//             fillterContainer.isotope({
//                 filter: function() {
//                     var name;
//                     if (filterValue != '*'){

//                         name = $(this).find('.portfolio-item-v2').hasClass(filterValue.replace('.', '').trim());
//                     }else{
//                         name = true;
//                     } 
//                     if (name == true && parseInt($(this).find('.portfolio-item-v2').attr("data-page")) == current_page) {
//                         return true;
//                     } 
//                     else {
//                         return false;
//                     }
//                 }
//             });
//         })
//     }


//     function changePage(page) {
//         current_page = page;

//         fillterContainer.isotope({
//             filter: function() {
//                 var name;
//                 if (filterValue != '*')
//                     name = $(this).find('.portfolio-item-v2').hasClass(filterValue.replace('.', ''));
//                 else name = true;
//                 if (name == true && parseInt($(this).find('.portfolio-item-v2').attr("data-page")) == current_page) {
//                     return true;
//                 } else {
//                     return false;
//                 }
//             }
//         });

//         scrollTop();
//         generatePagination(current_page, max_page);
//     }


//     function generatePagination(currentPage, totalPages) {
//         $(".portfolio-pagination").remove();
//         var currentPage = currentPage;
//         var page = currentPage;
//         var total_pages = totalPages;
//         var more_pages = total_pages - currentPage;
//         var html = "";
//         html = $("<nav class='portfolio-pagination'><ul class='clearfix'></ul></nav>");
//         html.find("ul").append("<li><a><i class='fa fa-angle-left'></i></a></li>");
//         if (currentPage == 1) {
//             html.find("li a").attr("href", "javascript:changePage(" + page + ")");
//             html.find("li").addClass("disabled");
//         } else {
//             html.find("li a").attr("href", "javascript:changePage(" + (page - 1) + ")");
//         }

//         if (more_pages == 0) {
//             if (currentPage - 4 == 1)
//                 html.find("ul li:last").after("<li><a href='javascript:changePage(" + (page - 4) + ")'>" + (page - 4) + "</a></li>");
//             if (currentPage - 4 > 1)
//                 html.find("ul li:last").after("<li><a href='javascript:changePage(" + (page - 4) + ")'>" + (page - 4) + "</a></li>");

//         }
//         if (more_pages <= 1) {
//             if (currentPage - 3 == 1)
//                 html.find("ul li:last").after("<li><a href='javascript:changePage(" + (page - 3) + ")'>" + (page - 3) + "</a></li>");
//             if (currentPage - 3 > 1)
//                 html.find("ul li:last").after("<li><a href='javascript:changePage(" + (page - 3) + ")'>" + (page - 3) + "</a></li>");

//         }
//         if (currentPage - 2 == 1)
//             html.find("ul li:last").after("<li><a href='javascript:changePage(" + (page - 2) + ")'>" + (page - 2) + "</a></li>");
//         if (currentPage - 2 > 1)
//             html.find("ul li:last").after("<li><a href='javascript:changePage(" + (page - 2) + ")'>" + (page - 2) + "</a></li>");
//         if (currentPage - 1 == 1)
//             html.find("ul li:last").after("<li><a href='javascript:changePage(" + (page - 1) + ")'>" + (page - 1) + "</a></li>");
//         if (currentPage - 1 > 1)
//             html.find("ul li:last").after("<li><a href='javascript:changePage(" + (page - 1) + ")'>" + (page - 1) + "</a></li>");

//         html.find("ul li:last").after("<li><a href='javascript:changePage(" + (page) + ")'>" + (page) + "</a></li>");
//         html.find("ul li:last").addClass("active");

//         if (currentPage + 1 <= total_pages)
//             html.find("ul li:last").after("<li><a href='javascript:changePage(" + (page + 1) + ")'>" + (page + 1) + "</a></li>");
//         if (currentPage + 2 <= total_pages)
//             html.find("ul li:last").after("<li><a href='javascript:changePage(" + (page + 2) + ")'>" + (page + 2) + "</a></li>");

//         if (currentPage <= 2) {
//             if (currentPage + 3 <= total_pages)
//                 html.find("ul li:last").after("<li><a href='javascript:changePage(" + (page + 3) + ")'>" + (page + 3) + "</a></li>");
//         }
//         if (currentPage <= 1) {
//             if (currentPage + 4 <= total_pages)
//                 html.find("ul li:last").after("<li><a href='javascript:changePage(" + (page + 4) + ")'>" + (page + 4) + "</a></li>");
//         }

//         html.find("ul li:last").after("<li><a><i class='fa fa-angle-right'></i></a></li>");

//         if (currentPage == total_pages) {
//             html.find("li:last a").attr("href", "javascript:changePage(" + (page) + ")");
//             html.find("li:last").addClass("disabled");
//         } else {
//             html.find("li:last a").attr("href", "javascript:changePage(" + (page + 1) + ")");
//         }
//         if (total_pages > 1 && !$(".portfolio-item-filter-wrapper").next().is("nav"))
//             $(".portfolio-item-filter-wrapper").parent().after(html);
//     }


// end file: Website/portfolio-4.js

// file: Website/resources-page.js
(function ($) {
    $.fn.cutWrappers = function (targetSelector) {
        $(this).find(targetSelector).appendTo($(this));
        $(this).find('.hs_cos_wrapper_type_custom_widget').remove();
        $(this).find('.hs_cos_wrapper_type_widget_container').remove();
        $(this).find('.widget-span').remove();
    };
})(jQuery);

// $('.resource__container').cutWrappers('.resource');
// $('.resource-categories__list').cutWrappers('.resource-categories__element');
var visibleElements = 9;
var elementsOnPage = 0;
$(document).ready(function () {
    if ($('body').hasClass('resources-page')) {
        filterGridResources();

        var initalLoad = true;
        if (initalLoad == true) {
            $('.resource.resource--vertical').map(function (i, e) {
                elementsOnPage++;
                if (i >= visibleElements) {
                    $(e).addClass('invis');
                }
            });
            initalLoad = false;
        }

        $('.resource-categories__element').eq(0).addClass('resource-categories__element--active');
        $('.resource-categories__element--active').trigger('click');
        $('.load-more-button a').on('click', function (event) {
            event.preventDefault();
            loadMoreElements('.resource.resource--vertical', 3);
        });
    }
});

function filterGridResources() {
    var resourcesContainer = $('.resources-page .resource__container');
    $('.resource-categories__element').on('click', function () {
        $('.resource-categories__element').removeClass('resource-categories__element--active');
        $(this).addClass('resource-categories__element--active');
        var filterValue = $(this).attr('data-filter');
        resourcesContainer.isotope({
            filter: filterValue,
            itemSelector: '.resource',
            layoutMode: 'fitRows',
            masonry: {
                columnWidth: '25%'
            }

        });
    });
}
function loadMoreElements(element, increment) {
    $(element).map(function (i, e) {
        if (i >= visibleElements && i < visibleElements + increment) {
            console.log(i);
            $(e).removeClass('invis');
        }
    });
    visibleElements = visibleElements + increment;

    if (visibleElements >= elementsOnPage) {
        $('.load-more-button').fadeOut();
    }
    $('.resource-categories__element').removeClass('resource-categories__element--active');
    $('.resource-categories__element').eq(0).addClass('resource-categories__element--active');
    $('.resource-categories__element--active').trigger('click');
}
// end file: Website/resources-page.js

// file: Website/services.js
// $(document).ready(() => {
//     if ($('.services .left .panel-group .accordion-panel').length) {
//         $('.services .left .panel-group >span >div:first-of-type .accordion-panel header').trigger('click');
//         console.log('click triggered');
//     }
// })
// end file: Website/services.js

// file: Website/single-person-circle.js
// IOS FIX 

$('.single-person-circle .social a').on('click touchend', function (e) {

    var link = $(this).attr('href');
    window.location = link;
});

// end file: Website/single-person-circle.js

// file: Website/team-all-page.js
// featured-item
$(document).ready(function () {

    $('.single-person-row.featured-item').each(function () {
        $(this).parent().addClass('featured-item-wrapper');
    });
});
// end file: Website/team-all-page.js

// file: Website/team-single-2.js
$(document).ready(function () {
    setImageHeight();
    avataImageSwap();
});

$(window).resize(function () {
    avataImageSwap();
    setImageHeight();
});

function avataImageSwap() {

    if ($('.avatar-wrapper').length) {
        var avatar_src = $('.avatar-wrapper img').attr('src');
        $('.avatar-wrapper').css('background-image', 'url("' + avatar_src + '")');
    }
}

function setImageHeight() {
    var deviceWidth = $(window).innerWidth();

    if ($('.avatar-wrapper').length && deviceWidth > 1024) {
        $('.top-section .avatar-wrapper').parent().parent().css('height', 'auto');
        var leftHeight = $('.top-section .half-container').parent().parent().height();
        $('.top-section .avatar-wrapper').parent().parent().css('height', leftHeight);
    } else {
        $('.top-section .avatar-wrapper').parent().parent().css('height', 'auto');
    }
}

// end file: Website/team-single-2.js
//# sourceMappingURL=template.js.map
