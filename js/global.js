if((navigator.userAgent.match(/iPhone/i)) || (navigator.userAgent.match(/iPod/i)) || (navigator.userAgent.match(/iPad/i))) {
    $(".load").bind('touchstart', function(){
        console.log("touch started");
    });
  };
function cloneCtaWrapper(){
    if ($('.header-main-content .cta-header').length) {
        const ctawrapperEl = $("<li class='hs-menu-item hs-menu-depth-1 cta-element'></li>");
        ctawrapperEl.appendTo('.slicknav_nav .hs-menu-wrapper > ul')
        $('.header-main-content .cta-header').clone(true).appendTo('.slicknav_nav .hs-menu-wrapper > ul > li:last-of-type');
    }
}

waitForLoad(".header-main-content", ".cta-header", cloneCtaWrapper);

(function ($) {
    $.fn.cutWrappers = function (targetSelector) {
        $(this).each(function(i,container){
            if($(container).find(">"+targetSelector).length==0){
                $(container).find(targetSelector).appendTo($(container));
                $(container).find('.hs_cos_wrapper_type_custom_widget').remove();
                $(container).find('.hs_cos_wrapper_type_widget_container').remove();
                $(container).find('.widget-span').remove();
            }
        });
    };



    // Select all links with hashes
$('.arrow a[href*="#"]')
  // Remove links that don't actually link to anything
  .not('[href="#"]')
  .not('[href="#0"]')
  .click(function(event) {
    // On-page links
    if (
      location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') 
      && 
      location.hostname == this.hostname
    ) {
      // Figure out element to scroll to
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      // Does a scroll target exist?
      if (target.length) {
        // Only prevent default if animation is actually gonna happen
        event.preventDefault();
        $('html, body').animate({
          scrollTop: target.offset().top
        }, 1000, function() {
          // Callback after animation
          // Must change focus!
          var $target = $(target);
          $target.focus();
          if ($target.is(":focus")) { // Checking if the target was focused
            return false;
          } else {
            $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
            $target.focus(); // Set focus again
          };
        });
      }
    }
  });
$('a.cta_button[href*=""]').on('click touchstart', function () {
    $( this ).click();
});
})(jQuery);
