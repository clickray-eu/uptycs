function releaseTheMargin(footerWrapper, bodyWrapper) {

    let footerWrap = $(footerWrapper),
        bodyWrap = $(bodyWrapper),
        headerHeight;
    if ($('header.header-standard').length) {
        headerHeight = $('header.header-standard > div').height();
        bodyWrap.css({
            'margin-top': headerHeight,
            'margin-bottom': footerWrap.height() - 3
        })
    } else if ($('header.permanent-white').length) {
        headerHeight = $('header.permanent-white > div').height();
        bodyWrap.css({
            'margin-top': headerHeight,
            'margin-bottom': footerWrap.height() - 3
        })
    } else if ($('header.header-landing--logo-only').length) {
        headerHeight = $('header.header-landing--logo-only > div').height();
        bodyWrap.css({
            'margin-top': headerHeight,
            'margin-bottom': footerWrap.height() - 3
        })
    } else {
        bodyWrap.css('margin-bottom', footerWrap.height() - 3);
    }
    if ($('.header-container-wrapper').height() + $('.body-container-wrapper').height() <= $(window).height()) {
        bodyWrap.css('margin-bottom', 0);
        $('.footer-container-wrapper').css('position', 'relative');
    }
}

$(document).ready(function () {
    const browserWindow = $(window);
    browserWindow.resize(() => releaseTheMargin('body:not(.landing) .footer-container-wrapper', 'body:not(.landing) .body-container-wrapper')).resize();

    const arrowUp = $(`<div class="arrow-up-wrapper">
                        <i class="fa fa-chevron-up" aria-hidden="true"></i>
                    </div>`);
    $('body').append(arrowUp);
    browserWindow.scroll(() => checkIfArrowIsNeeded(arrowUp)).scroll();
    arrowUp.on('click', directionMountain);
})

function checkIfArrowIsNeeded(arrowEl) {
    if (window.innerWidth > 768) {
        const windowHeight = window.innerHeight;
        arrowEl.offset().top > windowHeight ? arrowEl.addClass('visible') : arrowEl.removeClass('visible');
    }
}
function directionMountain() {
    $('body, html').animate({
        scrollTop: 0,
    }, 500);
}