$(document).ready(function() {
    manageSearchHamburger();
})

function manageSearchHamburger() {
    $('.me-header-search #search-field').bind('keypress', (e) => {
        if (e.which == 13) {
            const SQLocation = location.origin;
            let completeSQ = 'https://www.google.pl/search?q=site:';
            completeSQ += SQLocation;
            const SQArr = $(e.target).val().split(' ');
            SQArr.map((e, i) => {
                completeSQ += `+${e}`;
            })
            window.open(completeSQ);
        }
    });
}