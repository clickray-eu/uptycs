function managePopup() {
    const header = $('.header-container-wrapper');
    const arrowup = $('.arrow-up-wrapper');
    const body = $('body');
    let popup;
    $('.image-video-text-module .video-popup-btn').on('click', (e) => {
        popup = $(e.target).parent().closest('.image-video-text-module').find('.popup-outer');

        header.addClass('popup-fallback'),
            body.css('overflow', 'hidden'),
            arrowup.addClass('popup-fallback'),
            popup.fadeIn();
    })
    $('.image-video-text-module .popup-outer, .image-video-text-module .popup-outer .exit-popup').on('click', (e) => {
        popup.fadeOut();
        header.removeClass('popup-fallback'),
        body.css('overflow', ''),
        arrowup.removeClass('popup-fallback');
    })
    $('.image-video-text-module .popup-outer .popup-inner').on('click', (event) => {
        event.stopPropagation();
    })
};
$(document).ready(() => {
    if ($('.image-video-text-module .popup-outer').length) {
        managePopup();
    }
})