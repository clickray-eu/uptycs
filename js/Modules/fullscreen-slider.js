$(document).ready(function() {


// retina 
if( $(window).innerWidth() > 2000){
  var setHeight = $('.footer-container-wrapper').offset().top ;
  $('.portfolio-blur-slider-wrapper ').css('min-height', setHeight - 145);
  $('.portfolio-blur-slider-wrapper > div').css('max-height', setHeight );
}



    $('.portfolio-slider').append('<div class="portfolio-nav"></div>')
    $('.portfolio-slider > span > div').each(function( index ) {
      $('.portfolio-nav').append('<span>'+ (index + 1) +'</span>')
    });

    var t = '';

     $('.portfolio-nav span').each(function() {

        if ($(this).text().length < 2) {
            t = $(this).text();

            $(this).text('0' + t);
        }

    })



     $('.portfolio-slider > span').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: false,
        dots: false,
        focusOnSelect: true,
        arrows: true,
        asNavFor: '.portfolio-nav',
        speed: 600,
        fade: true,
        responsive: [
        {
          breakpoint: 485,
          settings: {
            arrows: false
          }
        }
       ]
    });

    $('.portfolio-nav').slick({
        slidesToShow: 3,
        centerMode: true,
        arrows: false,
        dots: false,
        vertical: true,
        focusOnSelect: true,
        slidesToScroll: 1,
        asNavFor: '.portfolio-slider > span',
        responsive: [
        {
          breakpoint: 1201,
          settings: {
            vertical: false
          }
        },
        {
          breakpoint: 410,
          settings: {
            vertical: false,
             slidesToShow: 3,
             centerMode: false
          }
        }
       ]
       
    });



    $('.portfolio-blur-slider-wrapper').next().addClass('portfolio-slider-wrapper');

    bgURL = $('.portfolio-slider .slick-track .slick-active').find('.portfolio-slide .slide').css('background-image');

    $('.portfolio-blur-slider-wrapper div').css('background-image', bgURL);



    $('.portfolio-slider > span').on('beforeChange', function(event, slick, currentSlide, nextSlide) {
        setBackgroundImage(nextSlide);
                   
    });




})




var bgURL = '';

function setBackgroundImage(nextSlide) {

    $('.portfolio-slider .slick-track div').each(function() {

        if ($(this).attr('data-slick-index') == nextSlide) {
            bgURL = $(this).find('.portfolio-slide .slide').css('background-image');
            $('.portfolio-blur-slider-wrapper div').css('background-image', bgURL);
        }
    })


}






