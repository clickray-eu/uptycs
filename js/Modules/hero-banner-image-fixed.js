if( $('body.landing-2').length == 1 ){

    // VARIABELS FOR START SCROLL 
    var imageTopPosition = $('.hero-banner-text-image .right img').offset().top - 10;
    var imageLeftMargin = $('.hero-banner-text-image .right img').css('margin-left');
    // VARIABELS FOR END SCROLL 
    var offsetSection = $('.color-wrapper').offset().top + 80;
    var positionToSet = offsetSection - 160; // manipulate value if image will be jumping 

    // SET HEIGH FOR BANENR WHEN IMAGE IS MOVE
    var bannerH = $('.hero-banner-text-image').innerHeight()
}
// check width device

function checkDeviceWidth(){
    var widthDevice = $(window).innerWidth();

    if(widthDevice < 1024){
       $('body.landing-2').addClass('aniamtion-off') 
    }else{
        $('body.landing-2').removeClass('aniamtion-off') 
    }
}


$(document).ready(function() {
    checkDeviceWidth();
})

$(window).resize(function() {
    checkDeviceWidth();
})

//  console.log(imageTopPosition);
$(window).scroll(function() {
    // console.log(imageTopPosition);
    if( $('body.landing-2.aniamtion-off').length == 0 && $('body.landing-2').length == 1){
        /// START ON SCROLL ////
        var scroll = $(window).scrollTop();
             if (0 <= scroll ){
            // SET HEIGH FOR BANENR WHEN IMAGE IS MOVE
            $('.hero-banner-text-image').css('height',bannerH );
            // BE SCROLL 
            $('.hero-banner-text-image .right ').css({
                'position': 'fixed',
                'top': imageTopPosition + 'px',
                'right': '0px',
                'width': '50%'
            })
            $('.hero-banner-text-image .right img').css({
                'margin-left': imageLeftMargin ,
                'margin-right': 'auto'
            })

        } else {
            
            $('.hero-banner-text-image .right ').css({
                'position': 'absolute',
                'width': '50%'
            })
            $('.hero-banner-text-image .right img').css({
                'margin-left': 'auto',
                'margin-right': '0'
            })
        }

        //// STOP ON BOTTOM ////
        
        var elementActualOffset = $('.hero-banner-text-image .right').offset().top - 0;

        if( offsetSection <= elementActualOffset )
        {
             $('.hero-banner-text-image .right ').css({
                 'position': 'absolute',
                 'top': positionToSet  + 'px'
             })
        }
    }
})