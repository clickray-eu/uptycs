
$(window).scroll(function() {
     
   checkSkillPosition();

});

$(document).ready(function() {
       
 checkSkillPosition();

});

var checkcounter = 0;

function skillProgress(){

  $('.skill-progress-bar').each(function(){


  	var dot = $(this).find('.dot');
  	var maxSkill = $(this).find('.dot').attr('data-skill')

  	dot.animate({
  	    left:  maxSkill+'%' 
    	}, 3000, "swing");

  	$({ countNum: dot.find('.text').text()}).animate({
          countNum: maxSkill
      },
  	{
  		duration: 3000,
          step: function() {
            dot.find('.text').text(Math.floor(this.countNum)+ '%');
          },
          complete: function() {
            dot.find('.text').text(this.countNum + '%');
          }
      });  

  })
  checkcounter = 1;

}



function checkSkillPosition() {
    var counterposition;
    if($(".skill-progress-bar").length != 0){
        counterposition = $(".skill-progress-bar").position().top + 100;

    }  
    if (($(window).scrollTop() + $(window).height()) > counterposition && checkcounter == 0) {
        
         skillProgress();
    }
}