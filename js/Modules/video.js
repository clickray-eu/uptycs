// file: Storylead/stl-product-slider.js
$(document).ready(function () {
    $('.stl-product-slider .hs_cos_wrapper_type_widget_container').slick({
        infinite: true,
        autoplay: true,
        dots: false,
        arrows: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        responsive: [{
            breakpoint: 991,
            settings: {
                arrows: false,
                slidesToShow: 2,
                slidesToScroll: 1
            }
        }, {
            breakpoint: 767,
            settings: {
                arrows: false,
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }]
    });
});
// end file: Storylead/stl-product-slider.js

// file: Storylead/stl-product-video-slider.js
$(document).ready(function () {
    $('.stl-product-video-slider .hs_cos_wrapper_type_widget_container').slick({
        infinite: true,
        autoplay: true,
        autoplaySpeed: 5000,
        dots: false,
        arrows: false,
        slidesToShow: 4,
        variableWidth: true,
        slidesToScroll: 1,
        responsive: [{
            breakpoint: 1200,
            settings: {
                slidesToShow: 1,
                centerMode: true
            }
        }]
    });

    videoAutoplayFix();
});

$(".stl-product-video-slider a.popup.video-url, .stl-product-video-slide a.popup.video-url").click(function () {
    var videoSrc = $(this).data('video');
    var $this = $(this);

    $.magnificPopup.open({
        items: {
            type: 'inline',
            src: '<video controls preload="auto" width="100%" height="100%" src="' + videoSrc + '"></video>'
        },
        callbacks: {
            open: function open() {
                $('html').css('margin-right', 0);
                $this.parent().parent().addClass("hover");
                $(this.content)[0].play();
            },
            close: function close() {
                $this.parent().parent().removeClass("hover");
                $(this.content)[0].load();
            }
        }
    });
});

$(".stl-product-video-slider a.popup.video-embed, .stl-product-video-slide a.popup.video-embed").click(function () {
    var videoSrc = $(this).data('video');
    var $this = $(this);

    $.magnificPopup.open({
        items: {
            type: 'inline',
            src: '<div class="iframe-container"><iframe src="' + videoSrc + '?autoplay=1" frameborder="0" allowfullscreen></iframe></div>'
        },
        callbacks: {
            open: function open() {
                $('html').css('margin-right', 0);
                $this.parent().parent().addClass("hover");
            },
            close: function close() {
                $this.parent().parent().removeClass("hover");
            }
        }
    });
});

$(".stl-product-video-slider a.popup.image, .stl-product-video-slide a.popup.image").click(function () {
    var imageSrc = $(this).data('image');
    var $this = $(this);

    $.magnificPopup.open({
        items: {
            type: 'inline',
            src: '<div><img width="100%" src="' + imageSrc + '"></div>'
        },
        callbacks: {
            open: function open() {
                $('html').css('margin-right', 0);
                $this.parent().parent().addClass("hover");
            },
            close: function close() {
                $this.parent().parent().removeClass("hover");
            }
        }
    });
});

function videoAutoplayFix() {
    $('video').each(function () {
        if ($(this).attr('autoplay')) {
            $(this)[0].load();
            $(this)[0].play();
        } else {
            $(this)[0].load();
        }
    });

    $('.stl-product-video-slider .vimeo, .stl-product-video-slide vimeo').each(function () {
        var video = $(this)[0];

        //Create a new Vimeo.Player object
        var player = new Vimeo.Player(video);

        //When the player is ready, set the volume to 0
        player.ready().then(function () {
            player.setVolume(0);
        });
    });
}
// end file: Storylead/stl-product-video-slider.js