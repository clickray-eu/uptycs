$(document).ready(function() {
	// Standard video - no popup
	$('.me-text-video-section .no-video-popup .video-container__video--standard video[autoplay]').each(function() {
		$(this).parents('.video-container').addClass('player-active');
		$(this).attr('controls', '');
	});
})

// Standard video - no popup
$('.me-text-video-section .no-video-popup .video-container__video--standard .video-play').on('click', function(e) {
	e.preventDefault();
	$(this).siblings('video')[0].play();
	$(this).siblings('video').attr('controls', '');
	$(this).parents('.video-container').addClass('player-active');
});

// Popup video
$('.me-text-video-section .video-popup .video-container__video .video-play').magnificPopup({
	type: 'iframe',
	mainClass: 'mfp-fade',
	removalDelay: 160,
	preloader: false,
	fixedContentPos: false
});