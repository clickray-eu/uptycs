$(document).ready(function () {

	if ($('.question-v2').length) {
		var img_src = $('img.inner-img').attr('src');
		$('img.inner-img').parent().css('background-image', 'url("' + img_src + '")')

	}
	accordionPanles();


	if ($('.accordion-wrapper').length) {
		setTimeout(function () {
			$(".accordion-wrapper span > div").first().find('.header').click();
		}, 10)
	}

})




function accordionPanles() {

	$('.accordion-panel .header').click(function () {

		var $this = $(this).parent();

		if ($this.hasClass('open')) {

			$this.removeClass('open');
			$this.children('.body').slideUp(350);


		} else {

			$this.parent().parent().find('.accordion-panel').removeClass('open');
			$this.parent().parent().find('.body').slideUp(350);
			$this.toggleClass('open');
			$this.children('.body').slideToggle(350);

		}
	});
}

