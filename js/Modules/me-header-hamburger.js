$(document).ready(() => {
    toggleHeader();
});

function toggleHeader() {

    const hamburger = $('.me-header-hamburger'),
        hamburgerBar = hamburger.find('div'),
        modal = $('.menu-modal').parent().parent(),
        body = $('body'),
        html = $('html'),
        header = $('header > div');

    hamburger.on("click", (event) => {
        hamburger.toggleClass('hamburger-animation');
        hamburger.hasClass('hamburger-animation') ? hamburgerBar.css({ "background-color": "white" }) : hamburgerBar.css({ "background-color": "" });
        modal.toggleClass('open');
        modal.hasClass('open') ? null : modal.addClass('close');
        modal.one("webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend", event => modal.removeClass('close'));
        body.toggleClass('overflow-hidden');
        html.css('margin-right') == "0px" ?
            html.css({ "margin-right": getScrollBarWidth() + "px" }) :
            html.css({ "margin-right": "0" });
        header.css('padding-right') == "0px" ?
            header.css({ "padding-right": getScrollBarWidth() + "px" }) :
            header.css({ "padding-right": "0" });
    });
}