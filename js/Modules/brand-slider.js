$(document).ready(function () {
      initBrandsSlider();
});
function initBrandsSlider(){
        if($(".brands-slider").length>0){
            $('.brands-slider').cutWrappers('.brands-slider__slide');
        $('.brands-slider').slick({
            autoplay: true,
            autoplaySpeed: 3000,
            slidesToShow:4,
            slidesToScroll: 1,
            arrows: false,
            dots: false,
              responsive: [
                {
                  breakpoint: 1150,
                  settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                  }
                }
,
                {
                  breakpoint: 900,
                  settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                  }
                }
,
                {
                  breakpoint: 760,
                  settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                  }
                }
            ]
        });
}
}
(function ($) {
    $.fn.cutWrappers = function (targetSelector) {
        $(this).find(targetSelector).appendTo($(this));
        $(this).find('.hs_cos_wrapper_type_custom_widget').remove();
        $(this).find('.hs_cos_wrapper_type_widget_container').remove();
        $(this).find('.widget-span').remove();
    };
})(jQuery);