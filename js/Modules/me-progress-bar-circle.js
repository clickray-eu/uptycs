$(document).ready(function () {
    if ($('.me-progress-bar-circle').length > 0) {
        const progressBars = $('.me-progress-bar-circle');
        for (let i = 0; i < progressBars.length; i++) {
            let currentEl = progressBars.eq(i).find('.me-progress-bar-circle__svg')
            let currentPercentage = makePercentageVal(progressBars.eq(i).find('.me-progress-bar-circle__percentage').text());
            let bar = new ProgressBar.Circle(currentEl[0], {
                strokeWidth: 5,
                easing: 'linear',
                duration: 700,
                color: '#ebebeb',
                trailColor: '#ebebeb',
                trailWidth: 5,
                svgStyle: null
            });
            bar.path.style.strokeLinecap = 'round';
            progressBars.eq(i).data("bars_done", false);
            $(window).on('scroll', function () {
                let windowHeight = $(window).height();
                let windowScrollTop = $(window).scrollTop();
                if (progressBars.eq(i).data("bars_done") == false) {
                    if (windowHeight + windowScrollTop > progressBars.eq(i).offset().top + 300) {
                        progressBars.eq(i).data("bars_done", true);
                        bar.animate(currentPercentage);
                    }
                }
            });
        }
        $(window).scroll();
    }
});

function makePercentageVal(val) {
    val = val.slice(0, -1).trim();
    return val / 100;
}

if(/MSIE 9/i.test(navigator.userAgent) || /rv:11.0/i.test(navigator.userAgent) || /MSIE 10/i.test(navigator.userAgent) ||/Edge\/12./i.test(navigator.userAgent) || window.navigator.userAgent.indexOf("Edge") > -1 ){
    $('body').addClass('ms-browser');
}