if ($('.me-testimonials-slider').length > 0) {
  $('.me-testimonials-slider').append('<div class="me-testimonials-main-slider"></div><div class="me-testimonials-nav-slider"></div>')

  $('.me-testimonials-slider .me-testimonials-slide').each(function(){
  	$(this).appendTo('.me-testimonials-main-slider');
  });


  $('.me-testimonials-slider .pagination-element').each(function(){
  	$(this).appendTo('.me-testimonials-nav-slider');
  })

  $('.me-testimonials-main-slider').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    fade: true,
    arrows: false,
    adaptiveHeight: true,
    asNavFor: '.me-testimonials-nav-slider',
    focusOnSelect: true,
    initialSlide: findMiddleSlide('.me-testimonials-slide') 
  });

  $('.me-testimonials-nav-slider').slick({
      slidesToShow: 5,
      centerMode: true,
      arrows: false,
      slidesToScroll: 1,
      asNavFor: '.me-testimonials-main-slider',
      focusOnSelect: true,
      initialSlide: findMiddleSlide('.me-testimonials-slide'),
      responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 680,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
     ]
  });

  slickLoopTransformFix('me-testimonials-nav-slider', 'fake-current');
}

function findMiddleSlide(slideClass) {
    var slidesNum, initialSlide;
    slidesNum = $(slideClass).length;
    if (slidesNum > 2) {
        if (slidesNum % 2 == 0) {
            initialSlide = Math.floor(slidesNum / 2) - 1;
        } else {
            initialSlide = Math.floor(slidesNum / 2) ;
        }
    } else {
        initialSlide = 0;
    }
    return initialSlide;
}

function slickLoopTransformFix(sliderClass, fakeCurrentSlideClass) {
  var slider = $('.' + sliderClass);
  var slidesCount = slider.find('.slick-slide:not(.slick-cloned)').length;
  slider.on('beforeChange', function(event, slick, currentSlide, nextSlide){
    // when on last slide and next is first
    slider.find('.slick-slide').removeClass(fakeCurrentSlideClass);
    if (currentSlide === (slidesCount - 1) && nextSlide === 0) {
      slider.find('.slick-slide[data-slick-index=' + String(slidesCount) + ']').addClass(fakeCurrentSlideClass);
    }
    // when on first slide and next is last
    if (currentSlide === 0 && nextSlide === slidesCount - 1) {
      slider.find('.slick-slide[data-slick-index=-1]').addClass(fakeCurrentSlideClass);
    }
  });
}