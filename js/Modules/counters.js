
$(window).scroll(function() {
     
     checkCounterPosition();

});

$(document).ready(function() {
       
    checkCounterPosition();
   

});

var checkcounter = 0;

function countTo(){

    $('.counter-box .count-to').each(function() {
      
      var $this = $(this)
      var countTo = $this.attr('data-count');


      $({ countNum: $this.text()}).animate({
        countNum: countTo
      },

      {

        duration: 4000,
        easing:'swing',
        step: function() {
          $this.text(Math.floor(this.countNum));
        },
        complete: function() {
          $this.text(this.countNum);
        }

      });  
      
      

    });
    checkcounter = 1;
}



function checkCounterPosition() {
    var counterposition;
    if($(".counter-box .count-to").length != 0){
        counterposition = $(".counter-box .count-to").position().top + 100;
    }  
    if (($(window).scrollTop() + $(window).height()) > counterposition && checkcounter == 0) {
         countTo();
    }
}