$(document).ready(function(){

	if( $('body.about-us').length != 0)
	timelineProgress();
})



function timelineProgress(){

if($('.timeline-item-wrapper').length==0)return;
	var check = 0;
	$('.timeline-item-wrapper>span').append('<div class="progress-bar"></div>');

	var progressBar = $('.progress-bar');

	var timeLineOffsetTop = $('.timeline-item-wrapper>span').offset().top;
	var timelineheight = $('.timeline-item-wrapper>span').innerHeight();

	var timeLineEnd = timeLineOffsetTop + timelineheight;

	var timeAnimation = timelineheight * 4;

	var endText = $('.timeline-end-text span').html();

	$(window).scroll(function() {
	        var scroll = $(window).scrollTop();

	        if(scroll > (timeLineOffsetTop - 800) && scroll < timeLineEnd){
	        	// console.log(scroll - timeLineOffsetTop);
	        	// progressBar.css('height', (scroll - timeLineOffsetTop + 400) );
	        	 progressBar.animate({
				    height: (timelineheight - 140)+"px",
				  }, timeAnimation );

	        	 $('.timeline-item-wrapper').addClass('animate')
	        }

	        if( $('.timeline-item-wrapper.animate').length != 0 && check == 0){
	        	check = 1;
	        	 setTimeout(function(){
						$('.timeline-item-wrapper>span').append('<div class="dot-end wow fadeInDown"></div>');
						$('.timeline-item-wrapper>span').append('<div class="text-end wow fadeInUp">'+ endText +'</div>');
				},timeAnimation)
	        }
	})
		
}



