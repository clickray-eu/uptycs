if ($('.me-hero-banner-slider .me-hero-banner-slide').length > 0) {
    $('.me-hero-banner-slider').append('<div class="me-hero-banner-main-slider"></div><div class="me-hero-banner-nav-slider"></div>')

    $('.me-hero-banner-slider .me-hero-banner-slide').each(function() {
        $(this).appendTo('.me-hero-banner-main-slider');
    })

    $('.me-hero-banner-slider .me-hero-banner-slide--element').each(function() {
        $(this).appendTo('.me-hero-banner-nav-slider');
    })

    $('.me-hero-banner-main-slider:not(.slick-initialized)').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        fade: true,
        arrows: false,
        adaptiveHeight: true,
        focusOnSelect: true
    });

    $('.me-hero-banner-nav-slider:not(.slick-initialized)').slick({
        slidesToShow: 3,
        centerMode: true,
        arrows: false,
        slidesToScroll: 1,
        asNavFor: '.me-hero-banner-main-slider',
        focusOnSelect: true,
        responsive: [
            {
                breakpoint: 680,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });
}
