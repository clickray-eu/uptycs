(function ($) {
    $.fn.cutWrappers = function (targetSelector) {
        $(this).find(targetSelector).appendTo($(this));
        $(this).find('.hs_cos_wrapper_type_custom_widget').remove();
        $(this).find('.hs_cos_wrapper_type_widget_container').remove();
        $(this).find('.widget-span').remove();
    }
}(jQuery));

// $('.resource__container').cutWrappers('.resource');
// $('.resource-categories__list').cutWrappers('.resource-categories__element');
let visibleElements = 9;
let elementsOnPage = 0;
$(document).ready(() => {
    if ($('body').hasClass('resources-page')) {
        filterGridResources();

        let initalLoad = true;
        if (initalLoad == true) {
            $('.resource.resource--vertical').map((i, e) => {
                elementsOnPage++;
                if (i >= visibleElements) {
                    $(e).addClass('invis');
                }
            })
            initalLoad = false;
        }

        $('.resource-categories__element').eq(0).addClass('resource-categories__element--active');
        $('.resource-categories__element--active').trigger('click');
        $('.load-more-button a').on('click', (event) => {
            event.preventDefault();
            loadMoreElements('.resource.resource--vertical', 3)
        });
    }
})

function filterGridResources() {
    var resourcesContainer = $('.resources-page .resource__container');
    $('.resource-categories__element').on('click', function () {
        $('.resource-categories__element').removeClass('resource-categories__element--active');
        $(this).addClass('resource-categories__element--active');
        var filterValue = $(this).attr('data-filter');
        resourcesContainer.isotope({
            filter: filterValue,
            itemSelector: '.resource',
            layoutMode: 'fitRows',
            masonry: {
                columnWidth: '25%'
            }

        });
    })
}
function loadMoreElements(element, increment) {
    $(element).map((i, e) => {
        if (i >= visibleElements && i < visibleElements + increment) {
            $(e).removeClass('invis');
        }
    })
    visibleElements = visibleElements + increment;

    if (visibleElements >= elementsOnPage) {
        $('.load-more-button').fadeOut();
    }
    $('.resource-categories__element').removeClass('resource-categories__element--active');
    $('.resource-categories__element').eq(0).addClass('resource-categories__element--active');
    $('.resource-categories__element--active').trigger('click');
}