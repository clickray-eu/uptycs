$(document).ready(function () {
  setImageHeight();
  avataImageSwap();
})


$(window).resize(function () {
  avataImageSwap();
  setImageHeight();
})



function avataImageSwap() {

  if ($('.avatar-wrapper').length) {
    var avatar_src = $('.avatar-wrapper img').attr('src');
    $('.avatar-wrapper').css('background-image', 'url("' + avatar_src + '")')

  }

}

function setImageHeight() {
  var deviceWidth = $(window).innerWidth();

  if ($('.avatar-wrapper').length && deviceWidth > 1024) {
    $('.top-section .avatar-wrapper').parent().parent().css('height', 'auto');
    var leftHeight = $('.top-section .half-container').parent().parent().height();
    $('.top-section .avatar-wrapper').parent().parent().css('height', leftHeight);

  } else {
    $('.top-section .avatar-wrapper').parent().parent().css('height', 'auto');
  }
}



