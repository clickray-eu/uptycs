
if ($('.client-reviews-slider').length > 0) {
        $('.client-reviews-slider > span').slick({
            centerMode: true,
            slidesToShow: 3,
            slidesToScroll: 1,
            arrows: false,
            dots: true,
              responsive: [
                {
                  breakpoint: 1150,
                  settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                  }
                }
,
                {
                  breakpoint: 900,
                  settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                  }
                }
,
                {
                  breakpoint: 760,
                  settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                  }
                }
            ]
        });
}
  $( ".slick-center" ).next().addClass('elo1');
    $( ".slick-center" ).prev().addClass('elo2');
jQuery('.client-reviews-slider > span').on('afterChange beforeChange', function(event, slick, currentSlide){
  	$( ".slick-slide").each(function(){
  		$( ".slick-slide").removeClass('elo1');
  		$( ".slick-slide" ).removeClass('elo2');
    });
    $( ".slick-center" ).next().toggleClass('elo1');
    $( ".slick-center" ).prev().toggleClass('elo2');
});