function setMapHeight() {
    var getHeight = $('.contact-3 .main-content').height()
    $('#map').css('height', getHeight);

    var deviceWidth = $(window).innerWidth();

    if ($('.contact-3').length && deviceWidth < 768) {
        $('#map').css('height', '500px');
    }
}


$(document).ready(function(){

	setMapHeight();
})

waitForLoad(".widget-type-form", "form", setMapHeight);