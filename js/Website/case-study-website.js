let visibleElementscount = 9;
let numberOfElements = 0;
$(document).ready(() => {
    if ($('body').hasClass('case-study-website')) {
        filterGridCaseStudies();

        let initalLoad = true;
        if (initalLoad == true) {
            $('.post-item').map((i, e) => {
                numberOfElements++;
                if (i >= visibleElementscount) {
                    $(e).addClass('invis');
                }
            })
            initalLoad = false;
        }

        $('.case-study-filter-single').eq(0).addClass('active');
        $('.case-study-filter-single.active').trigger('click');
        $('.load-more-button a').on('click', (event) => {
            event.preventDefault();
            loadMoreCaseStudies('.post-item', 3)
        });
    }
})

function filterGridCaseStudies() {
    var resourcesContainer = $('.case-study-blog.website .case-studies-listing-wrapper');
    $('.listing-tag-filters .case-study-filter-single').on('click', function () {
        $('.case-study-filter-single').removeClass('active');
        $(this).addClass('active');
        var filterValue = $(this).attr('data-filter');
        resourcesContainer.isotope({
            filter: filterValue,
            itemSelector: '.post-item',
            layoutMode: 'fitRows',
            masonry: {
                columnWidth: '25%'
            }

        });
    })
}
function loadMoreCaseStudies(element, increment) {
    $(element).map((i, e) => {
        if (i >= visibleElementscount && i < visibleElementscount + increment) {
            $(e).removeClass('invis');
        }
    })
    visibleElementscount = visibleElementscount + increment;

    if (visibleElementscount >= numberOfElements) {
        $('.load-more-button').fadeOut();
    }
    $('.case-study-filter-single').removeClass('active');
    $('.case-study-filter-single').eq(0).addClass('active');
    $('.case-study-filter-single.active').trigger('click');
}