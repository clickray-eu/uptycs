// Certification Slider 
$('.brands-slider-wrapper > span').slick({
    slidesToShow: 4,
    slidesToScroll: 4,
    focusOnSelect: true,
    autoplay: true,
    dots: false,
    responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 680,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
   ]
});

