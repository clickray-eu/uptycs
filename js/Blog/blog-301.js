$(document).ready(function () {
    if ($('.fixed-socials').length) {
        const spaceTop = $('.fixed-socials').offset().top - 150;
        $(window).scroll(() => {
            $(window).scrollTop() > spaceTop ? $('.fixed-socials').addClass('fixed') : $('.fixed-socials').removeClass('fixed');
        }).scroll();
    }
    $('.blog .follow-us .no-base, .blog .fixed-socials .no-base, .share-socials .socials-wrapper .no-base ').on('click', (e, i) => {
        const socialUrl = $(e.target).parent().data('href') || '#';
        // console.log(socialUrl);
        const requirement = socialUrl.startsWith('http') || socialUrl.startsWith('//');
        // console.log(requirement);
        requirement ? window.open(socialUrl) : window.location = socialUrl;
    })
})

function appendHeading(i, e) {
    if ($('.blog').length) {
        const textToAppend = `<p class="big">Write a comment</p>`;
        $(e).closest('#comment-form').prepend(textToAppend);
    }
}

waitForLoad(".widget-type-blog_comments", "#comment-form form", appendHeading)
