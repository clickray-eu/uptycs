$(document).ready(() => {
    if ($('.case-study-blog-single video').length) {
        const videos = Array.from(document.querySelectorAll('video'));
        const wrapper = $('<div class="video-blog-wrapper"></div>');
        $('video').wrap(wrapper);
        $('body.case-study-blog-single').on('click', wrapper, function (event) {
            if ($(event.target).hasClass('video-blog-wrapper')) {
                const clickedVideo = $(event.target).find('video');
                const clickedVideoJSDOM = clickedVideo.get(0);
                if (clickedVideoJSDOM.paused) {
                    clickedVideoJSDOM.play();
                    clickedVideo.parent().addClass('no-bg');
                    clickedVideo.attr('controls', true);
                } else {
                    clickedVideoJSDOM.pause();
                    clickedVideo.parent().removeClass('no-bg');
                    clickedVideo.attr('controls', false);                    
                }
            }
        });
    }
    $(".video-blog-wrapper").on("mousemove",function(e){
        var cursorPos=e.offsetY;
        var height=$(this).height();
        if(height-cursorPos<=50){
            $(this).addClass("mouseover");
        }else{
            $(this).removeClass("mouseover");    	
        }
    });
})
