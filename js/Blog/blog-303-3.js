// Select
function blogflitersSelect($wrapper, $form) {
    if ($form.hasClass('new-select-custom')) {
        return;
    }
    $form.addClass('new-select-custom');
    $form.find("select").each(function () {
        var parent = $(this).parent(),
            options = $(this).find('option'),
            placeholder = options.first().text() ? options.first().text() : '-Select-';

        parent.addClass('dropdown_select');
        parent.append('<div class="dropdown-header"><span>' + placeholder + '</span></div>');
        parent.append('<ul class="dropdown-list" style="display: none;"></ul>');
        options.not(":disabled").each(function (index) {
           
            if ( $(this).text() != "" && index > 0) {

                parent.find("ul.dropdown-list").append('<li value="' + $(this).text() + '">' + $(this).text() + '</li>')
            }else{
            	parent.find("ul.dropdown-list").append('<li value="">' + $(this).text() + '</li>')
            }
        })
    })

    $form.find('.dropdown-header').click(function (event, element) {
 
        if (!($(this).hasClass('slide-down'))) {
            $(this).addClass('slide-down');
            $(this).siblings('.dropdown-list').slideDown();
        } else {
            $(this).removeClass('slide-down');
            $(this).siblings('.dropdown-list').slideUp();
        }
        $(this).children('.arrow-white').toggle();
    });

    $form.find('.dropdown-list li').click(function () {
        var choose = $(this).attr("value");
        var text = $(this).text();
    
        $(this).parent().siblings('.dropdown-header').find('span').text(text);
        $(this).parent().siblings('select').find('option').removeAttr('selected');
        $(this).parent().siblings('select').val(choose).find(' option[value="' + choose + '"] ').attr('selected', 'selected').change();
        $(this).parent().find('li').removeClass('selected');
        $(this).addClass('selected');
        $(this).parent().siblings('.dropdown-header').toggleClass('slide-down').siblings('.dropdown-list').slideUp();
        $(this).parent().siblings('.dropdown-header').children('.arrow-white').toggle();
    });
    $form.find('.dropdown_select .input').click(function (event) {
        event.stopPropagation();
    });
}

waitForLoad(".blog-filtter", "form", blogflitersSelect)


// Share hero banner

$(".blog.blog-3 .social").click(function(e) {
    
    $(".item").removeClass("no");
   
    if(e.target != this) return;
    $('.blog.blog-3 .item').toggleClass("active");
});


// Comments

waitForLoad(".blog.blog-3 .widget-type-blog_comments", "form", function(){

    $('.comment-reply-to').text(replayText); // zmienna zdefiniowana w templacie
    $('.comment-form .big').text(formTitle); // zmienna zdefiniowana w templacie

    if( $('#comments-listing .comment').length != 0 ){
        $('#comments-listing').parent().prepend('<p class="comments-tilte">'+commentsTitle+'</p>')// zmienna zdefiniowana w templacie
    }

})

