waitForLoad(".widget-type-form, .form-wrapper, .widget-type-blog_comments, .hs_cos_wrapper_type_form", "form", customInputFile)
waitForLoad(".widget-type-form, .form-wrapper, .widget-type-blog_comments, .hs_cos_wrapper_type_form", "form", autoGrow)
waitForLoad(".widget-type-form, .form-wrapper, .widget-type-blog_comments, .hs_cos_wrapper_type_form", "form", fakePlaceholders)
waitForLoad(".widget-type-form, .form-wrapper, .widget-type-blog_comments, .hs_cos_wrapper_type_form", "form", hideEmptyLabel)
waitForLoad(".widget-type-form, .form-wrapper, .widget-type-blog_comments, .hs_cos_wrapper_type_form", "form", select)

function hideEmptyLabel() {
    $('form label').each(function (i, e) {
        if ($(this).text() == "*") {
            $(this).css({ "display": "none" })
        }
    })
}
 
// Input type file
function customInputFile() {

    $('input[type=file]').parent().parent().find('label:first-child').addClass("file-name");
    $('input[type=file]').addClass("inputfile");
    $('input[type=file]').parent().parent().addClass("inputfile-fake");

    var inputs = $('.inputfile');
    Array.prototype.forEach.call(inputs, function (input) {
        var label = document.querySelectorAll('.file-name')[0];
        input.addEventListener('change', function (e) {
            var fileName = e.target.value.split('\\').pop();
            if (fileName)
                label.querySelector('.file-name span').innerHTML = fileName;
            else
                label.innerHTML = labelVal;
        });
    });

}

let firstload = true;
function fakePlaceholders() {
    var inputName;
    $('input[type=file]').each(function () {
        inputName = $(this).parent().parent().find('label:first-child span').text();
        $(this).attr('placeholder', inputName);
        $(this).parent().parent().find('label:first-child span').text('');
    })


  
    var $inputs = $('input[type=text], input[type=tel], input[type=email], input[type=file], textarea').not('#search-input');
    $inputs.each(function () {
        if ($(this).attr('placeholder') != undefined ) {
            var el = $(this),
                placeholder = el.attr('placeholder'),
                id = el.attr('id');

            if ($(this).val().length > 0) {
                $(this).siblings(".fake-placeholder").addClass("has-value");
            } else {
                $(this).siblings(".fake-placeholder").removeClass("has-value");
            }

            el.attr('placeholder', '');
            el.parent().append('<label for="' + id + '" class="fake-placeholder">' + placeholder + '</label>');
        }

    })
    $inputs.on('change', function () {
        if ($(this).val().length > 0) {
            $(this).siblings(".fake-placeholder").addClass("has-value");
        } else {
            $(this).siblings(".fake-placeholder").removeClass("has-value");
        }
    })

    $inputs.map(function () {
        if ($(this).val().length > 0) {
            $(this).siblings(".fake-placeholder").addClass("has-value");
        } else {
            $(this).siblings(".fake-placeholder").removeClass("has-value");
        }
    })

    firstload = false;

}



// Autogrow textarea while typing
(function ($) {
    /**
     * Auto-growing textareas; technique ripped from Facebook
     * 
     * 
     * http://github.com/jaz303/jquery-grab-bag/tree/master/javascripts/jquery.autogrow-textarea.js
     */
    $.fn.autogrow = function (options) {
        return this.filter('textarea').each(function () {
            var self = this;
            var $self = $(self);
            var minHeight = $self.height();
            var noFlickerPad = $self.hasClass('autogrow-short') ? 0 : parseInt($self.css('lineHeight')) || 0;
            var settings = $.extend({
                preGrowCallback: null,
                postGrowCallback: null
            }, options);

            var shadow = $('<div></div>').css({
                position: 'absolute',
                top: -10000,
                left: -10000,
                width: $self.width(),
                fontSize: $self.css('fontSize'),
                fontFamily: $self.css('fontFamily'),
                fontWeight: $self.css('fontWeight'),
                lineHeight: $self.css('lineHeight'),
                resize: 'none',
                'word-wrap': 'break-word'
            }).appendTo(document.body);

            var update = function (event) {
                var times = function (string, number) {
                    for (var i = 0, r = ''; i < number; i++) r += string;
                    return r;
                };

                var val = self.value.replace(/&/g, '&amp;')
                    .replace(/</g, '&lt;')
                    .replace(/>/g, '&gt;')
                    .replace(/\n$/, '<br/>&#xa0;')
                    .replace(/\n/g, '<br/>')
                    .replace(/ {2,}/g, function (space) { return times('&#xa0;', space.length - 1) + ' ' });

                // Did enter get pressed?  Resize in this keydown event so that the flicker doesn't occur.
                if (event && event.data && event.data.event === 'keydown' && event.keyCode === 13) {
                    val += '<br />';
                }

                shadow.css('width', $self.width());
                shadow.html(val + (noFlickerPad === 0 ? '...' : '')); // Append '...' to resize pre-emptively.

                var newHeight = Math.max(shadow.height() + noFlickerPad, minHeight);
                if (settings.preGrowCallback != null) {
                    newHeight = settings.preGrowCallback($self, shadow, newHeight, minHeight);
                }

                $self.height(newHeight);

                if (settings.postGrowCallback != null) {
                    settings.postGrowCallback($self);
                }
            }

            $self.change(update).keyup(update).keydown({ event: 'keydown' }, update);
            $(window).resize(update);

            update();
        });
    };
})(jQuery);

function autoGrow() {
    $('textarea').autogrow();
}


// Select
function select($wrapper, $form) {
    if ($form.hasClass('new-select-custom')) {
        return;
    }
    $form.addClass('new-select-custom');
    $form.find("select").each(function () {
        var parent = $(this).parent(),
            options = $(this).find('option'),
            placeholder = options.first().text() ? options.first().text() : '-Select-';

        parent.addClass('dropdown_select');
        parent.append('<div class="dropdown-header"><span>' + placeholder + '</span></div>');
        parent.append('<ul class="dropdown-list" style="display: none;"></ul>');
        options.not(":disabled").each(function () {
            if ($(this).text() != "") {
                parent.find("ul.dropdown-list").append('<li value="' + $(this).text() + '">' + $(this).text() + '</li>')
            }
        })
    })

    $form.find('.dropdown-header').click(function (event, element) {
        if (!($(this).hasClass('slide-down'))) {
            $(this).addClass('slide-down');
            $(this).siblings('.dropdown-list').slideDown();
        } else {
            $(this).removeClass('slide-down');
            $(this).siblings('.dropdown-list').slideUp();
        }
        $(this).children('.arrow-white').toggle();
    });

    $form.find('.dropdown-list li').click(function () {
        var choose = $(this).text();
        $(this).parent().siblings('.dropdown-header').find('span').text(choose);
        $(this).parent().siblings('select').find('option').removeAttr('selected');
        $(this).parent().siblings('select').val(choose).find(' option[value="' + choose + '"] ').attr('selected', 'selected').change();
        $(this).parent().find('li').removeClass('selected');
        $(this).addClass('selected');
        $(this).parent().siblings('.dropdown-header').toggleClass('slide-down').siblings('.dropdown-list').slideUp();
        $(this).parent().siblings('.dropdown-header').children('.arrow-white').toggle();
    });
    $form.find('.dropdown_select .input').click(function (event) {
        event.stopPropagation();
    });
}