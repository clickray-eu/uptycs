// Change header on scroll
function scrollHeader() {
    if (!$('header').hasClass('permanent-white')) {
        if ($(window).scrollTop > 10) {
            header.addClass('header-scroll');
        }
        $(window).on('scroll', () => {
            let scroll = $(window).scrollTop();
            const header = $('header');
            const headerDiv = $('header > div');
            if (scroll > 10) {
                header.addClass('header-scroll');
                headerDiv.addClass('header-white');
            } else {
                header.removeClass('header-scroll');
                headerDiv.removeClass('header-white');
            }
        });
        if ($('header.swap-logo .logo img').length != 0) {
            let scroll;
            const header = $('header > div'),
                logo = $('.logo img'),
                logoSrc = logo.attr('src'),
                logoNewSrc = logoSrc.replace('-color', '-white'),
                hamburger = $('.hamburger');
            if ($('header.header-transparent.swap-logo').length) {
                logo.attr('src', logoNewSrc);
            }

            if ($('body:not(.same-logo)').length != 1) {
                logo.attr('src', logoNewSrc);
            }
            $(window).on('scroll', () => {
                scroll = $(window).scrollTop();
                if ($('body:not(.same-logo)').length == 1) {
                    if (scroll > 10) {
                        logo.attr('src', logoSrc);

                    } else {
                        logo.attr('src', logoNewSrc);
                    }
                }
            }).scroll();
        }
    }



}


// Open submenu
function toggleChildMenu() {
    const menuItem = $('.hs-item-has-children > a');

    menuItem.on('click', function () {
        const childrenMenu = $(this).siblings('.hs-menu-children-wrapper');

        $(this).parent().siblings('.hs-item-has-children').each(function () {
            $(this).children('.hs-menu-children-wrapper').slideUp();
        })

        childrenMenu.slideToggle();
    })

}


// Scrollbar width
function getScrollBarWidth() {
    let $outer = $('<div>').css({ visibility: 'hidden', width: 100, overflow: 'scroll' }).appendTo('body'),
        widthWithScroll = $('<div>').css({ width: '100%' }).appendTo($outer).outerWidth();

    $outer.remove();
    return 100 - widthWithScroll;

};


$(document).ready(() => {
    scrollHeader();
    toggleChildMenu();

    if ($('header .me-header-search').length == 0) {
        $('header nav .me-container > div > div:last-child .widget-type-menu').css('padding-bottom', 100);
    }
    if ($('.language-switcher .lang_switcher_class').length) {
        $('header .lang-opts').prepend('<div id="lang-current"></div>');
        const currentLangEl = $('#lang-current');
        const currentLang = $('header .lang_list_class li:first-of-type a').text().substr(0, 2);
        currentLangEl.text(currentLang);
    }
    if ($('.advanced-menu-header').length) {
        $(function () {
            $('.advanced-menu-header').slicknav({
                appendTo: '.header-main-content >div >div',
                label: '',
                duration: 500,
                openedSymbol: '+',
                closedSymbol: '+',
                slideDirection: 'left',
                init: () => {
                    $('.slicknav_nav').addClass('preventDefault').addClass("slicknav_hidden");
                },
                beforeOpen: (trigger) => {
                    $('.slicknav_nav').removeClass('preventDefault');
                    if ($(trigger).hasClass('slicknav_btn')) {
                        $('.mobile-menu--mask').fadeIn(700);
                    }
                },
                afterClose: (trigger) => {
                    if ($(trigger).hasClass('slicknav_btn')) {
                        $('.mobile-menu--mask').fadeOut(700);
                    }
                }
            });
            const exitBtn = $('<div class="mobile-menu--exit"></div>'),
                mobileMenuMask = $('<div class="mobile-menu--mask"></div>');
            $('.slicknav_menu').append(mobileMenuMask);
            $('.slicknav_menu .hs-menu-wrapper > ul').append(exitBtn);
            $('.slicknav_menu').on('click', '.mobile-menu--exit, .mobile-menu--mask', () => {
                $('.advanced-menu-header').slicknav('close');
            })
        });
    }


})
